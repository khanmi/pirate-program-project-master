extends Node

var ships = []
var dayNum = 0
#export (PackedScene) var Ship
#export (PackedScene) var Character
#export (PackedScene) var Player_ship

func _ready():
	ships.resize(2)
	new_game()
	
	
func new_game():
	var player_ship_scene = preload("res://map_view/ship/player_ship/Player_ship.tscn")
	var player_ship = player_ship_scene.instance()
	add_child(player_ship)
	ships[0] = player_ship
	var ship_scene = preload("res://map_view/ship/Ship.tscn")
	var ship = ship_scene.instance()
	add_child(ship)
	ships[1] = ship
	ships[0].start($StartPosition.position)
	var pos = Vector2()
	pos.x = 100
	pos.y = 100
	ships[1].start(pos)
	
	var player_scene = preload("res://map_view/island_view/Character/Character.tscn")
	var player = player_scene.instance()
	add_child(player)
	player.gold_amount = 100
	player.resource_amount[0] = 100
	player.resource_amount[1] = 100
	player.resource_amount[2] = 100
	player.resource_amount[3] = 100
	print("MERCHANTS:")
	var merchant_scene = preload("res://map_view/island_view/Character/npc/merchant/Merchant.tscn")
	var merchant = merchant_scene.instance()
	add_child(merchant)
	merchant.passive_reponse(player, ships[0])
	print(player.resource_amount)
	merchant.aggressive_reponse(player)
	print(player.resource_amount)
	print("NAVY: ")
	var navy_scene = preload("res://map_view/island_view/Character/npc/navy/Navy.tscn")
	var navy = navy_scene.instance()
	add_child(navy)
	navy.passive_response(player, ships[0])
	print(ships[0].speed)
	navy.aggressive_reponse(player)
	print(player.resource_amount)
	print("PIRATES: ")
	var pirate_scene = preload("res://map_view/island_view/Character/npc/pirate/Pirate.tscn")
	var pirate = pirate_scene.instance()
	add_child(pirate)
	pirate.passive_response(player, ships[0])
	print("GOLD: ", player.gold_amount)
	print("CREW: ", player.resource_amount[0])
	pirate.aggressive_response(player)
	print(player.resource_amount)
	
	# TEST INHABITED ISLAND ISLAND INTERACT METHOD
	print("Inhabited island test: ")
	var inhabited_scene = preload("res://map_view/island/inhabited_island/Inhabited_island.tscn")
	var inhabited = inhabited_scene.instance()
	player.gold_amount = 100
	player.resource_amount[0] = 100
	player.resource_amount[1] = 100
	player.resource_amount[2] = 100
	player.resource_amount[3] = 100
	add_child(inhabited)
	inhabited.new_island("SOME STORY", false, merchant)
	inhabited.interact(player, "passive", ships[0])
	print(player.resource_amount)
	inhabited.interact(player, "aggressive", ships[0])
	print(player.resource_amount)
	
	# TEST DESERTED ISLAND
	print("Deserted island test: ")
	var deserted_scene = preload("res://map_view/island/deserted_island/Deserted_island.tscn")
	var deserted = deserted_scene.instance()
	player.gold_amount = 100
	player.resource_amount[0] = 100
	player.resource_amount[1] = 100
	player.resource_amount[2] = 100
	player.resource_amount[3] = 100
	add_child(deserted)
	deserted.new_island("SOME STORY", false, null)
	
	
	
	
	
	
	
	


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
