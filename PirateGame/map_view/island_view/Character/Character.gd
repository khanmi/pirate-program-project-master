extends RigidBody2D

# Amount of gold the character has.
var gold_amount = 0
# Array for the amount of each resource of the player. [0] = crew num, [1] = gunpowder, [2] = repairs, [3] = rum.
var resource_amount = []



# Called when the node enters the scene tree for the first time.
func _ready():
	resource_amount.resize(4)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

