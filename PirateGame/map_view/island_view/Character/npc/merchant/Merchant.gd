extends "res://map_view/island_view/Character/npc/NPC.gd"



# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	# The amount of gold the merchant will hold is between 0 and 100.
	gold_amount = randi() % 101
	# Amount of each resource is within the list is between 0 and 25 apart from crew.
	resource_amount[0] = 0
	for i in (resource_amount.size() - 1):
		resource_amount[i+1] = randi() % 26
	# Set a random difficulty between 1 and 40
	difficulty = (randi() % 40) + 1
	# 70% change the merchant will give the player resources and join their crew.
	isGood = randi() % 10 < 7


# The merchants reponse to the players passive choice.
func passive_reponse(player, navy_ship):
	randomize()
	# LINK TO THE STORY IN SOME WAY
	for i in resource_amount.size():
		if isGood:
			# Gives the player resources and joins their crew.
			player.resource_amount[i] += resource_amount[i]
		else:
			# Takes away some of the players resources.
			if player.resource_amount[i] > resource_amount[i]:
				player.resource_amount[i] -= resource_amount[i]
			else: 
				player.resource_amount[i] = 0
	
	
func aggressive_reponse(player):
	return .aggressive_response(player)
	
	