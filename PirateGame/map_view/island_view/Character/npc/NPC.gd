extends "res://map_view/island_view/Character/Character.gd"

var difficulty = 0
var isGood = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func passive_reponse(player, navy_ship):
	return
	
func aggressive_response(player):
	randomize()
	# The amount of resources the player expended.
	var resource_used = 0
	# Loops through for each resource except rum i.e resource_amount[3].
	for i in resource_amount.size() - 1:
		# Takes away a random amount of each resource between 0 and the difficulty of the NPC.
		resource_used = randi() % difficulty
		if player.resource_amount[i] > resource_used:
			player.resource_amount[i] -= resource_used
		else:
			# The player has lost the game as they have 0 amount of a vital resource for a fight.
			player.resource_amount[i] = 0