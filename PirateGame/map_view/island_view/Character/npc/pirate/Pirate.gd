extends "res://map_view/island_view/Character/npc/NPC.gd"



# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	# The amount of gold the merchant will hold is between 0 and 500.
	gold_amount = randi() % 501
	# The Pirate Captain has a crew size between 1 and 25.
	resource_amount[0] = randi() % 25 + 1
	# Set a random difficulty between 30 and 70 including.
	difficulty = (randi() % 41) + 30
	
	# 50% chance that the pirate will be good.
	isGood = randi() % 10 > 4
	
func passive_response(player, navy_ship):
	randomize()
	# Checks whether the pirate is good or bad.
	if (isGood):
		# The pirate is good therefore, they give the player their gold and join their crew.
		player.gold_amount += gold_amount
		player.resource_amount[0] += resource_amount[0]
	else:
		# Otherwise the pirate crew steals half of the players gold.
		var gold_stolen = player.gold_amount/2
		player.gold_amount -= gold_stolen
		gold_amount += gold_stolen
		# Pirate crew kill 5 or less of the players crew
		if player.resource_amount[0] > 5:
			player.resource_amount[0] -= randi() % 6
		

func aggresive_response(player):
	return .aggressive_response(player)
	



