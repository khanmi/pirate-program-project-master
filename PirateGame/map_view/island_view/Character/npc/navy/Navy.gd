extends "res://map_view/island_view/Character/npc/NPC.gd"



# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	# Have a random amount of gold between 0 and 200.
	gold_amount = randi() % 201
	# Have a difficulty between 50 and 100.
	difficulty = randi() % 51 + 50
	# 70% change they will inform the navy ships that you are searching for the gold.
	isGood = randi() % 10 > 6

func passive_response(player, navy_ship):
	randomize()
	# LINK TO THE STORY IN SOME WAY
	if !isGood:
		# Speed up the ships.
		navy_ship.speed *= 1.5
	else:
		# Miss-inform the navy ships.
		navy_ship.speed *= 0.5
	
	
func aggressive_reponse(player):
	return .aggressive_response(player)