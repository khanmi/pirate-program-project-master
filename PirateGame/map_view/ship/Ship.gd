extends KinematicBody2D

# How quick the ship will move (pixels/sec)
export var speed = 400
var velocity = Vector2()
var screen_size
var target = Vector2()

# Called when the node enters the scene tree for the first time.
func _ready():
	hide()
	screen_size = get_viewport_rect().size
	
func start(pos):
	position = pos
	target = pos
	show()
	$CollisionShape2D.disabled = false
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if position.distance_to(target) > 10:
		velocity = (target - position).normalized() * speed
	else:
		velocity = Vector2()
	position += velocity * delta
		
		
