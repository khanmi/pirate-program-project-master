extends "res://map_view/island/Island.gd"

# An array contains each resource crew, gunpowder, repairs, rum
var resources_on_island= []



func _ready():
	pass # Replace with function body.

# Creates a new instance of the island which initialised the story and if it contains a clue. 
# island_character is not used for this class but is there to match the superclasses method signature.
func new_island(story_info, contains_clue, island_character):
	for i in resources_on_island.size() - 1:
		resources_on_island[i+1] += randi() % 50
	story = story_info
	has_clue = contains_clue
	
# The player chooses to search for the clue
func interact(player, response, navy_ship):
	return
		
		
		