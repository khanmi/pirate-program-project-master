extends "res://map_view/island/Island.gd"

var npc


func _ready():
	pass

# Creates a new instance of the island which contains the story, whether it contains a clue and a npc character
func new_island(story_info, contains_clue, island_character):
	npc = island_character
	# Calls the super class Island to finish the instantiation of the class fields.
	return .new_island(story_info, contains_clue, npc)
	
func interact(player, response, navy_ship):
	# Checks whether the player responsed to the character passively or negatively.
	# and calls the corresponding method.
	if response == "passive":
		npc.passive_reponse(player, navy_ship)
	else:
		npc.aggressive_response(player)
	
	
	
func store_interact(player):
	return