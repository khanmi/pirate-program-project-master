extends Node

# The daynight cycle reprsents the time ticking throughout the game and is used to show correct visuals 
# along with moving the navy ship based on the time of day.

const TIME_PERIOD = 720 #  12 mins
# If the time has started.
var start = false
# Sets the intial time.
var time = 0.0
# Signal timeout.
signal timeout

# Adds the time to saveable nodes.
func _ready():
	add_to_group('saveable')

# Saves the state including the filepath, parent and the time.
# @Return dict:Dictionary -- The saved content dictionary.
func save():
	var dict = {
		"filename": get_filename(),
		"parent": get_parent().get_path(), 
		"time": time
	}
	return dict

# Called every cycle and if the time has started, then the time is incremented and if the time 
# is passed the timeout then the time is reset.
# @Param delta -- The cylcle time
func _process(delta):
	if (start ==true):
		time += delta
		 #If the time has timed out then it emits the signal and resets the time.
		if time > TIME_PERIOD:
			emit_signal("timeout")
			# Reset timer
			time = 0

# Gets whether the time has statred
# @Return start:bool -- The 
func get_start()->bool:
	return start

# Gets the current time
# @Return time:float -- The current time.
func get_time()->float:
	return time

# Called when the game is started and sets the time to 0 and start to true.
func _on_start_game():
	time = 0
	start = true

# Called when the game is stopped and sets the start to false.
func _on_stop_game():
    start = false

# Resumes the game setting start back to true.
func _on_resume_game():
	start = true



