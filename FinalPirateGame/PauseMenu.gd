extends Control


# A controller to the pause menu screen coordinating what happens when the player chooses to continue with their
# game of go back to the main menu.

# When the quit button is pressed it stops the day night cycle and sets the scene back to the main menu.
func _on_btnQuit_pressed():
	DayNightCycle._on_stop_game()
	assert(get_tree().change_scene("res://MainMenu.tscn") == OK)

# When the save button is pressed it saves the game state.
func _on_btnSave_pressed():
	get_node("/root/Main").saveGame()

# When the contrinue button is pressed it returns to the view the player was last at and updates the 
# state of the game to reflect it and starts the day night cycle again.
func _on_btnContinue_pressed():
	var main = get_node("/root/Main")
	main.view.update(main.previous_state, main.game.State,  main.game.player.get_ship().get_current_island(), get_tree())
	main.game.current_state = main.previous_state
	DayNightCycle._on_resume_game()

# When the save button is pressed it saves the data 
func _on_save_button_pressed():
	save_data()

# Saves the data from the current game and writes it into a file is json format.
func save_data():
	# The player resouces data.
	var data = {
		'player_resources': get_node("/root/Main").game.player.resources
	}
	# Saves the lines of data into the file.
	var save_game = File.new()
	save_game.open("user://save_resources.save", File.WRITE)
	save_game.store_line(to_json(data))
	save_game.close()
	
# Saves the state of the current game based on the saveable nodes and places the information into a save game file.
func save_game():
    var save_game = File.new()
    save_game.open("user://savegame.save", File.WRITE)
    var save_nodes = get_tree().get_nodes_in_group("saveable")
    for node in save_nodes:
        # Check the node is an instanced scene so it can be instanced again during load
        if node.filename.empty():
            continue

        # Check the node has a save function
        if !node.has_method("save"):
            continue

        # Call the node's save function
        var node_data = node.call("save")

        # Store the save dictionary as a new line in the save file
        save_game.store_line(to_json(node_data))
    save_game.close()

# Called every cycle and plays the background sound if it isnt already playing.
# @Param delta:float -- The clock cycle.
func _process(delta):
	if (!$BackgroundSound.playing):
		$BackgroundSound.play(0.0)
