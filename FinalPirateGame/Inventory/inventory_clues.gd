extends Node

# Inventory clues is used to display the players inventory and any clues in which they have. This includes
# showing the player all the items in the items array, their story items, their equipped sword,
# their upgrade items and finally their clues along with text saying what they are when clicked on.
class_name Inventory_clues

# The player class instance used for the items.
var player: Player
# The indexes of the items to be dropped and the sword equipped.
var dropItemSlotUseableItem:int = -1
var dropShipUpgradeSlot: int=-1
var equipSwordIndex: int=-1
# The different nodes from the scen attached to this controller than it uses to display text and 
# show the clue visuals.
onready var itemList=get_node("InventoryBackground/ItemList")
onready var itemListUpgradeItem=get_node("InventoryBackground/ItemListUpgradeItem")
onready var storyItems=get_node("InventoryBackground/StoryItems")
onready var itemmenu=get_node("ItemList/WindowDialog_itemMenu") 
onready var itemmenu_TestureFrame=get_node("ItemList/WindowDialog_itemMenu/ItemMenu_TextureFrame_Icon") 
onready var itemmenu_richText=get_node("ItemList/WindowDialog_itemMenu/ItemMenu_richText_ItemInfo")
onready var itemmenu_button=get_node("ItemList/WindowDialog_itemMenu/ItemMenu_Button_Drop")
onready var game_state = get_node("/root/Main").game

# Sets up all of the visuals that are dynamic for the inventory including all
# the items they have for each part of their inventory and whether clues should 
# be visible if the player has them.
func _ready():
	# Initialize Item List
	
	#set icons to right size and mode
	
#	$InventoryBackground/ItemList.set_max_columns(3)
	$InventoryBackground/ItemList.set_fixed_icon_size(Vector2(48,48))
	$InventoryBackground/ItemList.set_icon_mode(ItemList.ICON_MODE_TOP)
	#$InventoryBackground/ItemList.set_same_column_width(true)
	$InventoryBackground/ItemListUpgradeItem.set_fixed_icon_size(Vector2(48,48))
	$InventoryBackground/ItemListUpgradeItem.set_icon_mode(ItemList.ICON_MODE_TOP)
	
	$InventoryBackground/StoryItems.set_fixed_icon_size(Vector2(48,48))
	$InventoryBackground/StoryItems.set_icon_mode(ItemList.ICON_MODE_TOP)
	
	$InventoryBackground/EquippedSword.set_fixed_icon_size(Vector2(48,48))
	$InventoryBackground/EquippedSword.set_icon_mode(ItemList.ICON_MODE_TOP)
	

	#initialise inventories
	initialItemInventory()
	initialUpgradeInventory() 
	initialStoryInventory()
	#give player no sword
	$InventoryBackground/EquippedSword.add_item("", load("res://Inventory/empty_slot.png"), false)
	
	player = game_state.player
	var index = 0
	#for each clue
	for clue in player.get_clues():
		#if no clue, hide graphic for clue
		if clue == null:
			$InventoryBackground.get_child(index).get_child(0).hide()
		index +=1
		
	renderItemInventory(game_state.get_player().get_items())##this renders the items
	renderUpgradeInventory(game_state.get_player().get_ship_upgrades())#this renders the ship upgrade
	renderStoryInventory(game_state.get_player().get_story_items())##this renders the story items

func _init() -> void:
		pass
	
# When the first clue button is pressed if the clues isnt null it shows the player
# the clue text so they can read it as a pop up
func _on_btnClue1_pressed():
	#when player clicks on clue 1
	#if the clue exists
	if player.clues[0] != null:
		#move the info popup to where the player clicked
		$InventoryBackground/WindowDialogClue1.set_position(get_viewport().get_mouse_position())
		#set the title of the popup
		$InventoryBackground/WindowDialogClue1.set_title("clue 1")
		#set the text for the popup
		$InventoryBackground/WindowDialogClue1/clueText.text=player.clues[0].get_description()
		#show the popup
		$InventoryBackground/WindowDialogClue1.popup()

# When the second clue button is pressed if the clues isnt null it shows the player
# the clue text so they can read it as a pop up
func _on_btnClue2_pressed():
	#when player clicks on clue 2
	#if the clue exists
	if player.clues[1] != null:
		#move the info popup to where the player clicked
		$InventoryBackground/WindowDialogClue1.set_position(get_viewport().get_mouse_position())
		#set the title of the popup
		$InventoryBackground/WindowDialogClue1.set_title("clue 2")
		#set the text for the popup
		$InventoryBackground/WindowDialogClue1/clueText.text=player.clues[1].get_description()
		#show the popup
		$InventoryBackground/WindowDialogClue1.popup()

#  When the third clue button is pressed if the clues isnt null it shows the player
# the clue text so they can read it as a pop up
func _on_btnClue3_pressed():
	if player.clues[2] != null:
		#move the info popup to where the player clicked
		$InventoryBackground/WindowDialogClue1.set_position(get_viewport().get_mouse_position())
		#
		$InventoryBackground/ClueDisplay/lblClueText.text = player.clues[2].get_description()
		#set the title of the popup
		$InventoryBackground/WindowDialogClue1.set_title("clue 3")
		#set the text for the popup
		$InventoryBackground/WindowDialogClue1/clueText.text=player.clues[2].get_description()
		#show the popup
		$InventoryBackground/WindowDialogClue1.popup()

# Whent the exit button is pressed it returns the game to the last state before the inventory.
func _on_btnExit_pressed():
	var main = get_node("/root/Main")
	#when exit clicked
	#go back to prevoious state and view
	main.view.update(main.previous_state, main.game.State,  main.game.player.get_ship().get_current_island(), get_tree())
	$InventoryBackground/WindowDialogClue1.set_position(get_viewport().get_mouse_position())
	#notify main of state change
	main.game.current_state = main.previous_state

# Sets up the initial item inventory all to blank spaces.
func initialItemInventory():
	var icon
	#for each inventory slot
	for i in range(1,7):
		#show slot as empty
		icon=ResourceLoader.load("res://Inventory/empty_slot.png")
		$InventoryBackground/ItemList.add_item("", icon, false)
		continue

# Sets up the initial story inventory all to blank spaces.		
func initialStoryInventory():
	var icon
	#for each inventory slot
	for i in range(1,10):
		#show slot as empty
		icon=ResourceLoader.load("res://Inventory/empty_slot.png")
		$InventoryBackground/StoryItems.add_item("", icon, false)
		continue

# Sets up the initial upgrade inventory all to blank spaces.
func initialUpgradeInventory():
	var icon
	#for each inventory slot
	for i in range(1,4):
		#show slot as empty
		icon=ResourceLoader.load("res://Inventory/empty_slot.png")
		$InventoryBackground/ItemListUpgradeItem.add_item("", icon, false)
		continue

# Renders the item inventory with the relevant graphics for the items
# and stores metadata about them which can be used to display infromation in a pop up.
func renderItemInventory(items:Array):
	#for each item in inventory
	for index in items.size():
			var name
			#if item exists
			if(items[index]!=null):
				
				var icon
				var img: String = "res://images//Title.png"
				#find image for item
				img = "res://images/" + items[index].get_item_name() + ".png"
				#load the image
				icon=ResourceLoader.load(img)
				
				#set the image graphic for the item
				itemList.set_item_icon(index,icon)
				#set meta data for item
				itemList.set_item_metadata(index,items[index])
			
			#draw inventories
			renderUpgradeInventory(game_state.get_player().get_ship_upgrades())
			renderStoryInventory(game_state.get_player().get_story_items())
			renderEquippedSword()

# Renders the upgrade inventory with the relevant graphics for the upgrade items
# and stores metadata about them which can be used to display infromation in a pop up.
func renderUpgradeInventory(items:Array):
	#for each item
	for index in items.size():
			var name
			#if item exists
			if(items[index]!=null):
				
				var icon
				var img: String = "res://images//Title.png"
				#find image for item
				img = "res://images/" + items[index].get_item_name() + ".png"
				#load the image
				icon=ResourceLoader.load(img)
				
				#set the image graphic for the item
				itemListUpgradeItem.set_item_icon(index,icon)
				#set meta data for item
				itemListUpgradeItem.set_item_metadata(index,items[index])
				
# Renders the story inventory with the relevant graphics for the story items
# and stores metadata about them which can be used to display infromation in a pop up.
func renderStoryInventory(items:Array):
	#for each item
	for index in items.size():
			var name
			#if item exists
			if(items[index]!=null):
				
				var icon
				var img: String = "res://images//Title.png"
				#find image for item
				img = "res://images/" + items[index].get_item_name() + ".png"
				#load the image
				icon=ResourceLoader.load(img)
				
				#set the image graphic for the item
				storyItems.set_item_icon(index,icon)
				#set meta data for item
				storyItems.set_item_metadata(index,items[index])

# Renders the equipped sword display and sets its meta data which can be displayed.
func renderEquippedSword():
	#get sword
	var sword = player.get_equipped_sword()
	#draw sword image
	$InventoryBackground/EquippedSword.set_item_icon(0, load("res://images/SWORD_equipped.png"))
	#set metadata for sword
	$InventoryBackground/EquippedSword.set_item_metadata(0, sword)


# When the item menu button drop is pressed it removes the item from the player inventory 
# and updates the visuals accordingly.
func _on_ItemMenu_Button_Drop_pressed():
	#when drop item clicked
	var game_state = get_node("/root/Main").game
	player = game_state.player
	#delete item from player inventory
	game_state.get_player().delete_item(dropItemSlotUseableItem)
	#update metadata
	itemList.set_item_metadata(dropItemSlotUseableItem,null)
	
	#replace image with empty image
	var icon=ResourceLoader.load("res://Inventory/empty_slot.png")
	itemList.set_item_icon(dropItemSlotUseableItem,icon)
	#hide item and dialogue
	$InventoryBackground/EquipSwordBox.hide()
	$InventoryBackground/WindowDialog_itemMenu.hide()




# When an item is selected is loads the relvant pop up with the items 
# information and shows the pop up in the positon where the mouse cursor is.
# @Param index:int -- The index for the item.
func _on_ItemList_item_selected(index):
	 #store item index for other functions
	 dropItemSlotUseableItem=index
	 equipSwordIndex=index
	 var itemData = $InventoryBackground/ItemList.get_item_metadata(index)
		#check to see if the user clicks on the item in the item list
		if(itemData!=null):
			#find and load item image
			var img = "res://images/" + itemData.get_item_name() + ".png"
			var icon=ResourceLoader.load(img)
			#if item is sword, and not currently equipped sword
			if (itemData.get_item_name() == "Sword" && player.get_equipped_sword() != itemData):
				#move equip popup to click location
				$InventoryBackground/EquipSwordBox.set_position(get_viewport().get_mouse_position())
				#set popup title to sword name
				$InventoryBackground/EquipSwordBox.set_title(itemData.get_item_name())
				#set popup image to sword icon 
				$InventoryBackground/EquipSwordBox/ItemMenu_TextureFrame_Icon.set_texture($InventoryBackground/ItemList.get_item_icon(index))
				#set popup text to sword description
				$InventoryBackground/EquipSwordBox/ItemMenu_richText_ItemInfo.set_bbcode(itemData.get_description())
				#show popup
				$InventoryBackground/EquipSwordBox.popup()
			#otherwise, it is a normal item or the currently equipped sword
			else:
				#show the popup
				$InventoryBackground/WindowDialog_itemMenu/ItemMenu_Button_Drop.show()
				#if the item is the eqipped sword, do not show the popup
				if (player.get_equipped_sword() == itemData):
					$InventoryBackground/WindowDialog_itemMenu/ItemMenu_Button_Drop.hide()
				#move popup to click position
				$InventoryBackground/WindowDialog_itemMenu.set_position(get_viewport().get_mouse_position())
				#set popup title to item name
				$InventoryBackground/WindowDialog_itemMenu.set_title(itemData.get_item_name())
				#set popup image to item icon
				$InventoryBackground/WindowDialog_itemMenu/ItemMenu_TextureFrame_Icon.set_texture($InventoryBackground/ItemList.get_item_icon(index))
				#set popup text to item description
				$InventoryBackground/WindowDialog_itemMenu/ItemMenu_richText_ItemInfo.set_bbcode(itemData.get_description())
				#display the popup (will not show if .hide() was called earlier in the sword check)
				$InventoryBackground/WindowDialog_itemMenu.popup()


# When an upgrade item is selected is loads the relvant pop up with the upgrade items 
# information and shows the pop up in the positon where the mouse cursor is.
# @Param index:int -- The index for the upgrade item.
func _on_ItemListUpgradeItem_item_selected(index):
	#store the index for use by other functions
	dropShipUpgradeSlot=index
	#get metadata of item selected
	var itemData = $InventoryBackground/ItemListUpgradeItem.get_item_metadata(index)
	#if item exists
	if(itemData!=null):
		#load item image 
		var img = "res://images/" + itemData.get_item_name() + ".png"
		var icon=ResourceLoader.load(img)
		
		#move popup to click location 
		$InventoryBackground/WindowDialog_ItemListUpgradeItem.set_position(get_viewport().get_mouse_position())
		#set popup title to item name
		$InventoryBackground/WindowDialog_ItemListUpgradeItem.set_title(itemData.get_item_name())
		#set popup image to item icon
		$InventoryBackground/WindowDialog_ItemListUpgradeItem/ItemListUpgradeItem_TextureFrame_Icon.set_texture($InventoryBackground/ItemListUpgradeItem.get_item_icon(index))
		#set popup text to item description
		$InventoryBackground/WindowDialog_ItemListUpgradeItem/ItemListUpgradeItem_richText_ItemInfo.set_bbcode(itemData.get_description())
		#$InventoryBackground/WindowDialog_itemMenu/ItemMenu_Button_Drop._pressed(deleteItem())#
		#_on_ItemMenu_Button_Drop_pressed(index)
		#show popup
		$InventoryBackground/WindowDialog_ItemListUpgradeItem.popup()
			
# When an story item is selected is loads the relvant pop up with the story items 
# information and shows the pop up in the positon where the mouse cursor is.
# @Param index:int -- The index for the story item.
func _on_StoryItems_item_selected(index):
	#get item metadata
	var itemData = $InventoryBackground/StoryItems.get_item_metadata(index)
	#check to see if the user clicks on the item in the item list
	if(itemData!=null):
		#load item image
		var img = "res://images/" + itemData.get_item_name() + ".png"
		var icon=ResourceLoader.load(img)
		
		#move popup to click location
		$InventoryBackground/WindowDialog_ItemListUpgradeItem.set_position(get_viewport().get_mouse_position())
		#set popup title to icon name
		$InventoryBackground/WindowDialog_ItemListUpgradeItem.set_title(itemData.get_item_name())
		#set popup image to item image
		$InventoryBackground/WindowDialog_ItemListUpgradeItem/ItemListUpgradeItem_TextureFrame_Icon.set_texture($InventoryBackground/StoryItems.get_item_icon(index))
		#set popup text to item description
		$InventoryBackground/WindowDialog_ItemListUpgradeItem/ItemListUpgradeItem_richText_ItemInfo.set_bbcode(itemData.get_description())
		#$InventoryBackground/WindowDialog_itemMenu/ItemMenu_Button_Drop._pressed(deleteItem())#
		#_on_ItemMenu_Button_Drop_pressed(index)
		#show popup
		$InventoryBackground/WindowDialog_ItemListUpgradeItem.popup()

# When an equipped sword is selected is loads the relvant pop up with the equipped sword
# information and shows the pop up in the positon where the mouse cursor is.
# @Param index:int -- The index in the inventory that the sword is to be equipped
func _on_EquippedSword_item_selected(index):
	#get item metadata
	var itemData = $InventoryBackground/EquippedSword.get_item_metadata(index)
	#check to see if the user clicks on the item in the item list
	if(itemData!=null):
		#load item image
		var img = "res://images/" + itemData.get_item_name() + ".png"
		var icon=ResourceLoader.load(img)
		
		#move popup to click location
		$InventoryBackground/WindowDialog_ItemListUpgradeItem.set_position(get_viewport().get_mouse_position())
		#set popup title to icon name
		$InventoryBackground/WindowDialog_ItemListUpgradeItem.set_title(itemData.get_item_name())
		#set popup image to item image
		$InventoryBackground/WindowDialog_ItemListUpgradeItem/ItemListUpgradeItem_TextureFrame_Icon.set_texture($InventoryBackground/EquippedSword.get_item_icon(index))
		#set popup text to item description
		$InventoryBackground/WindowDialog_ItemListUpgradeItem/ItemListUpgradeItem_richText_ItemInfo.set_bbcode(itemData.get_description())
		#show popup
		$InventoryBackground/WindowDialog_ItemListUpgradeItem.popup()



# Equips the sword based on the equip sword index of the player inventory and shows the 
# updated graphics of this.
func _on_EquipSword_Button_pressed():
	#get player
	var game_state = get_node("/root/Main").game
	player = game_state.player
	#set player's equipped sword to sword clicked
	game_state.get_player().equip_sword(equipSwordIndex)
	#update the inventory graphics
	renderItemInventory(player.get_items())
	#hide the equip sword popup
	$InventoryBackground/EquipSwordBox.hide()
