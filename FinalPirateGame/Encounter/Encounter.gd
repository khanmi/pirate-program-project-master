extends Node
# An encounter reprenents all the stages with corresponding options to those stage for the player
# to navigate through with the NPC. This includes the setting checking whether the player
# has certain items to progress the encounter and lets the player choose options.

class_name Encounter

# The current stage and all the stages of the encounter.
var stage_scene = load("res://Encounter/Stage.gd")
var stage: Stage setget ,get_stage
var all_stages = [] setget ,get_all_stages
# The setting of the scene and the index where the encounter should be saved at.
var setting:String setget ,get_setting
var save_idx: int setget ,get_save_idx

#returns dictionary of variables for saving
func save():
	var dict = {
		"object": get_filename(),
		"parent": get_parent().get_path(),
		"stage": stage,
		"all_stages": all_stages
	}
	return dict

#Initialises the encounter by generating the first stage of that encounter using the stage_info passed into it.
# @Param tmp_stage_info: Array -- An array containing the encounter information. 
func _init(tmp_stage_info) ->void:
	var stage_info = tmp_stage_info.duplicate(true)
	setting = stage_info[0].split(":  ")[1]
	stage_info.remove(0)
	#split lines into 2D components array
	for i in range(0, stage_info.size()-1):
		all_stages.append(stage_info[i].split("|"))
	if stage_info.size() != 0:
		stage = stage_scene.new(all_stages[0])

# Returns the current stage of the encounter
# @Return stage:Stage -- The current stage of the encounter.
func get_stage() -> Stage:
	return stage

# Sets the current stage of the encounter.
# @Param index:int -- The index within all_stages that the current stage should be initialised with.
func set_stage(index:int) -> void:
	stage = Stage.new(all_stages[index])	
	
# Start the encounter, gives the player any item from the inital stage of the encounter.
# @Param player:Player -- An instance of Player.
func start_encounter(player:Player)->void:
	give_player_stage_item(player) 

# Proceeds to the next stage of the encounter by choosing one of the options of the current stage.
# @Param option:int -- The option number chosen out of the current stages options.
# @Param player:Player -- An instance of player.
func choose_option(option: int, player:Player) ->void:
	# Checks whether the option chosen was valid in the current stages options.
	for check_option in stage.get_options():
		if String(option) == check_option[0]:
			if check_player_has_item_or_resources(check_option, player, true):
				# Loops through all stages in the encounter and finds the corresponding stage to the option chosen.
				for i in all_stages.size():
					if all_stages[i][0] == String(option):
						stage = stage_scene.new(all_stages[i])
						give_player_stage_item(player)
						# If the save_idx has not been set before and the current stage is save, then save_idx is set to the index
						# of the current stage.
						if save_idx == -1:
							if stage.should_save_at_stage():
								save_idx = i
						# Otherwise if the stage_idx has been set (therefore the game has been saved before), if any line after 
						# says not to save, then the save_idx is reset (therefore no longer saved).
						else:
							if !stage.should_save_at_stage():
								save_idx = -1
						break

# Checks whether the player has the item or resource needed to choose a particular option and if so takes the resource from
# the player.
# @Param check_option: Array -- An array containing the option being checked to see if the player has the resource/item.
# @Param player:Player  -- The Player being checked.
# @Param should_update_player_resource:bool -- Whether the player should have the resources taken off them (true if yes).
# @Return bool - True if the Player has the item or resources needed.
func check_player_has_item_or_resources(check_option: Array, player:Player, should_update_player_resource:bool)->bool:
	var need_type_and_value = stage.get_options()[check_option].split("=")
	var has_item: bool = true
	var has_resources: bool = true
	# If the entity being checked is an item, it gets the name of the item and calls the check player has item method.
	if need_type_and_value[0] == "Item":
		var item_needed_name:String = need_type_and_value[1]
		has_item = check_player_has_required_item(item_needed_name, player)
	# Otherwise the entity is a resource therefore it gets the resource type and the corresponding value needed.
	else:
		var resource: String = need_type_and_value[0]
		var resource_amount: int =  int(need_type_and_value[1])
		# Checks the player has enough of that resource and if they do and the call wants the player resources to be taken, it does.
		has_resources = check_player_has_required_resources(resource, resource_amount, player)
		if has_resources && should_update_player_resource:
			player.change_resource_amount(resource, -resource_amount)
	return has_item && has_resources

# Checks whether the player passed in has the item corresponding to the item_needed_name in their inventory.
# @Param Item_needed_name:String -- The name of a story item.
# @Param player:Player --  An instance of the Player class, used to check if they have the item in their inventory.
# @Return bool -- True if the player has the item or there isn't an item needed.
func check_player_has_required_item(item_needed_name: String,  player:Player) ->bool:
	var item_needed: Item
	if item_needed_name.find("null") == -1:
		var story_collection = StoryItemCollection.new()
		item_needed = story_collection.get_story_item(item_needed_name)
	if item_needed == null || player.has_item(item_needed):
		return true
	return false

# Checks whether the player has the same or more resouces of the type passed in compared to the value passed in.
# @Param resource:String -- The type of resource being checked.
# @Param resource_amount:String -- The amount of that resource needed.
# @Param player:Player -- The player being checked.
# @Return bool -- True if the player has enough of the required resource.
func check_player_has_required_resources(resource:String, resource_amount:int, player:Player)->bool:
	var player_resource:int  = player.get_resource_value(resource)
	if player_resource < resource_amount:
		return false
	return true
	
	
# Gives the player the item corresponding to the particular stage of the encounter.
# @Param player:Player -- Instance of the player class, used to give an item to.						
func give_player_stage_item(player:Player) -> void:
	# Gives the player any item that the current stage of the interaction gives the player.
	var item: Item = stage.get_give_item()
	#Checks to make sure the current stage does have an item.
	if stage.get_give_item() != null:
		# Checks if the item is a clue, if so adds it to the player clues
		if item as Clue:
			player.give_clue(item)
			# Otherwise it is a normal item added
		else:
			# If the item is just a pre clue to allow the player to get hold of a clue
			if (item.get_item_name().find("PRE_CLUE") != -1):
				player.add_pre_clue(item)
			# If the item cannot be sold it must be a story item only
			elif (!item as SellableItem):
				player.add_story_item(item)
			else:
				player.add_item(item)
		stage.set_give_item(null)

# The setting of the encounter i.e where it takes place.
# @Return setting:Sring -- The background setting of the encounter.
func get_setting()-> String:
	return setting

# Get all the stages of the current encounter.
# @Return all_stages: Array -- All the stages of the encounter.
func get_all_stages() -> Array:
	return all_stages

# Returns the save index
# @Return save_idx: int -- The index of all_stages that will be started from next time.
func get_save_idx() -> int:
	return save_idx
