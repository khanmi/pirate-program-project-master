extends Node2D

# This scripts is the visual representation of the final (Endgame island) which acts differently to all
# other types of islands. This is used to position the players character on the island once they enter the 
# island into the spawn location and 

# The current state of the game and time which is used for the sounds on the island.
var game_state: PlayGame
var time: float = 3.0

# Called when all nodes on the scene have been initialsied. Sets the player in the correct position
# either at the spawn location if they just got to the island or the position they were last 
# if they paused or went into their inventory. It also updates the overlay to accurately show the
# player the information.
func _ready():
	# Gets the game state and the players character from that state.
	game_state = get_node("/root/Main").game
	var player_character = game_state.player.get_character()
	# Checks whether the player characters position is the default one.
	if (player_character.get_position() == player_character.get_default_position()):
		# If so their position is set to the Position2D point.
		player_character.set_position($Spawn.get_position())
		player_character.set_target($Spawn.get_position())
		# Sets the front end version of the character to have its target and position the same as the backend.
		$Navigation2D/TileMap/Player.get_child(0).set_position(player_character.get_position())
		$Navigation2D/TileMap/Player.get_child(0).set_final_target(player_character.get_position())
	# Updates the overlay.
	$Navigation2D/TileMap/Player/ParallaxBackground/Overlay.update()
	yield(get_tree().create_timer(0.2),"timeout")
	# Tells the front end player character that the scene has loaded therefore ready to start moving.
	$Navigation2D/TileMap/Player.get_child(0).loaded = true

# Updates the visuals on the island in which can be interacted with as they dissapear once they have been interacted with
# by the player if its the statue obstacle.
func update_interactables()-> void:
	# Gets the players ships current island's obstacles.
	var obstacles: Array = game_state.get_player().get_ship().get_current_island().get_obstacles()
	# If the third obstacle. has been interacted with i.e. its now null then hide the visual.
	if obstacles[3] == null:
		$InteractableNPC1.hide()

# The process method is called every frame and updates the interactables on the island and  and plays relevant
# sounds based on the time.
func _process(delta):
	update_interactables()
	# Increases the time 
	time += 0.01
	# Loops through all the sound and plays their sound if they arent already playing.
	for sound in $SeagullSounds.get_children():
		# If its a seagull sound it will only play every 25 seconds.
		if (sound.playing == false && time > 25):
			time = 0
			sound.play(0.0)
	# Otherwise for the background sounds they are constantly playing.
	for sound in $BackgroundSounds.get_children():
		# Plays the sound if its not playing already.
		if (sound.playing == false):
			sound.play(0.0)
