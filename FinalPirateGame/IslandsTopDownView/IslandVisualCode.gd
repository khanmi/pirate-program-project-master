extends Node2D

# This class controls all the visual representations that exist upon an island which isnt the endgame island.
# It is responsible for showing all NPCs that spawn on the island and updating their behaviour 
# based on whats happeneing on the island at the current time and generates all visuals 
# that correspond to the backend of the game including the items that the player can pick up.
# It also controls the sound the player hears when navigating across the island.

# The current state of the game.
var game_state:PlayGame
# The relevant scene for loading visuals of items.
var item_scene = load("res://IslandsTopDownView/Assets/Item.tscn")
# The scenes corresponding to the visuals of all character types that all inherit the same behaviour for movement.
var merchant_visual = load("res://CharacterTopDownView/NPCs/MerchantVisual.tscn")
var navy_visual = load("res://CharacterTopDownView/NPCs/NavyVisual.tscn")
var pirate_visual = load("res://CharacterTopDownView/NPCs/PirateVisual.tscn")
var vendor_visual = load("res://CharacterTopDownView/NPCs/VendorVisual.tscn")
var civilian_visual = load("res://CharacterTopDownView/NPCs/CivilianVisual.tscn")
# A dictionary contains the current island items and what 
# the visuals items are, used to check whether the player has picked up an item by comparing them.
var current_island_items: Dictionary
var visual_island_items: Dictionary
# An array of all visual entities that will need controlling.
var visual_entities: Array
# Whether the scene has loaded and whether the player is leaving.
var loaded: bool = false
var leaving: bool = false
# The time used for the sound only.
var time = 3.0

# Called when the scene is loaded. Sets up the entire initial visuals of the island including
# all the NPCs being created corretly and placed based off the backend model, places all the items and the vendor
# into correct positions and orders all visuals in the heirachy.
func _ready():
	# Gets the game state and updates the overlay to show the correct resource amounts.
	game_state = get_node("/root/Main").game
	$Navigation2D/TileMap/Player/ParallaxBackground/Overlay.update()
	# If the interactable NPC has not already been interacted with i.e. it still exists 
	# then it is loaded.
	if game_state.get_player().get_ship().get_current_island().get_interactable_NPC() != null:
		load_static_entity()
	# Otherwise it frees up the node.
	else:
		$StaticNPC.free()
	# Loads the players characters and the vendor along with the visual entities on the island.
	load_player_character()
	load_vendor()
	add_visual_enitities()
	# Moves the overlay to the front.
	move_child($Navigation2D/TileMap/Player/ParallaxBackground/Overlay,get_child_count())
	# Shows the island items.
	place_island_items()
	# Used to wait for the scene is fully ready before the player character can start moving.
	yield(get_tree().create_timer(0.2),"timeout")
	$Navigation2D/TileMap/Player.get_child(0).loaded = true
	
# Loads the static entity on the island based on the type of NPC that the island that the player is currently
# at has. It also sets its position to the correct place based on the model.
func load_static_entity()->void:
	# Gets the type of NPC this island has as the main interactable NPC.
	var npc_type = game_state.player.get_ship().get_current_island().get_interactable_NPC().get_character_type()
	# If the island is not deserted (as all deserted islands simply have a barrel as the visual).
	if (!game_state.player.get_ship().get_current_island() is DesertedIsland):
		# Loads the correct image and stores it into the variable image depending on the character type.
		var image: StreamTexture
		if (npc_type == "Navy" || npc_type == "Admiral"):
			image  = load("res://CharactersImages/CharacterImages/Navy_idle.png")
		elif (npc_type == "Merchant"):
			image  = load("res://CharactersImages/CharacterImages/Merchant_Idle.png")
		elif (npc_type == "Pirate" || npc_type == "Pirate_captain"):
			image  = load("res://CharactersImages/CharacterImages/Pirate_idle.png")
		elif (npc_type == "Vendor"):
			image = load("res://CharactersImages/TopDownVendorStanding.png")
		elif (npc_type == "Civilian_1" || npc_type == "Civilian_2"):
			image = load("res://CharactersImages/CharacterImages/Civilian_idle.png")
		# Sets the Node in the scene called StaticNPC with the correct image.
		$StaticNPC.get_child(0).texture = image	
	# Sets the position of the static NPC visual to the backend NPCs position on the island.
	$StaticNPC.set_position(game_state.get_player().get_ship().get_current_island().get_interactable_NPC().get_position())

# Loads the vendor if the island is not deserted and sets its position to the correct one
# based off the model.
func load_vendor()->void:
	# If the island is not deserted the island will have a vendor.
	if(!game_state.player.get_ship().get_current_island() is DesertedIsland):
		# Creates a new Sprite and sets the texture.
		var vendor_visual: Sprite = Sprite.new()
		vendor_visual.texture = load("res://CharactersImages/TopDownVendorStanding.png")
		# Scale the image and sets its position based off the backend position.
		vendor_visual.scale = Vector2(2,2)
		vendor_visual.set_position(game_state.get_player().get_ship().get_current_island().get_vendor().get_position())
		# Adds the visual to the scene graph.
		add_child(vendor_visual)

# Loads the player character by setting its position on the island to the correct one based off the backend of the 
# game. 
func load_player_character()->void:
	# Gets the player character.
	var player_character = game_state.player.get_character()
	# If the player characters position is the default one it means they have just reached the island.
	if (player_character.get_position() == player_character.get_default_position()):
		# Therefore their position is set to the spawn position and their target.
		player_character.set_position($Spawn.get_position())
		player_character.set_target($Spawn.get_position())
		# Sets the front end representations position to the same as the backend.
		$Navigation2D/TileMap/Player.get_child(0).set_position(player_character.get_position())
		$Navigation2D/TileMap/Player.get_child(0).set_final_target(player_character.get_position())


# Adds the visual for the island entities into the scene graph and sets their positions to correspond to 
# those of the model of the game so long as the island is an inhabited island. 
# It also creates a visual reprensentation of the island entitise array.
func add_visual_enitities()->void:
	# If the island is inhabited it will load moveable NPC visuals.
	if game_state.get_player().get_ship().get_current_island() is InhabitedIsland:
		var path_points_child_idx: int = 0
		# Creates a local copy of the entities.
		visual_entities.resize(game_state.get_player().get_ship().get_current_island().get_island_entities().size())
		# Loops through the entities on the current island.
		for entity in game_state.get_player().get_ship().get_current_island().get_island_entities():
			# If the entities position is their default one i.e. they havn't moved at all yet 
			if (entity.get_position() == entity.get_default_position()):
				# Sets their position to one of the points on the path indicated by the 
				# path points child index.
				entity.set_position($PathPoints.get_child(path_points_child_idx).get_child(0).get_position())
				entity.set_target($PathPoints.get_child(path_points_child_idx).get_child(0).get_position())
			# It then gets the correct visual represenation of the entity.
			var entity_image = get_entity_image(entity)
			# The packed_scene which contains the correct visual represenation calls the contruct method
			# to set them up correctly and sets their position to the backends version's position.
			entity_image.get_child(0).construct(entity, $PathPoints.get_child(path_points_child_idx).get_child(0).get_position())
			entity_image.get_child(0).set_position(entity.get_position())
			# Stores the entities visual representation in the array.
			visual_entities[path_points_child_idx] = entity_image
			# Adds the visual entities to the scene graph.
			$Navigation2D/TileMap.add_child(entity_image)
			# Increments the path points.
			path_points_child_idx +=1 
	# Sets the loaded variable to true as the entities have been loaded.
	loaded = true

# Gets the Packed scene contains the correct visual for an entity based on the entity passed into the 
# method.
# @Param entity:MoveableNPC -- The entity that will be used to get the correct visual reprsentation.
# @Return :PackedScene -- The scene containing the correct visual reprsentation of the entity.
func get_entity_image(entity: MoveableNPC)->PackedScene:
	# If the character is a pirate then the pirate visual is returned.
	if (entity.get_character_type() == "Pirate" || entity.get_character_type() == "Pirate_captain"):
		return pirate_visual.instance()
	# If the character is Navy then the navy scene is returned.
	elif (entity.get_character_type() == "Navy" || entity.get_character_type() == "Admiral"):
		return navy_visual.instance()
	# if the character is a merchant then the merchant scene is returned.
	elif (entity.get_character_type() == "Merchant"):
		return merchant_visual.instance()
	# If the character is a vendor then the vendor visual is returned.
	elif (entity.get_character_type() == "Vendor"):
		return vendor_visual.instance()
	# If the character is a Civilian then the civilian scene is returned.
	elif (entity.get_character_type() == "Civilian_1" || entity.get_character_type() == "Civilian_2"):
		return civilian_visual.instance()
	return civilian_visual.instance()


# Places the visuals onto the scene based of the items on the backend current island. Their positions
# are also set based on the backend of model of the island.
func place_island_items() -> void:
	var ground_spaces = game_state.get_player().get_ship().get_current_island().get_ground_spaces()
	# Loops through each space from the backends grounds spaces.
	for space in ground_spaces.keys():
		# Gets the entity at that space from the dictionary.
		var entity = ground_spaces.get(space)
		# If the entity is an Item
		if  entity is Item:
			# An instance of the item scene is created 
			var item_image = item_scene.instance()
			# Gets the correct image of the item.
			var img: String = item_visual_choice(entity)
			# If the image doesnt exist yet
			# Then a temp image is set to the string
			if ! load(img):
				img = "res://images//Title.png"
			# Sets the visual sprite with the correct image and sets its position.
			item_image.get_node("Sprite").set_texture(load(img))
			item_image.set_position(space)
			# Adds the visual reprsentation to the visual items array 
			visual_island_items[space] = item_image
			# Adds the entity into the current island items used to compare whether an item has been picked up
			# on the backend of the game.
			current_island_items[space] = entity
			# Adds the item to the scene.
			add_child(item_image)
			
			
# Returns the string reprsentating the path to the correct image corresponding to the item passed in.
# @Param item:Item -- The item in which needs a visual represenation.
# @Return img:String -- The image path.		
func item_visual_choice(item:Item) -> String:
	var	img = "res://images/" + item.get_item_name() + ".png"
	return img

# Updates the visual for all the items on the island as the player may have picked them up
# therefore no longer exist on the backend of the island.
func update_visuals_for_items_on_island() -> void:
	# Gets the ground spaces with their items.
	var items = game_state.get_player().get_ship().get_current_island().get_ground_spaces()
	# Loops through the spaces
	for space in current_island_items.keys():
		# If the space contains an item for the copy this class has and the backend no longer contains at item 
		# there.
		if (current_island_items[space] is Item):
			if current_island_items[space] != items[space]:
				# Then the overlay is informed the player has picked up an item therefore should tell the player
				$Navigation2D/TileMap/Player/ParallaxBackground/Overlay.picked_up_item(current_island_items[space])
				# The local copy of the items array is then updated and the item visual is removed.
				current_island_items[space] = items[space]
				visual_island_items[space].free()
			# Otherwise if its the same it means the player cannot pick up the item if they are
			# within the a distance of 50 therefore the overlay is called to inform the player.
			elif ($Navigation2D/TileMap/Player.get_child(0).get_position().distance_to(space) < 50):
				$Navigation2D/TileMap/Player/ParallaxBackground/Overlay.cant_pick_up_item(current_island_items[space])

# Checks whether the player is leaving the island i.e. are within a distance of 140 to the exit node.
func check_player_leaving()->void:
	# If the player isnt already leaving and their distance to the exit is less than 140.
	if (!leaving && game_state.get_player().get_character().get_position().distance_to($Exit.get_position())< 140):
		# The UI is informed they are leaving and updates the front end as this is a controller.
		leaving = true
		get_node("/root/Main").game.leave_island()	

	
# Process called every cycle and is navigates moving around the moveable entities on the island, checking whether the 
# player is leaving the island and playing the correct sounds.
func _process(delta):
	# Checks whether the player is leaving and moves the entities.
	check_player_leaving()
	move_entities(delta)
	# If the scene is loaded then the visuals for the items are checked to update them.
	if loaded:
		update_visuals_for_items_on_island()
	time += 0.01
	# Loops through the seagull sounds and plays the sound every 60 seconds if its not already playing.
	for sound in $SeagullSounds.get_children():
		if (sound.playing == false && time > 60):
			time = 0
			sound.play(0.0)
	# Loops through the background sounds and play the sound if its not already playing.
	for sound in $BackgroundSounds.get_children():
		if (sound.playing == false):
			sound.play(0.0)

# Moves the entities on the island if the island is inhabited and updates the backend if the 
# backend NPC is not within a close proximity to the player.			
func move_entities(delta)->void:
	var path_points_child_idx: int = 0
	# If the island is inhabited it can have moveable entities.
	if game_state.get_player().get_ship().get_current_island() is InhabitedIsland:
		# Loops through the entities on the island and if they are not null.
		for entity in game_state.get_player().get_ship().get_current_island().get_island_entities():
			if entity != null:
				# Gets the points the entity will move between 
				var path_points = $PathPoints.get_child(path_points_child_idx)
				var pointA = path_points.get_child(0)
				var pointB = path_points.get_child(1)
				# Calls the move entity method with the entity, relevant points and the delta time.
				move_entity(entity,pointA, pointB, path_points_child_idx,  delta)
			# Moves to the next child node.
			path_points_child_idx+=1

# Moves a single entity between the points passed in as long as the NPC is not within a close proximity to the 
# player already i.e. the NPC backend should not be triggered.
# @Param entity:MoveableNPC -- The entity corresponding to the visual.
# @Param pointA:Position2D -- A Position on the island.
# @Param pointB:Position2D -- A position on the island thats not the same 
# @Param path_points_child_idx:int -- The index of the path points the entity is moving between
# @Param delta:float -- The delta cycle.
func move_entity(entity:MoveableNPC, pointA:Position2D, pointB:Position2D, path_points_child_idx:int, delta)->void:
	if pointA != null && pointB != null:
		# If the backend entity is not triggerd then the position will be controlled by the front end.
		if entity.triggered == entity.STATE.NOT_TRIGGERED:
			# If the NPC is close to Point A then it will go to pointB
			if entity.get_position().distance_to(pointA.get_position()) < 50:
				entity.set_target(pointB.get_position())
			# Otherwise its target will be pointA
			elif entity.get_position().distance_to(pointB.get_position()) < 50 || entity.get_target() != pointB.get_position():
				entity.set_target(pointA.get_position())
		# Sets the front ends target position based off the backend and calls the front ends physics process to
		# let the entity move.
		visual_entities[path_points_child_idx].get_child(0).set_target(entity.get_target())
		visual_entities[path_points_child_idx].get_child(0)._physics_process(delta)
