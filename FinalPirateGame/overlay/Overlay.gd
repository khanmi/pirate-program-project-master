extends Control

# Used to control the display of the overlay for the player to see their resoucres and resource changes, along 
# with the current in game time and their the ability to click on their inventory button.

# Used to support he overlay with the timing of showing changes.
var overlay_support:OverlaySupport

# Starts the day night cycle on any scene with the overlay when the scene is loaded.
func ready():
	$AnimationPlayer.play("day_and_night")

# Calls every frame and is used to check whether the resoucres changes along with picking up items should stop displaying
# after a period of time.
# @Param delta -- The time cycle.
func _process(delta):
	# If the overlay support hasn't loaded then it doesn't attempt to complete the tasks.
	if overlay_support == null:
		return
	# If its been 5 seconds since an item was picked up then the text is stopped displaying.
	if (DayNightCycle.get_time() - overlay_support.get_time_since_picked_up_item() > 5):
		hide_pick_up_item_text()
	# If the resources has changed and the time since the UI was last update was more than 5 seconds, it updates the UI so the resource difference disappears.
	if (overlay_support.changed && DayNightCycle.get_time() - overlay_support.get_last_update_time() > 5):
		#yield(get_tree().create_timer(8),"timeout")
		var child_index = 3
		# Loops through all the overlay child nodes from the 2nd to the 6th to update hide them (these are the value change indicators).
		while (child_index < 8):
			get_child(child_index).hide()
			child_index += 1
		# Updates so the version of the resources stored is now the recent version.
		#prev_resources = $"/root/Main".game.player.get_resources().duplicate(true)
		overlay_support.set_previous_resources($"/root/Main".game.player.get_resources())
		overlay_support.set_changed(false)

	
# Updates the overlay with the values corresponding to the resource values and if the resources has changed 
# make the resource indicator show the difference in red or green depending on whether 
# it was a positive or negative change.
func update():
	# If the overlay support is null then it creates the overlay support.
	if overlay_support == null:
		overlay_support = get_node("/root/Main").view.overlay_support
	# Gets the string representationf of the values of each resource and shows them on the overlay.
	$Gunpowder_Value.text = String($"/root/Main".game.player.get_resource_value("Gunpowder"))
	$Repairs_Value.text = String($"/root/Main".game.player.get_resource_value("Repairs"))
	$Gold_Value.text = String($"/root/Main".game.player.get_resource_value("Gold"))
	$Crew_Value.text = String($"/root/Main".game.player.get_resource_value("Crew"))
	$Rum_Value.text = String($"/root/Main".game.player.get_resource_value("Rum"))
	var child_index = 3 # Used for the child nodes of the Overlay scene to show the changes of resources.
	# Loops through the resources.
	for key in $"/root/Main".game.player.get_resources().keys():
		# Checks to make sure the resources have been set for the key.
		if overlay_support.get_previous_resources().get(key) != null && key != "Morale":
			# Checks whether the version of resources stored/previous is the same as the current.
			if $"/root/Main".game.player.get_resource_value(key) != overlay_support.get_previous_resources().get(key):
				overlay_support.set_last_update_time(DayNightCycle.get_time())
				overlay_support.set_changed(true)
				#If not it works out the difference and whether the player gained or lost resources. 
				# If gain the colour is set to green and a plus symbol is added.
				var result : int = $"/root/Main".game.player.get_resource_value(key) - overlay_support.get_previous_resources().get(key)
				var result_text: String = String(result)
				if (result > 0):
					result_text = "+" + result_text
					get_child(child_index).add_color_override("font_color", Color("14b950"))
				# Otherwise the colour of the text is set to red indicating lose.
				else:
					get_child(child_index).add_color_override("font_color", Color("ea2121"))
				# Gets the child node according to the index and sets its text.
				get_child(child_index).text = result_text
				get_child(child_index).show()
			child_index += 1
			

# Displays the inventory of the player.
func _on_TextureButton_pressed():
	$"/root/Main".game.display_inventory()

# Displays the shop.
func _on_ShopButton_pressed():
	$"/root/Main".game.displayShop()
	
# Hides the pick up item text.
func hide_pick_up_item_text():
	$PickedUpItemName.hide()
	$PickedUpText.hide()
	
# Displays a message to the player informing them they picked up an item and showing the text.	
# @Param item:Item -- The item name that will be displayed and has been picked up.
func picked_up_item(item:Item):
	# Sets the time since an item was picked up last.
	overlay_support.set_time_since_picked_up_item(DayNightCycle.get_time())
	# Sets the text and the name to the name of the item and a short message.
	$PickedUpText.text = "You have picked up a"
	$PickedUpItemName.text = item.get_item_name()
	# Shows the two lots of text.
	$PickedUpItemName.show()
	$PickedUpText.show()

# The player cannot pick up the item therefore displays an appropriate message as to why.
# @Param item:Item -- The item being picked up.
func cant_pick_up_item(item:Item):
	overlay_support.set_time_since_picked_up_item(DayNightCycle.get_time())
	# If the item is a gun,sword or explosive barrel than it tells the player they have reached the limit 
	# for that item.
	if (item.get_item_name() == "Gun" || item.get_item_name() == "Sword" || item.get_item_name() == "Explosive Barrel"):
		$PickedUpText.text = "Max capacity reached for"
		$PickedUpItemName.text = item.get_item_name() + "s."
		$PickedUpItemName.show()
	# Otherwise it says their inventory is full.
	else:
		$PickedUpText.text = "Inventory full"
	$PickedUpText.show()
