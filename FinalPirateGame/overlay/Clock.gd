extends Node

# Used to update the clock visuals.

# Plays the day night cycle and jumps to the correct position.
func _ready():
	#find time from the DayNightCycle
	var time = DayNightCycle.get_time()
	var animation_player = $AnimationPlayer
	#start playing the day/night animation
	animation_player.play("day_and_night")
	#jump to the correct part of the animation
	animation_player.seek(time,false)
	