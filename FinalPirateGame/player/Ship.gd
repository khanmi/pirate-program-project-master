extends Node

# The ship reprenting a Ship which moves along the map containing a position, target to get to, the island its at and speed of the ship.
class_name Ship
# The default speed.
const DEFAULT_SPEED = 100
# How quick the ship will travel
var speed = 100 setget set_speed, get_speed
# The target position of the ship.
var target = Vector2() setget set_target, get_target
# The island the ship is at.
var current_island : Island setget set_current_island, get_current_island
# The sector the ship is at.
var current_sector : int = 1 setget set_current_sector, get_current_sector
#var island_at : Island setget set_island_at, get_island_at
var velocity = Vector2()
# The current position of the ship.
var position = Vector2() setget set_position, get_position
# Constructor of the Ship class.
func _init():
	pass

# Set the speed of the ship with the value passed in.
# @Param percent:int -- the percent increase of speed.
func set_speed(percent: int) -> void:
	speed = DEFAULT_SPEED * percent / 10 as int

# Get the current speed of the ship.
# @Return speed:int -- The speed of the ship.
func get_speed() -> int:
	return speed

# Set the position of the ship, passing in the x and y coordinates.
# @Param pos:Vector2 -- the position of the ship.
func set_position(pos: Vector2) -> void:
	position = pos

# Get the current position of the ship.	
# @Return position:Vector2 -- the position of the ship.
func get_position() -> Vector2:
	return position
	
# Get the target destination of the ship.
# @Return target:Vector2 -- The target the ship is going to.
func get_target() -> Vector2:
	return target

# Set the target position of the ship to the value passed in.
# @Param tmp_target:Vector2 -- the target of the ship .
func set_target(tmp_target: Vector2) ->void:
	target = tmp_target
	
# Set the current island of the ship to the value passed in.
# @Param tmp_island:Island -- the island the ship will be at.
func set_current_island(tmp_island) -> void:
	current_island = tmp_island
	
# Get the current island of the ship.
# @Return current_island:Island -- the island the ship is at.
func get_current_island() -> Island:
	return current_island

# Set the current sector of the ship
# @Param tmp_sector:int -- the sector the ship will be at.
func set_current_sector(tmp_sector : int) -> void:
	current_sector = tmp_sector

# Gets the current sector of the ship.
# @Return current_sector:int -- the current_sector of the ship.
func get_current_sector() -> int:
	return current_sector

# Moves the ship towards the desired target location by a unit vector from its current to target position.
# @Param delta -- The delta value i.e. clock cycle value.
func move_ship(delta):
	if position.distance_to(target) > 10:
		velocity = (target - position).normalized() * speed
		position += velocity * delta
	else:
		position = target
	
