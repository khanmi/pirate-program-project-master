extends Node

class_name Player

# Dictionary of each resource thw player has in the game.
var resources = {"Gold":100, "Gunpowder":100, "Repairs":100, "Crew":100, "Rum":100, "Morale":100} setget , get_resources
# The player ship and charatcer
var ship : Ship setget ,get_ship
var character: PlayerCharacter setget ,get_character
# An aray of the clues the player has and their index.
var clues: Array setget ,get_clues
var clue_index: int = 0
# The player type i.e. pirate, navy, merchant.
var type: String setget set_type, get_type
# The ship upgrades the player has and the current count of index.
var ship_upgrades: Array 
var num_ship_upgrades: int = 0
# The items the player has and an index of how many they currently have.
var items: Array setget ,get_items 
var numItems: int = 0
# Pre clues used invisible behind the scenes to ensure the player has completed certain encounters to get the clues
var pre_clues:Array setget ,get_pre_clues
var pre_clues_count:int = 0
# Story items inventory
var story_items:Array setget ,get_story_items
var story_items_count:int = 0
# Whether the player has the treasure.
var has_treasure:bool setget set_has_final_treasure, has_final_treasure
# The different states the player could be in.
enum Player_states{
	IDLE,
	INTERACTING,
	LEAVING_ISLAND,
	ON_ISLAND
	ENTERING_ISLAND,
	LEFT_ISLAND,
	SHOPPING,
	BATTLING,
	SELECTING_CHARACTER
}
# The current state is default to IDLE.
var current_state: int = 0
# Used to determine the last state the player was in.
var prev_state: int = current_state
# The state of the player two states ago, useful for when returning back to the island after an interaction and a battle within that.
var check_state: int
# The sword the player has equiped. If null it is the default one.
var equipped_sword:UseableItem
# The number of swords, guns and gunpowder_barrels the player has
var item_counts:Dictionary = {"Sword": 1, "Gun": 0, "Explosive Barrel": 0}
# Max of each item type
var MAX_ITEMS_COUNT:Dictionary = {"Sword": 2, "Gun": 2, "Explosive Barrel": 1}


# Returns dictionary of variables for saving
# @Return dict:Dictionary -- The dictionary of save data.
func save():
	var dict = {
		"filename": get_filename(),
		"parent": get_parent().get_path(),
		"pos_x": character.position.x,
		"pos_y": character.position.y,
		"resources": resources,
		"character": character,
		"ship": ship,
		"clues": clues,
		"type": type,
	}
	return dict


# Constructor for the player, settting up the ship, clues, items and ship_upgrades.
func _init() -> void:
	add_to_group('persistent')
	# Sets up the ship and the player character.
	ship = Ship.new()
	character = PlayerCharacter.new(Vector2(0,0))
	# Sets up the clues
	clues = []
	clues.resize(3)
	# Sets up the items and ship_upgardes arrays, to the correct size.
	items=[]
	ship_upgrades=[]
	# Resize the arrays to the correct size.
	ship_upgrades.resize(3)
	items.resize(6)
	# Sets the size for the pre clues
	pre_clues = []
	pre_clues.resize(10)
	# Sets the story items array to 
	story_items = []
	story_items.resize(9)
	# Spawns the player with a default sword.
	var sword: UseableItem = UseableItem.new("Sword", 1)
	equipped_sword = sword
	
	

# Get this entire dictionary of resources containing key-value pairs.
# @Return resources:Dictionary -- The player resouces dictionary.
func get_resources() -> Dictionary:
	return resources

# Returns player's upgrades
# @Return ship_upgrades:Array -- The player ship upgrades array.
func get_ship_upgrades() -> Array:
	return ship_upgrades
	
# Change the player ship_upgrades to the ones passed in.
# @Param new_ship_upgrades:Array -- The array of ship upgrades the player will now have.
func set_ship_upgrades(new_ship_upgrades: Array):
	ship_upgrades = new_ship_upgrades

# Get the value for a specific resource.
# @Param resource:String -- The name of a resouce in the resouces dictionary
# @Return resouces[resouce]:int -- The number of resources for a specific resource.
func get_resource_value(resource: String) -> int:
	return resources[resource]

# Set the value for a specific resource.
# @Param resource:String -- The name of the resource to be set
# @Param value:int -- The number this resource value should be 
# @Return resouces[resouce]:int -- The number of resources for a specific resource.
func set_resource_value(resource: String, value: int) -> void:
	resources[resource] = value

# Change the amount of a specific resource to a specified value.
# @Param key: String -- A key used for the resouces dictionary
# @Param value: int -- The value that the key into the dictionary will have.
func change_resource_amount(key: String, value: int) -> void:
	# Adjusts the value based on the players type and upgrades.
	resources[key] += adjustResource(key, value)
	if (resources[key] < 0):
		resources[key] = 0

# Adjust a resouce value for the key based on the players type and upgrades they have.
# @Param key: String -- A key used for the resouces dictionary
# @Param value: int -- The value that the key into the dictionary will have.
# @Return :int -- The new value thats been calculated. 
func adjustResource(key: String, baseValue: int) -> int:
	#if gaining
	if (baseValue > 0):
		#increase amount gained
		return int(baseValue * (1.0 + characterModifier(key)) * (1.0 + upgradeModifier(key)))
	#if losing
	else:
		#decrease amount lost
		return int(baseValue * (1.0 - characterModifier(key)) * (1.0 - upgradeModifier(key)))

# Gets the type of the player
# @Return type:String -- The type the player is.
func get_type() -> String:
	return type

# Sets the type for the player
# @Param new_type:String -- The type the player will become.
func set_type(new_type: String):
	type = new_type

# Calculates a value which is a float (representing a percentage) based on the key and the player type.
# @Param key:String -- The key to the resources.
# @Return :float -- The float version of the percentage.
func characterModifier(key: String) -> float:
	# If the player is a pirate and the resouces is crew, a 15% bonus value is returned
	if ((get_type() == "Pirate") and (key == "Crew")):
			return 0.15
	# If the player is a merchant and the resouces is gold, a 15% bonus value is returned
	elif ((get_type() == "Merchant") and (key == "Gold")):
			return 0.15
	# If the player is Navy and the resouces is Gunpowder, a 15% bonus value is returned
	elif ((get_type() == "Navy") and (key == "Gunpowder")):
			return 0.15
	else:
		return 0.0

# Calculates the total amount of modifying values based on the player items for a particular resource type.
# @Param key:Strign -- A resouce type.
# @Return total_modifier:float -- The total percentage the player item will modify a value corresponding to the resouce type passed in.
func upgradeModifier(key: String) -> float:
	var total_modifier: float = 0
	for item in items:
		if item is UpgradeItem:
			if (item.get_affected_resource() == key):
				total_modifier += item.get_affect()
	for upgrade in ship_upgrades:
		if(upgrade != null):
			if (upgrade.get_affected_resource() == key):
				total_modifier += upgrade.get_affect()
	return total_modifier

# Returns the player instance of Ship
# @Return ship:Ship -- The players ship.
func get_ship() -> Ship:
	return ship

# Returns the player character
# @Return character:PlayerCharacter -- The players character.
func get_character() -> PlayerCharacter:
	return character
	
# Gives the player the clue passed in so long as they have space for another clue.
# @Param clue:Clue -- The clue to be added to the players inventory.
# @Return :bool -- Whether the clue was added.
func give_clue(clue:Clue) -> bool:
	# If the clues is null then return false to indicate a clue hasn't been added.
	if clue == null:
		return false
	# If the clue index is less than the size and its currently null i.e. there is room for another clue.
	if clue_index < clues.size() && clues[clue_index] == null:
		# Add the clue to the player clues and increment the clue index.
		clues[clue_index] = clue
		clue_index+=1
		# Return true as its been added.
		return true
	# Otherwise return false.
	return false

# Return the clues array.
# @Return clues:Array -- The player clues.
func get_clues() -> Array:
	return clues

# Return the items array.
# @Return items:Array -- The player items.
func get_items() -> Array:
	return items

# Add an item to the players inventory so long as its not already full.
# @Param item:Item -- The item being added.
# @Return :bool -- Whether the item was added or not.
func add_item(item:Item) -> bool:
	#	checks to see if there is size in the array then adds it at the end and returns true
	#else return false
	if numItems<items.size():
		# Checks whether the item is of one of these specific types, if so if the player already has the max of the item
		# type it returns false
		if (item_counts.has(item.get_item_name())):
			if (item_counts.get(item.get_item_name()) >= MAX_ITEMS_COUNT.get(item.get_item_name())):
				return false
			# Otherwise it increments the number they have by 1
			else:
				print("should be here")
				item_counts[item.get_item_name()] = item_counts.get(item.get_item_name()) + 1
		# Sets the items at item index to the new item and returns true
		items[numItems]=item
		numItems+=1
		return true
	else:
		return false
		
# Adds a ship upgrade to the players inventory if it's not already full
# @Param upgrade_item: UpgradeItem -- The ship upgrade to be added.
# @Return :bool -- Whether the item was added.
func add_ship_upgrade(upgrade_item: UpgradeItem)->bool:
	# Checks to see if there is size in the array then adds it at the end and returns true.
	if num_ship_upgrades<ship_upgrades.size():
		ship_upgrades[num_ship_upgrades]=upgrade_item
		num_ship_upgrades+=1
		return true
	else:
		return false
	
# Removes an item from the player inventory.
# @Param index:int -- The position in the player items array to remove the item from.
# @Return :bool -- Whether an item was removed.
func delete_item(index:int) -> bool:
	#check to see if the index  is bigger than the size of array
	#if it is it will return false if not it will remove element from that index and return true
	if index>items.size() or index<0 or items[index] == null or items[index] == equipped_sword:
		return false
	else:
		# If the item being removed is a special item, it updates the item count.
		if item_counts.has(items[index].get_item_name()):
			item_counts[items[index].get_item_name()] = item_counts.get(items[index].get_item_name()) - 1
		# Removes the item from the players inventory
		items.remove(index)
		items.append(null)
		numItems=numItems-1
		return true

# Checks whether the player has the item passed in.
# @Param find_item:Item -- The item being checked.
# @Return :bool -- Whether the player has that item, true if yes.
func has_item(find_item:Item) -> bool:
	print(find_item.get_item_name())
	var items_to_search:Array
	# If PRE CLUE cannot be found in its name then it is a normal item
	if (find_item.get_item_name().find("PRE_CLUE") == -1):
		# If the item is a clue then search the clues array else the items
		if (find_item.get_item_name().find("Clue") != -1):
			items_to_search = clues
		# Otherwise checks whether the item is sellable as if not then its a story item.
		elif (!find_item as SellableItem):
			items_to_search = story_items
		else:
			items_to_search = items
	else:
		# Otherwise its a pre clue therefore look through pre clues.
		items_to_search = pre_clues
	# Loops through the items and checks if the item is contained.
	for item in items_to_search:
		if item != null:
			if item.equals(find_item):
				return true
	return false

# Finds index of item in player inventory, can be used along with delete_item() to delete specific item.
# @Param item:Item -- The item that you want to find its index position in the items array.
# @Return :int -- The index of the item in the items array, -1 if it doesn't exist. 
func getIndexOfItem(item: Item) -> int:
	for i in range(get_items().size()):
		if item == get_items()[i]:
			return i
	return -1

# Whether the player has the final treasure
# has_final_treasure: bool -- True if the player has the treasure.
func has_final_treasure() -> bool:
	return has_treasure

# Sets the player var has final treasure.
# @Param has_treasure:bool -- Whether the player has the treasure or not.
func set_has_final_treasure(tmp_has_treasure: bool) -> void:
	has_treasure = tmp_has_treasure

# Changes the state of the player, along with its previous state and its check state (the current state two states ago.)
# @Param state:int -- The state the player is currently.
func change_state(state:int)->void:
	prev_state = current_state
	check_state = prev_state
	current_state = state

# Equips a sword based on the index passed in from their inventory and swaps the sword
# in the inventory with the currently equipped sword.
# @Param index:int -- The position in the inventory.
func equip_sword(index:int)->void:
	# If the item from the index is actually a sword.
	if items[index].get_item_name() == "Sword":
		# Swaps the sword in the inventory with their equipped sword.
		var sword:Item = items[index]
		items[index] = equipped_sword
		equipped_sword = sword

# Gets the equipped sword.
# @Return equipped_sword:Item -- The sword equipped.
func get_equipped_sword()->Item:
	return equipped_sword

# Adds the pre clue to the players pre clues array if there is space.
# @Param pre_clue:Item -- The pre clue item being added to the array.
# @Return :bool -- Whether the item was added.
func add_pre_clue(pre_clue:Item)->bool:
	# Checks to see if there is size in the array then adds it at the end and returns true.
	if pre_clues_count < pre_clues.size():
		pre_clues[pre_clues_count]=pre_clue
		pre_clues_count+=1
		return true
	# Otherwise returns false.
	else:
		return false

# Adds a story item to the players story item inventory.
# @Param item:Item -- The item being added.
# @Return :bool -- Whether the item was added.		
func add_story_item(item:Item)->bool:
	# Checks to see if there is size in the array then adds it at the end and returns true.
	if story_items_count < story_items.size():
		story_items[story_items_count]=item
		story_items_count+=1
		return true
	# If not it returns false.
	else:
		return false

# Gets the pre clues array
# @Return pre_clues:Array -- The pre_clues
func get_pre_clues()->Array:
	return pre_clues

# Gets the story items array
# @Return story_items:Array -- The story items.
func get_story_items()->Array:
	return story_items
	
