extends Node

# The map class acts as the main loop of the game. It contains a number of sectors which contain a different number of islands.
# It keeps track of moving the player ship and the navy ship along from island to island.
class_name Map

# Each sector in the game as the key with the corresponding array of islands
var sectors : Dictionary = {1 : [], 2: []} setget ,get_sectors
# The size of each sector apart from the first tutorial sector (1)
const SECTOR_SIZE : int = 14
# The player and navy ships
var player_ship : Ship setget ,get_player_ship
var navy_ship : Ship setget ,get_navy_ship
# The distance the ship will be away from the island they are currently at.
const SHIP_DISTANCE_FROM_ISLAND = 134
# The centre of the map
var sector_centre: Vector2
# Boolean containg whether the navy ship should be moving.
var should_move_navy:bool = false
# The current number of turns, used for navy movement.
var num_turns: int = 0
# The amount of days until the navy ships begins to move
const NAVY_START_MOVING: int = 2

# Encounters dictionary containing the island types as keys and the values being a dictionary. The inner dictionary is the names of islands corresponding to the island type with the value
# being another dictionary in which they key is the occurences of when the encounter happens. The value is an array where encounters are stored with the different line of the encounter.									
var encounter_dictionary: Dictionary = {
	"Pirate": {"Thieves Cove": {"OnEnter": [],"OnIsland": [], "OnExit": [], "Vendor": [], "Moveable_NPC":[]},
				"Flintlock Shores": {"OnEnter": [],"OnIsland": [], "OnExit": [], "Vendor": [], "Moveable_NPC":[]},
				"Shovel Shores": {"OnEnter": [],"OnIsland": [], "OnExit": [], "Vendor": [], "Moveable_NPC":[]},
				"Lucky Landing": {"OnEnter": [],"OnIsland": [], "OnExit": [], "Vendor": [], "Moveable_NPC":[]},
				"Backstab Bay": {"OnEnter": [],"OnIsland": [], "OnExit": [], "Vendor": [], "Moveable_NPC":[]},
				"Narvera Isle": {"OnEnter": [],"OnIsland": [], "OnExit": [], "Vendor": [], "Moveable_NPC":[]},
				},
	"Merchant": {"Merchant Castaway":{"OnEnter": [],"OnIsland": [], "OnExit": [], "Vendor": [], "Moveable_NPC":[]},
				"Vosea":{"OnEnter": [],"OnIsland": [], "OnExit": [], "Vendor": [], "Moveable_NPC":[]},
				"Willhelm":{"OnEnter": [],"OnIsland": [], "OnExit": [], "Vendor": [], "Moveable_NPC":[]},
				},
	"Deserted": {"Stranded Isle":{"OnEnter": [],"OnIsland": [], "OnExit": [], "Vendor": [], "Moveable_NPC":[]},
				"Silent bay":{"OnEnter": [],"OnIsland": [], "OnExit": [], "Vendor": [], "Moveable_NPC":[]},
				"Silent Starboard":{"OnEnter": [],"OnIsland": [], "OnExit": [], "Vendor": [], "Moveable_NPC":[]},
				},
	"Navy": {"Scallywags Downfall":{"OnEnter": [],"OnIsland": [], "OnExit": [], "Vendor": [], "Moveable_NPC":[]},
				"Royal Shores":{"OnEnter": [],"OnIsland": [], "OnExit": [], "Vendor": [], "Moveable_NPC":[]},
				"Sunken Sands":{"OnEnter": [],"OnIsland": [], "OnExit": [], "Vendor": [], "Moveable_NPC":[]},
				},
	"Endgame": {"Barsea's hideaway":{"Obstacle1Correct": [],"Obstacle1InCorrect": [], "Obstacle2": [], "Onstacle3": []}
	}
}


var loaded_island_name = null
var loaded_ship_position = null


#returns dictionary of variables for saving
func save():
	var dict = {
		'filename': get_filename(),#get_script(),#get_path_to(self),#get_filename(),
		'parent': get_parent().get_path(),
		'sectors': sectors,
		'player_ship': player_ship,
		'navy_ship': navy_ship,
		#'sector_centreX': sector_centre[0],
		#'sector_centreY': sector_centre[1],
		'encounter_dictionary': encounter_dictionary
	}
	return dict


# Creates an instance of map setting up all the islands and the players and navy ships are set to the first island. The speed
# of the navy ship is also set
# @Param tmp_sector_centre -- The centre of sector so that island can be placed around it
# @Param tmp_player_ship -- An instance of the Ship class to be the player ship.
# @Param player -- An instance of the Player class.
func _init(tmp_sector_centre: Vector2, tmp_player_ship: Ship, player:Player) -> void:
	# Call to set up the dictionary containing all encounters.
	set_up_encounter_dictionary()
	# Initialises the class instances' attributes.
	sector_centre = tmp_sector_centre
	player_ship = tmp_player_ship
	navy_ship = Ship.new()
	# Calls to set up the sectors of the map.
	create_sectors(player)
	# Sets the current sector of the game to initiate at 1 and set the island the player starts at to the first island.
	set_player_sector_and_island(1, 1)
	navy_ship.set_current_sector(1)
	navy_ship.set_current_island(sectors[1][0])
	# Set the position of the ship to the first island - the distance the ship stays away from the islands * 2.
	# Therefore behind the player ship at the start of the game.
	navy_ship.set_position(sectors[1][0].get_position() - (Vector2(SHIP_DISTANCE_FROM_ISLAND, 0) * 2))
	# Set the target to be the island they are at so the ship doesn't move.
	navy_ship.set_target(navy_ship.get_position())
	navy_ship.set_speed(5)

func _ready():
	load_data()

func load_data():
	
	var save_game = File.new()
	var save_file = true
	if not save_game.file_exists("user://savedata.save"):
		save_file = false
	
	if save_file:
		save_game.open("user://savedata.save", File.READ)
		var data = parse_json(save_game.get_as_text())
		save_game.close()
		loaded_ship_position = Vector2(data['ship_x'], data['ship_y'])
		loaded_island_name = data['island']
		DayNightCycle.time = data['time']
		print("Current Time: " + str(DayNightCycle.time))

func _process(delta):
	load_data()	
# Sets up the encounter dictionary by using the FileReader class's read method returning the entire "Encounters.tres file" and placing in the nested dictionaries based
# on their island type, then name, then occurence.
func set_up_encounter_dictionary() -> void:
	var encounter_deck = FileReader.new().read("res://Encounter/encounters.tres")
	# Loops throuhg each encounter and splits the block up into lines.
	for encounter in encounter_deck:
		var deck_choice = encounter.split("\n")
		# The first line is the island type, the second line is the island name and the third line is how the encounters occurs.
		var island_type: String = deck_choice[0]
		var island_name:String = deck_choice[1]
		var occurence: String = deck_choice[2]
		# Removes the attributes listed above from the encounter (as this is just data about the encounter).
		for i in range(3):
			deck_choice.remove(0)
		# Checks to see if the occurence is moveable_npc as if so, it will append it to an array rather than overwrite it.
		if (occurence == "Moveable_NPC"):
			encounter_dictionary[island_type][island_name][occurence].append(deck_choice)
		else:
			# Adds the encounter to is corresponding spot.
			encounter_dictionary[island_type][island_name][occurence] = deck_choice

func clear_console():
    var escape = PoolByteArray([0x1b]).get_string_from_ascii()
	
func set_loaded_sector_and_island():
	for sector in sectors:
		for island in sectors[sector]:
			if island.get_island_name() == loaded_island_name:
				clear_console()
				player_ship.set_current_sector(sector)
				player_ship.set_current_island(island)
				player_ship.set_position(player_ship.get_current_island().get_position() - Vector2(SHIP_DISTANCE_FROM_ISLAND, 0))
				player_ship.set_target(player_ship.get_position())

# Sets up the players ship on the map based on the sector number and the island number passed in.
# @Param sector_num -- An integer representing the sector the players ship will be at.
# @Param island_num -- A number represents what island the player ship will be at within the sector.
func set_player_sector_and_island(sector_num: int, island_num: int) -> void:
	if not Main.new_game_pressed:
		load_data()
		if loaded_island_name and loaded_ship_position:
			set_loaded_sector_and_island()
			return
	# Sets the player ship the sector passed in and its current island to the island num passed in - 1 of the current sector.
	player_ship.set_current_sector(sector_num)
	player_ship.set_current_island(sectors[sector_num][island_num - 1])
	# Set the position of the ship to the first island - the distance the ship stays away from the islands.
	
	player_ship.set_position(player_ship.get_current_island().get_position() - Vector2(SHIP_DISTANCE_FROM_ISLAND, 0))
	
	# Set the target to be the island they are at so the ship doesn't move.
	
	player_ship.set_target(player_ship.get_position())
	
# Creates the sectors of the map i.e. generates all the islands for each sector and calls relevant methods to place/position
# the islands and connect them to adjacent ones accordingly.
func create_sectors(player) -> void:
	# Dictionary of the island_keys containg the types of islands with an array of names corresponding to that type.
	var island_keys:Dictionary =  {"Pirate": encounter_dictionary["Pirate"].keys(), "Merchant": encounter_dictionary["Merchant"].keys(), "Navy": encounter_dictionary["Navy"].keys(), 
										"Deserted": encounter_dictionary["Deserted"].keys(), "Endgame": encounter_dictionary["Endgame"].keys()}
	# The island difficulty and count of islands
	var island_difficulty: int = 1
	var island_count: int = 1
	# Loops through all sectors.
	for sector in get_sectors():
		# Resizes the sector sizes to the SECTOR_SIZE constant unless it is the first sector, which is size 1 always.
		if sector == 1:
			sectors[sector].resize(1)
		else:
			sectors[sector].resize(SECTOR_SIZE)
		var islands: Array = create_islands(sector, player, island_keys, island_difficulty, island_count)
		# Calls the function to connect all the islands.
		if sector != 1:
			connect_islands(sector, islands)
		else:
			sectors[sector] = islands
		# Calls the function to position islands.
		position_islands(islands, sector_centre)
		# Increments the island difficulty and count
		island_difficulty += (1 * sectors[sector].size() % 4)
		island_count += (1 * sectors[sector].size())
		
# Creates the islands on the map for a sector. This includes creating instance of island classes with corresponding names and 
# their types and returning them in an array. An empty array is returned if the sector number is invalid or island_keys is null.
# @Param sector -- An integer representing the sector number, used to check the sectors size.
# @Param player -- A Player instance used to pass to the Islands (the NPCS need the player to interact with them).
# @Param island_keys -- A dictionary Island Types as the key and island names as an Array as the value.
# @Param island_difficulty:int -- The difficulty the island will be 
# @Param island_count:int -- The island number out of all the islands on every sector.
# @Return -- An array representing all the islands in the sector; in order. 
func create_islands(sector:int, player, island_keys: Dictionary, island_difficulty:int, island_count:int) -> Array:
	# Return an empty array if an invalid sector number.
	if (sector < 0 || sector > sectors.size()):
		return []
	# Return a empty array if the island_keys is null.
	if island_keys == null:
		return []
	# Sets up the island array to the correct size and specifies island type.
	var islands: Array = []
	islands.resize(sectors[sector].size())
	var island_type: String
	# Used to increment the difficulty by one every 3 islands.
	
	# For each array location it creates a new island with a island_type passed in.
	for index in sectors[sector].size():
		# Gets the island type.
		island_type = compute_island_type(index, sector)
		# Gets the name of the next island in the island_keys dictionary corresponding to the island type.
		var island_name: String = island_keys[island_type].pop_front()
		var encounters:Dictionary = encounter_dictionary[island_type][island_name]			
		# If the island is Deserted, its creates an instance of DesertedIsland with the encounters information, if its Endgame, it does the same
		# but creates an instance of an EndGameIsland.
		if island_type == "Deserted":
			islands[index] = DesertedIsland.new(island_name, encounters["OnEnter"], encounters["OnIsland"], encounters["OnExit"], island_difficulty, player)
		elif island_type == "Endgame":
			islands[index] = EndGameIsland.new(island_name, encounters["Obstacle1Correct"], encounters["Obstacle1InCorrect"], encounters["Obstacle2"], encounters["Obstacle3"] ,player)
		# Otherwise its a InhabitedIsland therefore it also passing in a vendor encounter from the encounters dictionary.
		else:
			islands[index] = InhabitedIsland.new(island_name, island_type, encounters["OnEnter"], encounters["OnIsland"], encounters["OnExit"],
				encounters["Vendor"], encounters["Moveable_NPC"], island_difficulty, player)
		# Increments the difficulty by 1 every 3 islands.
		if (island_count % 4 == 1):
			island_difficulty +=1	
		island_count += 1
	return islands

# Computes which type the island within the sector given at the index given will be.
# @Param island_index -- An integer representing what island number within the sector it is.
# @Param sector -- An integer representing which sector the island is within.
# @Return -- A String containing the islands name.
func compute_island_type(island_index:int, sector: int)-> String:
	# Return an empty string if the island_index or sector number is invalid.
	if (island_index < 0 || sector < 0 || sector > sectors.size()):
		return ""
	# If the island is the last island in the last sector it will not meet this condition.
	if (!(sector == sectors.size() && island_index == sectors[sector].size()-1)):
		# Every 5th island will be of type navy.
		if island_index % 5 == 1:
			return "Navy"
					# Every 4th island will be of type merchant, if not already navy.
		elif island_index % 4 == 1:
			return "Merchant"
					# Every 3rd island will be of type deserted, if not already merchant or navy.
		elif island_index % 3 == 1:
			return "Deserted"
					# Every other island will be of type pirate.
		else:
			return "Pirate"
	else:
		return "Endgame"

# Position the islands relative to one another on the map using an array of islands an the sector centre
# as all islands are place relative to the centre. It places the island in a diamond pattern.
# @Param islands -- An array of islands to be place relative to one another.
# @Param sector_cntre -- A vector2 representing the centre of that sector.
func position_islands(islands: Array, sector_centre: Vector2) -> void:
	# If the array has no islands or the sectors centre is not specified it cannot place the islands therefore it returns null.
	if (islands == null || sector_centre == null):
		return 
	# Initialise the x and y variables which will be used as the x and y coordinates of each island.
	var x = sector_centre.x
	var y = sector_centre.y
	# Used to switch between two and three island columns on the map.
	var three_island_column = false
	# Used to count the number of island in the current column.
	var counter = 0
	# Used for how much to change the x coordinate from the initial position
	var change_amount = 0
	# Loops throught for every island in the current sector.
	for index in islands.size():
		# Places the first and last islands of the sector
		if index == 0 or index == islands.size() - 1:
			x = 600
			y = 0
			if index == 0:
				x = -600
		# Otherwise it will follow a more general pattern
		else:
			# The same pattern repeats every 5th island for the y coordinate.
			var y_category = index % 5
			match y_category:
				0:
					y = -400
				1:
					y = 200
				2:
					y = -200
				3:
					y = 400
				4:
					y = 0
			x = -400
			# If reached the last of the islands in that column go to the next column.
			# The next column will have the opposite number of islands as the previous i.e. (from 2 islands to 3).
			if (counter == 2 and !three_island_column) or counter == 3:
				if !three_island_column:
					three_island_column = true
				else:
					three_island_column = false
				# Reset the counter of number of islands in that column iterated over.
				counter = 1
				# Change the amount the x coordinate will be changed by with an extra 100
				change_amount += 200
			# All islands in the current column have not been positioned yet therefore increment the counter.
			else:
				counter += 1
			# Change the x coordinate of the current island an amount for that specific row.
			x += change_amount
		# Position the island with the x and y coordinates.
		islands[index].set_position(Vector2(sector_centre.x + x,sector_centre.y + y))

# Connects each island with its adjacent islands within a sector maintaining the diamond shape of island connections.
# used to prevent the player from moving across multiple islands.
# @Param sector -- The sector number the islands are at.
# @Param islands -- An array of islands that are to be connected.
func connect_islands(sector: int, islands: Array) -> void:
	# Returns null if the sector number or islands are null as it cannot connect them otherwise.
	if (islands == null || sector == null):
		return 
	var max_island_index = (islands.size() - 1)
	# Whether the current column will have 3 or 4 islands.
	var three_island_column = false
	# Number of islands before it switches to the next column.
	var switch_num_island_column = 0
	# Joins the last island of the last sector, with the first island of the current sector as long as its not the first sector.
	if sector != 1:
		var last_sector = sectors[sector-1]
		islands[0].add_adjacent_island(last_sector[last_sector.size()- 1], true)
		
	# Loops through every island in the sector to join them with their adjacent islands.
	for index in islands.size():
		# If its the first or the last island in the sector the joining adjacent islands behaves differently.
		if index == 0 or index == max_island_index:
			# If first then joins with next 2 island, if last join with previous 2 islands.
			var change_amount = 1
			if index != 0:
				change_amount = -1
			islands[index].add_adjacent_island(islands[index + change_amount], true)
			change_amount += change_amount
			islands[index].add_adjacent_island(islands[index + change_amount], true)
			
		# Else if its not the 2 islands after the first or the 2 islands before the last, as these have already been connected.
		elif !(index in [(max_island_index - 2), (max_island_index - 1)]):
			# If the current column of islands is 3 in size.
			if (three_island_column):
				# If the 2nd or 3rd island of the column, join with the 1st and 2nd of the next column respectively.
				if switch_num_island_column == 1 or switch_num_island_column == 2:
					islands[index].add_adjacent_island(islands[index + 2], true)
				# If the 1st or 2nd island of the column, join with the 1st and 2nd of the next column respectively.
				if switch_num_island_column == 0 or switch_num_island_column == 1:
					islands[index].add_adjacent_island(islands[index + 3], true)
				# If the 3 islands have been connected in the current column, change to the next column (2 island column).
				if switch_num_island_column == 2:
					three_island_column = false
					switch_num_island_column = 0
				else:
					switch_num_island_column += 1
			else:
				# If a 2 island column then join with either the 1st and 2nd or 2nd and 3rd of the next 3 island column.
				islands[index].add_adjacent_island(islands[index + 2], true)
				islands[index].add_adjacent_island(islands[index + 3], true)
				# If the 2 islands have been connected in the current column, change to the next column (3 island column).
				if switch_num_island_column == 1:
					three_island_column = true
					switch_num_island_column = 0
				else:
					switch_num_island_column += 1
	# Place the current sector (array of islands) in the sectors Dictionary
	sectors[sector] = islands

# Return all the sectors on the map.
# @Return -- Dictionary of sectors.
func get_sectors() -> Dictionary:
	return sectors

# Return a specified sector which is an array of its islands.
# @Param sector_no -- An integer representing the sector.
# @Return -- An array with the islands within that sector.
func get_sector(sector_no: int) -> Array:
	return sectors[sector_no]

# Gets the players ship.
# @Return -- A Ship which is the player ship instance.
func get_player_ship() -> Ship:
	return player_ship

# Gets the navy ship
# @Return -- A Ship which is the navy ship instance.
func get_navy_ship() -> Ship:
	return navy_ship

# Returns a boolean based on whether the player_ship has been caught by the navy_ship.
# @Return -- True if the navy ship is further ahead in terms of position or sector.
func navy_reached_player() -> bool:
	# If the navys current sector is greater than the player current sector or if they are at the same but the navy
	# ships x position is further along return true.
	if (navy_ship.get_current_sector() >= player_ship.get_current_sector()):
		if (navy_ship.get_current_sector() > player_ship.get_current_sector()):
			return true
		elif (navy_ship.get_position().x >= player_ship.get_position().x):
			return true
	return false
		
# This method looks to see if the navy ship should be moving, which is the case once every 10 seconds, after the players_ship
# has left the starting island and a certain number of turns has passed by.
# @Return -- True if the player_ship should be moving, otherwise false.
func should_move_navy() -> bool:
	# Every n seconds the Navy should will potentially move.
	if (int(DayNightCycle.get_time()) % 300 == 0):
		# If the navy currently shouldnt be moving, set the variable indicating it should.
		if (!should_move_navy):
			should_move_navy = true
			# Checks whether the playre has left the starting island, if so increment the turns.
			if player_left_tutorial_island():
				num_turns+=1
				# If the number of turns is greater than the amount it takes for the navy to start moving, return true
				if num_turns >= NAVY_START_MOVING:
					return true
	# Otherwise the navy shouldn't be moving therefore setting the variable and return false if any of the conditions aren't met.			
	else:
		should_move_navy = false	
	return false		

# Checks whether the player_ship is still at the first island.
# @ Return -- true if the player_ship is not at the first island.
func player_left_tutorial_island()-> bool:
	return player_ship.get_current_island() != sectors[1][0]

# Moves the navy ship to the next island, if they currently dont have an island it defaults to the first sectors first island.
# Otherwise if the navy ship is at the last island in a sector, the navy goes to the first island of the next sector.
func move_navy_ship() -> void:
	if !should_move_navy():
		return	
	var found:bool = false
	# If the navy ships current island is null, meaning it hasn't been set, its sets to the first island in the first sector.
	if navy_ship.get_current_island() == null:
			navy_ship.set_current_island(sectors[1][0])
			found = true
	else:
		# Otherwise it sets it to the first island found that it further across the map.
		for island in sectors[navy_ship.get_current_sector()]:
			if navy_ship.get_current_island().get_position().x < island.get_position().x && found == false:
				navy_ship.set_current_island(island)
				found = true
	# If the island still hasn't been found and the navy ship sector is not at the end of the map, it sets the players ship
	# to the first island of the next sector.
	if !found && navy_ship.get_current_sector() != sectors.size():
		navy_ship.set_current_sector(navy_ship.get_current_sector() + 1)
		navy_ship.set_current_island(sectors[navy_ship.get_current_sector()][0])
		var island_position = navy_ship.get_current_island().get_position()
		navy_ship.set_position(Vector2(island_position.x - 500, island_position.y))
	navy_ship.set_target(Vector2(navy_ship.get_current_island().get_position().x, sector_centre.y))


# Moves the player to the next sector if they arent already at the last sector
func progress_to_next_sector() -> void:
	# Progresses the player forwards by a sector (if not at the max) and sets the players hips to the first island of the sector.
	if (player_ship.get_current_sector() != sectors.size()):
		player_ship.set_current_sector(player_ship.get_current_sector() + 1)
		move_player_ship(sectors[player_ship.get_current_sector()][0], true)


# Checks whether the island passed in is the same island the player is currently at.
# @Param island:Island -- the island being compared to.
func is_visiting_island(island) -> bool:
	if island is String:
		if player_ship.get_current_island().get_island_name() == island:
			return true
		return false
	else:
		if player_ship.get_current_island() == island:
			return true
		return false

# Set the player ship's target to the island, if its an island that is connected the island the ship is currently at
# @Param island:Island -- The island the player ship is moving to.
# @Param next_sector: bool -- Whether the island is in the next sector.
# @Return bool -- True if the player ship is able and has moved to the island
func move_player_ship(island: Island, next_sector: bool) -> bool:
	var target_island = island
	# Checks whether the island the user clicked on is a valid adjacent island.
	for adj_island in player_ship.get_current_island().get_adjacent_islands():
		if (adj_island == target_island):
			# If so move the ship to the target island and update the map to say which island the ship is at.
			if (next_sector):
				#If the island is in the next sector, move the ship to that sector.
				player_ship.set_position(target_island.get_position() - Vector2(SHIP_DISTANCE_FROM_ISLAND, 0))
			player_ship.set_current_island(target_island)
			player_ship.set_target(target_island.get_position() - Vector2(SHIP_DISTANCE_FROM_ISLAND, 0))
			save_data()# saves  data each time it moves ship
			return true
	return false



## saving data to savedata file
##this information is about the ship
func save_data():
	var data = {
		'state': Main.previous_state,
		'island': Main.game.player.get_ship().get_current_island().get_island_name(),
		'ship_x': player_ship.get_position().x,
		'ship_y': player_ship.get_position().y,
		'time': DayNightCycle.time,
	}
	
	var save_game = File.new()
	save_game.open("user://savedata.save", File.WRITE)
	save_game.store_line(to_json(data))
	save_game.close()
