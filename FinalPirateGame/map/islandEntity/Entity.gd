extends Node

# Interface representing any entity.
class_name Entity 

func _init():
	pass
	
# Every concrete class implenting this class must implement this method
# @Param player:Player -- The player interacting with the entity.
# @Return :bool
func interact(player)->bool:
	return false