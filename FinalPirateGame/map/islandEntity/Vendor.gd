extends "res://map/islandEntity/NPC.gd"

# A vendor NPC whichs  has a shop where players are able to purchase items from including ship upgrades, upgradeItems and sell their own items
# to the vendor
class_name Vendor

# A instance of shop corresponding to the Vendor
var shop: Shop setget ,get_shop
# Constructs an instance of a Vendor with the encounter information, the position and the type of NPC.
# @Param tmp_encounter: Array -- An array of encounter information.
# @Param pos: Vector2 -- The position of the NPC.
# @Param temp_type: String -- The tpye of NPC.
# @Param player:Player -- An instance of Player.
# @Param difficulty:int -- The NPC level.
func _init(tmp_encounter: Array, pos: Vector2, temp_type: String, player:Player, difficulty:int).(tmp_encounter, pos, temp_type, player, difficulty) -> void:
	shop = Shop.new(player,difficulty)
	special_interaction = shop

# Gets the shop instance.
# @Return shop:Shop -- The shop the Vendor has.
func get_shop() -> Shop:
	return shop

# Moves to the next stage of the encounter based on the option chosen by the player. If
# the encounter if finished, the encounter is reinitialsed for multiple encounters.
# @Param player:Player -- The player interacting with the NPC.
# @Param option_chosen:String -- The option the player has chosen from the current stages options.
func next_encounter_stage(player,option_chosen:String) ->bool:
	# If the encounter has finished, set up the encounter again.
	if(.next_encounter_stage(player,option_chosen)):
		if encounter.save_idx == -1:
			encounter = Encounter.new(encounter_deck)
		return true
	return false
	

	



