extends "res://map/islandEntity/Entity.gd"

# A character represents anything that can move on an island, containing its position, target and the speed in which they can move.
class_name Character

# Target position of the character.
var target: Vector2 setget set_target, get_target
# The speed of the character in terms of movement.
var speed: int = 100 setget set_speed, get_speed
# Current position of the character.
var position: Vector2 setget set_position, get_position

# Contructs the character instane with its position and target.
# @Param pos:Vector2 -- The position of the character.
func _init(pos:Vector2)->void:
	position = pos
	target = position
	
# Sets the target position of the character
# @Param targ:Vector2 -- The target position.
func set_target(targ: Vector2) -> void:
	target = targ

# Gets the target position of the character
# @	Return target:Vector2 -- The target position.
func get_target() -> Vector2:
	return target

# Sets the speed of the character.
# @Param tmp_speed:int -- The new speed of the character.
func set_speed(tmp_speed: int) -> void:
	speed = tmp_speed

# Gets the speed of the character.
# @Return speed:int -- The speed of the character.
func get_speed() -> int:
	return speed
	
# Sets the position of the character
# @Param pos:Vector2 -- The new position.	
func set_position(pos: Vector2) -> void:
	position = pos
	
# Gets the position of the character
# @Return position:Vector2 -- The position of the character..	
func get_position() -> Vector2:
	return position


