extends "res://map/islandEntity/Character.gd"

# PlayerCharacter represents the player instance of a character used on island and extends from the @see character class. 
# In addition to a normal character the player character also holds a default position used to return to when entering back onto an island.
class_name PlayerCharacter
# A default position of the player character.
var default_position: Vector2 setget ,get_default_position

# Contructs the instance of a player character including its position and default position.
# @Param pos:Vector2 -- The position of the character.
func _init(pos:Vector2).(pos):
	default_position = position

# Gets the default position of the character
# @Return default_position:Vector2 -- The default position of the players character.
func get_default_position() -> Vector2:
	return default_position

