extends "res://map/islandEntity/SpecialInteraction.gd"

# The shop is used for the player to purchase and sell items that they have. They can buy upgrade items, rum and crew which all affect their resources
# along with their items accordingly. They can sell items which removes the items from their inventory and gives the player the gold value of those items.
# If the npc that contains this shop instance is level 1 i.e. the first island, the shop will always sell cannons and make all other items
# expensive so it fits the story.

class_name Shop

# All the available ship upgrades in the game.
var shipUpgrades: Dictionary = {"Improved Sails": {"cost": 250, "resource": "SPEED", "affect": 0.2},
								"Improved Hull": {"cost": 250, "resource": "REPAIRS", "affect": 0.2},
								"Improved Cannons": {"cost": 250, "resource": "GUNPOWDER", "affect": 0.2},
								"Bunks": {"cost": 200, "resource": "MORALE", "affect": 0.15},
								"Butcher": {"cost": 300, "resource": "CREW", "affect": 0.2},
								"Monkey": {"cost": 150, "resource": "MORALE", "affect": 0.15},
								}
var playerUpgrades

# The ship upgrade that this shop has and the purchaseable player upgrades the shop has.
var ship_upgrade: UpgradeItem setget ,get_ship_upgrade
var purchaseable_player_upgrades: Array setget ,get_purchaseable_player_upgrades

# Constructs the shop with the ship upgrade and basic upgrades.
func _init(player:Player, npc_level:int).(player) -> void:
	# Gets the possible player upgrades the shop can have.
	var upgradeItem = UpgradeItem.new(0)
	playerUpgrades = upgradeItem.get_upgrades()
	# Sets the purchasble items array up to size 3.
	purchaseable_player_upgrades = []
	purchaseable_player_upgrades.resize(3)
	#Creates the ship upgrade and the purchaseable items.
	construct_ship_upgrade(npc_level)
	construct_purchaseable_player_upgrades(npc_level)

# Used to generate a random ship upgrade from the list and make an item containing its information.
# The level of the item created is based on the npc_level passed in
# @Param npc_level:int -- The level the ship upgrade will be
func construct_ship_upgrade(npc_level:int) -> void:
	#randomly select one ship upgrade at shop
	var ship_upgrade_name:String
	ship_upgrade_name = shipUpgrades.keys()[randi() % shipUpgrades.keys().size()]
	var itemEntry:Dictionary
	ship_upgrade = UpgradeItem.new(1)
	# If its the first island it guarantees cannons and the price is low
	if (npc_level == 1):
		itemEntry = shipUpgrades["Improved Cannons"]
		ship_upgrade.set_gold_value(50)
		ship_upgrade.set_item_name("Improved Cannons")
	# Otherwise its made from the random item name and its corresponding price
	else:
		itemEntry = shipUpgrades[ship_upgrade_name]
		ship_upgrade.set_gold_value(itemEntry["cost"])
		ship_upgrade.set_item_name(ship_upgrade_name)
	# Sets up the rest of the item including its resource, affect and description
	ship_upgrade.set_affected_resource(itemEntry["resource"])
	ship_upgrade.set_affect(itemEntry["affect"])
	ship_upgrade.set_upgrade_item_description()
	

# Constructs three basic player upgrade items from the list of available ones in which their levels
# are based on the npc_level passed in
# @Param npc_level:int -- The level of the npc that contains this shop.
func construct_purchaseable_player_upgrades(npc_level:int) -> void:
	#randomly select three player upgrades at shop
	for i in range(3):
		var upgrade_item: UpgradeItem = UpgradeItem.new(player.get_ship().get_current_sector())
		# If its the first island, sets its price very high to make them buy cannons.
		if (npc_level == 1):
			upgrade_item.set_gold_value(500)
		purchaseable_player_upgrades[i] = upgrade_item
			

# Gets the gold value of the ship upgrade, if the ship upgrade hasnt already been purchased.
# @Return :int -- The cost of the ship upgrade.
func get_ship_upgrade_cost() -> int:
	if (ship_upgrade!= null):
		return player.adjustResource("Gold", ship_upgrade.get_gold_value())
	return 0

# Gets the gold cost of one of the player upgrade items based on the index, returning 0 if it has already been purchased.
# @Return :int -- The cost of the player upgrade.
func get_player_upgrade_cost(index:int) -> int:
	if (purchaseable_player_upgrades[index]!= null):
		return player.adjustResource("Gold", purchaseable_player_upgrades[index].get_gold_value())
	return 0

# Gets the gold value of one of the players items, if they have an item in that item slot else returns 0.
# @Param index:int -- The position of the item in the players inventory you want the gold value of
# @Return :int -- The cost of the item.
func get_player_item_cost(index: int) -> int:
	if (player.get_items()[index] != null):
		return int(round(player.get_items()[index].get_gold_value() * 0.5))
	return 0

# Gets the name of the item stored in the player inventory items at a slot at index, if the slot is empty returns empty.
# @Param index:int -- The position of the item in the players inventory you want the name.
# @Return :String -- The name of the item.
func get_item_name(index: int) -> String:
	if (player.get_items()[index] != null):
		return player.get_items()[index].get_item_name()
	return "Empty"

# Get the ship upgrade in the shop, returning empty if its already been purchased.
# @Return :String -- The name of the item.
func get_ship_upgrade_name() -> String:
	if (ship_upgrade != null):
		return ship_upgrade.get_item_name()
	return "Empty"

# Get the player upgrade item at index in the shop, returning empty if its already been purchased.
# @Param index:int -- The position of the item in the shop you want the name.
# @Return :String -- The name of the item.
func get_purchaseable_player_upgrade_name(index: int) -> String:
	if (purchaseable_player_upgrades[index] != null):
		return purchaseable_player_upgrades[index].get_item_name()
	return "Empty"

# Purchases the item at the index if its not null and they have enough gold to. It then reduces the players gold and add the item to their inventory.
# @Param player_upgrade_index: int -- The position in the upgrades array you want to purchase.
func upgrade_item_purchase(player_upgrade_index: int)->void:
	# If the index is a valid index and there is an item there it continues otherwise it just returns as its an invalid purchase.
	if player_upgrade_index < 0 || player_upgrade_index > 2 || purchaseable_player_upgrades[player_upgrade_index] == null:
		return
	# Gets the item out of the array and checks whether the player has enough money to get it.
	var item: UpgradeItem = purchaseable_player_upgrades[player_upgrade_index]
	if (player.get_resource_value("Gold") >= get_player_upgrade_cost(player_upgrade_index)):
		# If they do it takes the money off the player, adds the item to their inventory and sets the position in the array to null as its been purchased.
		player.change_resource_amount("Gold", item.get_gold_value() *-1)
		player.add_item(item)
		purchaseable_player_upgrades[player_upgrade_index] = null
		
# Sells the item at the index from the players inventory and gives them the gold amount.
# @Param player_items_index: int -- The position in the players items array that you want to sell.
# @Return player.delete_item(player_items_index):bool -- Whether the item was sold.
func sell_item_pressed(player_items_index: int)->bool:
	player.change_resource_amount("Gold", int(round(get_player_item_cost(player_items_index))))
	return player.delete_item(player_items_index)

# Purchases the ship upgrade at this shop if they have enouhg money and add it to the players ship upgrades. It also removes it from the shop.
func on_ship_upgrade()->void:
	if (ship_upgrade == null):
		return
	# If the player has enough space for another ship upgrade
	if (player.num_ship_upgrades < 3):
		# If hey have enough gold then the gold is taken from them and the upgrade is added to their inventory.
		if (player.get_resource_value("Gold") >= get_ship_upgrade_cost()):
			player.change_resource_amount("Gold", ship_upgrade.get_gold_value() *-1)
			player.add_ship_upgrade(ship_upgrade)
			# The ship upgrade has been brought therefore its now null
			ship_upgrade = null
			
# Gets the ship upgrade.
# @Return ship_upgrade: UpgradeItem			
func get_ship_upgrade() -> UpgradeItem:
	return ship_upgrade

# Gets the items that can be purchased from this shop.
# purchaseable_player_upgrades: Array -- All the purchaseable items.
func get_purchaseable_player_upgrades() -> Array:
	return purchaseable_player_upgrades

# Returns how much rum would cost the player
# @Return :int -- The rum value.
func get_rum_cost()->int:
	return player.adjustResource("Gold", -20)

# Takes money from player and gives them rum if they have enough money to get it.
func buy_rum()->void:
	var cost =  get_rum_cost()
	# If they have enough then it gives them the rum and takes gold from them.
	if (player.get_resource_value("Gold") >= -cost):
		player.change_resource_amount("Gold", cost)
		player.change_resource_amount("Rum", 10)

# Returns how much crew would cost the player
# @Return :int -- The cost of crew to buy.
func get_crew_cost()->int:
	return player.adjustResource("Gold", -30)

# Takes money from player and gives crew if they have enough money to do so.
func buy_crew()->void:
	var cost = get_crew_cost() 
	# If they have enouhg gold then gives them the crew and takes the gold away from them.
	if (player.get_resource_value("Gold") >= -cost):
		player.change_resource_amount("Gold", cost)
		player.change_resource_amount("Crew", 12)


# Triggers the speical interaction so the player enters the shopping state
func trigger()->void:
	player.change_state(player.Player_states.SHOPPING)
