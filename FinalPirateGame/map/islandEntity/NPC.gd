extends "res://map/islandEntity/Character.gd"

# The NPC class is the NPCs that are on the islands and have encounters that the player can interact with them through. They also contain
# special interactions which the player can trigger in which is a Battle object, used to battle the NPC.
class_name NPC
# The type of character and their encounter.
var character_type: String  setget , get_character_type #type of character 
var encounter: Encounter setget , get_encounter
# The states the NPC can be.
enum NPC_states{
	IDLE
	INTERACTING,
	SPECIAL_INTERACTION
}
var current_state: int 
# Special interaction is a Battle that can start if the encounter option the player picks leads to it initiating
var special_interaction:SpecialInteraction
var encounter_deck: Array 
# Whether the NPC has been killed
var is_alive: bool
# Whether the NPC's encounter has been saved at some point for repeatability.
var encounter_saved: bool

# Constructs an NPC with an encounter array, a position and the type of NPC it is.
# @Param tmp_encounter: Array -- An array containing the encounter information for the NPC.
# @Param pos: Vector2  -- The position of the NPC.
# @Param temp_type: String  -- The type of NPC it is i.e. Navy.
# @Param player:Player -- An instance of Player, used for the NPC to battle with.
# @Param difficulty:int -- The NPC level.
func _init(tmp_encounter: Array, pos: Vector2, temp_type: String, player:Player, difficulty:int).(pos) -> void:
	self.encounter_deck = tmp_encounter
	encounter = Encounter.new(tmp_encounter)
	position = pos
	character_type = temp_type
	current_state = 0
	# Set special interaction instance as a Battle for default NPCS.
	special_interaction=Battle.new(player, difficulty)
	is_alive = true
	#special_interaction=Battle.new()
	
# Interact with the player passing in an instance of Player. If this is called the state of the NPC is interacting and the encounter associated with the NPC is started.
# It also triggers an special_interaction if the current stage of the encounter has a special interaction.
# @Param player:Player -- An instance of player 
func interact(player)->bool:
	current_state = NPC_states.INTERACTING
	encounter.start_encounter(player)
	# If the encounters stage has a special itneraction then trigger the special interaction the NPC has.
	if (encounter.get_stage().has_special_interaction()):
		special_interaction.trigger()
		# The NPC will die as the encounter is a Battle.
		is_alive = false
	return true
	
# Called when the player chooses an option from the current encounter passing in an instance of Player and the option chosen in string format (i.e. the text).
# Goes to the appropriate stage corresponding to the option chosen if the options is valid and if the size of the options of the stage is 0 before the encounter option chosen,
# it returns true to signal the interaction has finished and false otherwise. It also gives/takes resources to/off the player if the stage is a leaf stage. 
# @Param player:Player -- The Player interacting with the NPC.
# @Param option_chosen:String -- A string containing the text corresponding to one of the options.
# @Return bool -- Returns true if the stage has no options left to choose from.
func next_encounter_stage(player,option_chosen:String) ->bool:
	# If its called with no options left, the encounter has finished therefore it returns true
	if encounter.get_stage().get_options().size() == 0:
		# If the encounter was saved at any stage, the NPC is given a new encounter with the saved version of the stage information.
		if encounter.get_save_idx() != -1:
			#encounter = Encounter.new(encounter_deck)
			encounter.set_stage(encounter.get_save_idx())
#			# Set the false so that the encounter
#			encounter_saved = false
		return true
	# Loops through each option, checking whether the options description matches  the one chosen, if so
	# it calls the next_encounter_stage passing in the options number for that option.
	else:
		var option_idx: int 
		for option in encounter.get_stage().get_options().keys():
			if option[1] == option_chosen:
				option_idx = int(option[0])
		encounter.choose_option(option_idx, player)
		# Checks whether the encounter has reached an end node therefore there are no more options.
		if encounter.get_stage().get_options().size() == 0:
			# If so update the players resource amounts by the amount the interactions has changed them by.
			for key in player.get_resources().keys():
				print(int(encounter.get_stage().resource_changes[key]))
				player.change_resource_amount(key, int(encounter.get_stage().resource_changes[key]))
		interact(player)
		return false

# Gets the type of character.
# @Return character_type:String -- The type of NPC it is.
func get_character_type() -> String:
	return character_type

# Sets the encounter of the NPC (useful for repeat interactions)
# @Param tmp_encounter:Array -- An array of encounter information.
func set_encounter(tmp_encounter: Array) -> void:
	encounter = Encounter.new(tmp_encounter)

# Get the current encounter.
# @Return encounter -- The Encounter object attached to the NPC.
func get_encounter() -> Encounter:
	return encounter
	
# Set the position of the NPC
# @Param pos:Vector2 -- A position used to set the NPCs position.
func set_position(pos: Vector2) -> void:
	position = pos

# Get the position of the NPC.
# @Return position:Vector2 -- The current position of the NPC.
func get_position() -> Vector2:
	return position

# Check to see if the NPC has already interacted with the player.
# @Return bool -- Whetehr the NPC has got any options left, if not it has interacted with the player.
func interacted_with_player() -> bool:
	# If the encounter stage options is 0, it means the NPC has already been interacted with.
	if encounter.get_stage().get_options().size() == 0:
		return true
	else:
		return false