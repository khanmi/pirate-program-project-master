extends "res://map/islandEntity/Entity.gd"
# Abstract class containing the core implementation of any item within the game conisting the a name and description,
# along with a way in which it interacts with the player.
class_name Item

# The name and description of the item.
var item_name: String setget set_item_name, get_item_name
var description: String setget set_item_description ,get_description


# Constructor for the item, consisting of setting the name and description.
# @Param item_name: String -- The name of the item
# @Param description: String -- The description of the item.
func  _init(item_name: String, description: String):
	self.item_name = item_name
	self.description = description

# Interacts with the player by placing itself into the players items.
# @Param player:Player -- The player that will gain this item.
# @Return player.add_item(self):bool -- Whether the item was able to be added or not.
func interact(player)->bool:
	return player.add_item(self)

# Gets the name of the item
# @Return item_name:String -- The name of the item.
func get_item_name() -> String:
	return item_name

# Gets the description of an item
# @Return description:String -- The description.
func get_description() -> String:
	return description

# Set the name of the item
# @Param name:String -- The name of the item.
func set_item_name(name:String) -> void:
	item_name = name
	
# Set the description of the item.
# desc:String -- The description of the item.
func set_item_description(desc: String) -> void:
	description = desc

# Compares whether an object is identical to another
# @Param object:Node -- An object to be compared to.
# @Return :bool -- Whether they are equal.
func equals(object: Node) -> bool:
	if object as Item:
		if item_name == object.get_item_name():
			if description == object.get_description():
				return true
	return false

	
