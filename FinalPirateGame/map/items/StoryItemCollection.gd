extends Node

# This class represents a collection of all the items the player can obtain or will need throughout the story. This includes the clues in which they get and 
# all items that are specific to the story including pre_clue items that cannot be seen by the player but are used to register they have completed certain interactions.

class_name StoryItemCollection
# A dictionary containing a String with the clue number and the value is the clues itself.
var clues: Dictionary setget ,get_clues
# A dictionary containing all the story items alongside their corresponding item.
var story_items: Dictionary = {"GEM": Item.new("Mystical Gem", "The rumoured mystical gem, thought to have mystical powers!"), 
								"PIRATE_LETTER": Item.new("Pirate captain letter", "A secret pirate letter, containing the pirate code. Sure to gain a pirates trust"),
								"MERCHANT_LETTER": Item.new("Merchant letter", "A letter of graditude, sure to gain any merchant's trust."),
								"NAVY_LETTER": Item.new("Navy service letter", "A letter of service to the Royal Navy."),
								"SEA_TACTICS_MANUAL":  Item.new("Sea tactics manual.", "A written book of legendary sea tactics sure to win a battle."),
								"PRE_CLUE_1_BEEN_ON_VOSEA":  SellableItem.new("PRE_CLUE_1_BEEN_ON_VOSEA", "temp.", 0),
								"PRE_CLUE_2_SPOKE_TO_JESSIES_DAD":  SellableItem.new("PRE_CLUE_2_SPOKE_TO_JESSIES_DAD", "temp.", 0),
								"PRE_CLUE_2_LET_JESSIE_KEEP_GOLD":  SellableItem.new("PRE_CLUE_2_LET_JESSIE_KEEP_GOLD", "temp.", 0),
								"PRE_CLUE_2_TOOK_GOLD_OFF_JESSIE":  SellableItem.new("PRE_CLUE_2_TOOK_GOLD_OFF_JESSIE", "temp.", 0),
								"PRE_CLUE_3_SENT_TO_GET_LETTER":  SellableItem.new("PRE_CLUE_3_SENT_TO_GET_LETTER", "temp.", 0),
								"PRE_CLUE_3_REMOVED_THE_CURSE":  SellableItem.new("PRE_CLUE_3_REMOVED_THE_CURSE", "temp.", 0),
								"LIFT_CURSE_POTION": Item.new("Curse removal potion", "Potion that removes ancient curses."),
								"BROKEN_TELESCOPE": UpgradeItem.new(1),
								"BROKEN_FLINTLOCK": UpgradeItem.new(1),
								"BROKEN_TANKARD": UpgradeItem.new(1),
								"ALE":  Item.new("Ale", "Some good old ale. Sure to lighten up someones day."),
								"RED_PEARL": Item.new("Red Pearl","A stunning Red Pearl. Believed to be lucky among pirates."),
								"SILVER_PEARL": Item.new("Silver Pearl","A stunning silver pearl. Worth a shiny penny.")} setget ,get_story_items
# A pointer reprsenting which clue the player will get next.
var clue_pointer: int = 0

# Constructor which sets up all the clues by reader them from the reader and then sets up the items in which are 
# specifically to do with the players character choice.
func _init():
	var reader = ClueReader.new()
	reader.read("res://map/clue/Clues.tres")
	# Stores each clue with their corresponding string
	clues["CLUE_1"] = reader.get_next_clue()
	clues["CLUE_2"] = reader.get_next_clue()
	clues["CLUE_3"] = reader.get_next_clue()
	# Sets up the character style items
	set_up_character_style_items()

# Sets the character style items up and places them into the dictionary.
func set_up_character_style_items()->void:
	story_items["BROKEN_TELESCOPE"] = set_up_item_player_type_item("Broken telescope","Gold")
	story_items["BROKEN_FLINTLOCK"] = set_up_item_player_type_item("Broken flintlock","Gunpowder")
	story_items["BROKEN_TANKARD"] = set_up_item_player_type_item("Broken tankard","Crew")
	
# Sets up the player item based on the name and the resource it affects and returns the upgrade item
# @Param name:String -- The name of the item.
# @Param resource_affected:String -- The resource it affects.
func set_up_item_player_type_item(name:String, resource_affected:String):
	# An upgrade item is created with the name and resource affected set with the ones passed in.
	var item = UpgradeItem.new(1)
	item.set_item_name(name)
	item.set_affected_resource(resource_affected)
	# Sets the gold value of the item to 10 and the affect to 15%
	item.set_gold_value(10)
	item.set_affect(0.15)
	item.set_upgrade_item_description()
	return item
	
# Returns the clues dictionary
# @Return clues:Dictionary -- The clues.
func get_clues() -> Dictionary:
	return clues

# Gets the next clues in the array
# @Return clues[clue_pointer]:Clue -- The next clue.
func get_next_clue() -> Clue:
	clue_pointer += 1
	return clues[clue_pointer]

# Gets all the story items 
# @Return story_items:Dictionary -- The story items.
func get_story_items() -> Dictionary:
	return story_items

# Gets a story item from the name passed in out of the story items dictionary
# @Param name:String -- The name of the item
# @Return story_items[name]:Item -- The item corresponding to the name
func get_story_item(name:String) -> Item:
	if name.find("CLUE") != -1 && name.find("PRE_CLUE") == -1:
		return clues[name]
	return story_items[name]
	

