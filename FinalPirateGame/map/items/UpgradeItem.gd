extends "res://map/items/SellableItem.gd"

# Items that have a affect on the players resources; i.e. a parrot companion will have a beneficial effect of 5% additional
# resource if changing players resources by positive amount, and 5% less resources lost when changing by a negative amount

class_name UpgradeItem

# Variables for the resource the item will affect, the % (or float) amount it will affect that resource and the level of the upgrade.
# I.e. upgrade level 2 will add on to the affect to generate an extra 0.04 or 4% bonus.
var affected_resource: String setget set_affected_resource, get_affected_resource
var affect: float setget set_affect, get_affect
var upgrade_level: float setget set_upgrade_level, get_upgrade_level
# All the available upgrade types.
var upgrades: Dictionary = {"Parrot Companion": {"cost": 200, "resource": "MORALE", "affect": 0.05},
							"Compass": {"cost": 120, "resource": "MORALE", "affect": 0.01},
							"Crate of Flintlocks": {"cost": 300, "resource": "CREW", "affect": 0.08},
							"Lucky Gem": {"cost": 350, "resource": "GOLD", "affect": 0.02},
							"Spyglass": {"cost": 175, "resource": "REPAIRS", "affect": 0.06},
							"Wood plank": {"cost": 50, "resource": "REPAIRS", "affect": 0.03},
							"Rum barrel": {"cost": 200, "resource": "RUM", "affect": 0.02}}

# Constructor which sets up a random item from the list of upgrades and sets up the corresponding variables.
# @Param max_level: int	-- The max level the item created can be.					
func _init(max_level: int).("","",0):
	# Generates a random level from the max level and a random item from the upgrades.
	upgrade_level = randi() % (max_level + 1)
	item_name = upgrades.keys()[randi() % upgrades.keys().size()]
	# Sets the cost, affected_resource and the amount/affect.
	var cost = upgrades[item_name].get("cost")
	affected_resource = upgrades[item_name]["resource"]
	affect = upgrades[item_name]["affect"] + (upgrade_level / 100 * 2)
	# Calls the parent to init the name and the description from the data.
	._init(item_name, item_name + " improves the " + affected_resource + " by " + (get_affect() * 100) as String + "%.", cost)
	
# Gets the resource it affects.
# @Return affected_resouce:String -- The resource it affects.
func get_affected_resource() -> String:
	return affected_resource

# Gets the level of the upgrade.
# @Return upgrade_level:float -- The upgrade level.
func get_upgrade_level() -> float:
	return upgrade_level
	
# Gets the affect i.e. the value.
# @Return :float -- The value of the affect.
func get_affect() -> float:
	return affect

# Gets the dictionary of upgrades.
# @Return upgrades:Dictionary -- The upgrades.
func get_upgrades() -> Dictionary:
	return upgrades

# Sets the affected resource
# @Param new_affected_resource: String -- The affected resource.
func set_affected_resource(new_affected_resource: String) -> void:
	affected_resource = new_affected_resource
	
# Sets the upgrade level 
# @Param new_level:float -- The new level of the item.
func set_upgrade_level(new_level: float) -> void:
	upgrade_level = new_level

# Sets the affect value of the item
# @Param new_affecr:float -- The new affect value.
func set_affect(new_affect: float) -> void:
	affect = new_affect

# Sets the items description based on its name, affected resource and its affect value.
func set_upgrade_item_description() -> void:
	.set_item_description(item_name + " improves the " + affected_resource + " by " + (get_affect() * 100) as String + "%.")

# Compares whether an object is identical to another
# @Param object:Node -- An object to be compared to.
# @Return :bool -- Whether they are equal.
func equals(object: Node) -> bool:
	if object as UpgradeItem:
		if item_name == object.get_item_name():
			if description == object.get_description():
				if (affected_resource == object.get_affected_resource() && affect == object.get_affect()):
					return true
	return false
	