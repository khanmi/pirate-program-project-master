extends "res://map/islands/NormalIsland.gd"

# A deserted island is a special type of island in which there are no Human NPCs therefore doesnt have any moveable npcs on the island
# and this type of island has a higher probability of spawning items.
class_name DesertedIsland

# Constructor which sets up the onEnter, OnIsland and OnExit encounters 
# and the initialises the probability of items spawning based on the sector. Additionally it calls the super constructor of normalIsland.
# @Param name:String -- The name of the island.
# @Param onEnter_encounter_deck: Array -- The encounter information for the entering the island encounter. 
# @Param onIsland_encounter_deck: Array -- The encounter information for the island encounter. 
# @Param onExit_encounter_deck: Array  -- The encounter information for the leaving the island encounter. 
# @Param sector_num:int -- The sector the island is at.
# @Param player:Player -- An instance of Player.
func _init(name:String, onEnter_encounter_deck: Array, onIsland_encounter_deck: Array, onExit_encounter_deck: Array, sector_num:int, player).(name, onEnter_encounter_deck, onIsland_encounter_deck, onExit_encounter_deck, sector_num, "Object", player) -> void:
	upgrade_item_spawn_probability = Probability.HIGH
	useable_item_spawn_probability = Probability.MEDIUM
	generate_ground_items()
	
	
