extends Node
# An island which contains adjacent island connections, an island name and a position. Acts as an abstract class for other concrete islands
# to extend.
class_name Island
# The adjacent islands to this one.
var adjacent_islands := [] setget , get_adjacent_islands
# The number of adjacent island the island has and the max it can have.
var adj_count = 0
const MAX_CONNECTIONS = 4
# The position of the island.
var position := Vector2() setget set_position, get_position
var player
# The name of the island.
var island_name:String setget ,get_island_name


# Create a new island instance passing in the name of the island and a instance of player.
# @Param name:String -- the name of the island.
# @Param player:Player -- An instance of the Player class.
func _init(name:String, player) -> void:
	adjacent_islands.resize(MAX_CONNECTIONS)
	self.player = player
	self.island_name = name
	
# Abstract method, should be implemented by child classes.
func player_interact_with_entity(player)-> void:
	return
	

# Adds an adjacent island to this island and calls this method on the island passed in therefore a first to check whether 
# this island has already been connected with the other, therefore would not connect back up with it.
# @Param island:Island -- the island this is being connected to
# @Param first:bool -- Whether this the other island has already connected with this one in its instance.
func add_adjacent_island(island: Island, first: bool) -> void:
	adjacent_islands[adj_count] = island
	adj_count += 1
	# Adds this Island instance to the adjacent_islands Array.
	if first:
		island.add_adjacent_island(self, false)

# Get all the adjacent islands to this island.
# @Return adjacent_islands: Array -- the islands that are adjacent to this one.
func get_adjacent_islands() -> Array:
	return adjacent_islands

# Set the islands position passing in the 2D vector it will be positioned at.
# @Param pos:Vector2 -- The position the island will be set to.
func set_position(pos: Vector2):
	position = pos

# Get the current position of the island.
# @Return position:Vector2 -- The position of the island.
func get_position() -> Vector2:
	return position

# Gets the name of the island
# @Return island_name:String -- The name of the island.
func get_island_name() -> String:
	return island_name
	
# Checks whether the object is the same as this island.	
# @Param o: Node -- A node object.
func equals(o: Node) -> bool:
	if o as Island:
		var island: Island = o as Island
		if (position != island.get_position()):
			return false
		for index in range (adjacent_islands.size()):
			if (adjacent_islands[index] != island.get_adjacent_islands()[index]):
				return false
		return true
	return false
