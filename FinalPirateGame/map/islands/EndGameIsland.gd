extends "res://map/islands/Island.gd"

# The EndGameIsland is a special type of island therefore inheriting all the properties from the interface island. This islands contains a few things in which is unique to this island, including multiple
# obstacle NPCs which the player must overcome to get the final treasure. The final treasure has a set position and if the player reaches its position they win the game. There is also an OnEnter
# encounter which informs the player to use their clues to win the game.
class_name EndGameIsland
# The different NPC obstacles the player must overcome.
var obstacle1: Array
var obstacle2: NPC
var obstacle3: NPC
# The NPC specifically used to inform the player to use clues.
var onEnter: NPC
var onEnter_encounter = ["Setting:  Beach_with_boat",
"0|Description:  Ye' have made it. The infamous..... Barsea's hideaway. |Option:  1|Desc:  Continue|Need:  Item=null|Give_item:  null|Special_interaction:  false|Save_encounter:  false",
"1|Description:  Now don't ya forget to use those clues, they will help find Old Barsea's treasure.|Option:  2|Desc:  Continue|Need:  Item=null|Give_item:  null|Special_interaction:  false|Save_encounter:  false",
"2|Description:  Choose not to use them or didn't find them, well..... GOOD LUCK!|null|Gold_change:  0|Gunpowder_change:  0|Repairs_change:  0|Crew_change:  0|Rum_change:  0|Morale_change:  0|Give_item:  null|Special_interaction:  false|Save_encounter:  false",
"3|Description:  Never gets here|null|"]

# The NPC in which the player is currently interacting with.
var interacting_npc: NPC setget ,get_interacting_npc

# The constructor of this class which creates all the NPCs on the island and their relevant encounters also.
# @Param name:String -- The name of the island.
# @Param obstacle1_correct: Array -- The encounter for the correct direction choice for the first obstacle, each array element is a line.
# @Param obstacle1_incorrect: Array -- The encounter for the incorrect direction choice for the first obstacle, each array element is a line.
# @Param obstacle2_encounter: Array -- The encounter information for the second obstacle.
# @Param obstacle3_encounter: Array -- The encounter information for the thrid obstacle.
# @Param player:Player -- An instance of player in which the NPCs will interact with.
func _init(name:String, obstacle1_correct: Array, obstacle1_incorrect: Array, obstacle2_encounter: Array, obstacle3_encounter: Array, player).(name, player):
	obstacle1 = [NPC.new(obstacle1_correct, Vector2(1655,560), "Skeleton_pirate", player, 4), NPC.new(obstacle1_incorrect, Vector2(268,560), "Undead_pirate", player, 4) ,
				NPC.new(obstacle1_incorrect, Vector2(995,560), "Undead_pirate", player, 8)]
	obstacle2 = NPC.new(obstacle2_encounter, Vector2(1030,125), "Statue",player, 2)
	obstacle3 = NPC.new(obstacle3_encounter, Vector2(1030,-545), "Treasure",player, 2)
	onEnter = NPC.new(onEnter_encounter, Vector2(1030,1500), "",player, 1)
	
	
# This method checks whether the player should be interacting with any of the entities on the island and if so updates the players state to interating. Additionally
# this checks whether the player has found the treasure, updating the player accordingly.
# @Param player:Player -- The player interacting with NPCs and finding the treasure.
func player_interact_with_entity(player)-> void:
	# If the player is entering the island and hasn't interacted with the onEnter NPC, the player is set to interacing and interacting NPC is the onEnter one.
	if player.current_state == player.Player_states.ENTERING_ISLAND:
		if onEnter != null:
			interacting_npc = onEnter
			player.current_state = player.Player_states.INTERACTING
		# Otherwise the player just enters the island if they have interacted with the NPC.
		else:
			player.current_state = player.Player_states.ON_ISLAND
	# Loops through the obstacles on the island.
	for obstacle in get_obstacles():
		if (obstacle != null):
			# The default distance for interactions is 100, but if the obstacle is one of the first ones, then its increased.
			var distance_to_interact: int = 100
			for obstacle1_obstacle in obstacle1:
				if obstacle == obstacle1_obstacle:
					distance_to_interact = 200
			# If the player is within a proximity then the player is set to interactign and the npc is the obstacle.
			if (player.get_character().get_position().distance_to(obstacle.get_position()) < distance_to_interact):
				interacting_npc = obstacle
				player.current_state = player.Player_states.INTERACTING
				
		
	
	
# Updates the NPC that is interacting with the player to its next stage based on the option the player chose. If the interaction has reached an end,
# the NPC is removed and the player is returned to the island.
# @Param player:Player -- The player interacting with the NPC
# @Param option_chosen:String -- The text option the player has chosen from the NPCs options.
func interact_with_npc(player, option_chosen:String)->void:
	# Interacts with the NPC and if the method returns true, this means the encounter has finished.
	if (interacting_npc.next_encounter_stage(player,option_chosen)):
		# If the interacting_npc was the second or third obstacle, or the onEnter NPC then its set to null.
		if (interacting_npc== obstacle2):
			obstacle2 = null
		elif (interacting_npc == obstacle3):
			obstacle3 = null
			player.set_has_final_treasure(true)
		elif (interacting_npc == onEnter):
			onEnter = null
		# Otherwise all the NPCs from the first obstacle are checked to see whether the player interacted with that obstacle.
		else:
			for index in obstacle1.size():
				# If it was, then the obstacle is set to null.
				if interacting_npc == obstacle1[index]:
					obstacle1[index] = null
		# The interacting_npc is set to null as the interaction has finished and the players state is changed back to being on_island.
		interacting_npc = null
		player.current_state = player.Player_states.ON_ISLAND

# Gets the interacting npc
# @Return interacting_npc: NPC -- The NPC interacting with the player.
func get_interacting_npc() -> NPC:
	return interacting_npc

func process(delta, player):
	pass

# Gets all the obstacles on the island.
# @Return [obstacle1[0], obstacle1[1], obstacle1[2], obstacle2, obstacle3] : Array -- The obstacles on the island (NPCs). 
func get_obstacles() -> Array:
	return [obstacle1[0], obstacle1[1], obstacle1[2], obstacle2, obstacle3]

# Returns the current encounter the player is having
# @Return interacting_npc.get_encounter(): Encounter -- The encounter from the interacting NPC.
func current_encounter() -> Encounter:
	return interacting_npc.get_encounter()
