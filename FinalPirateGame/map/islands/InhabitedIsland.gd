extends "res://map/islands/NormalIsland.gd"

# InhabitedIsland represents any island which has NPCs on it that are Humans (not just objects). This object can hold 
# multiple moving entities as well as a vendor which is used to buy items from. This class extends NormalIsland therefore 
# it also has the normal functionaility of any other island.
class_name InhabitedIsland

# An array of island_entities that move.
var island_entities:Array setget ,get_island_entities
# The type of island it is i.e. Pirate, Navy or Merchant island.
var island_type :String setget ,get_island_type
# An instance of a Vendor
var vendor: Vendor setget ,get_vendor

# Constructor which sets up the island entities including the static, moveable and vendor NPCS 
# and the initialises the probability of items spawning based on the sector. Additionally it calls the super constructor of normalIsland.
# @Param name:String -- The name of the island.
# @Param type: String -- The type of island it is.
# @Param onEnter_encounter_deck: Array -- The encounter information for the entering the island encounter. 
# @Param onIsland_encounter_deck: Array -- The encounter information for the island encounter. 
# @Param onExit_encounter_deck: Array  -- The encounter information for the leaving the island encounter. 
# @Param vendor_encounter_deck: Array -- The encounter information for the vendor encounter. 
# @Param moveable_npc_deck: Array -- The encounter information all the moveable npcs.
# @Param sector_num:int -- The sector the island is at.
# @Param player:Player -- An instance of Player.
func _init(name:String, type: String, onEnter_encounter_deck: Array, onIsland_encounter_deck: Array, onExit_encounter_deck: Array, vendor_encounter_deck: Array, moveable_npc_deck: Array,
			sector_num:int, player).(name, onEnter_encounter_deck, onIsland_encounter_deck, onExit_encounter_deck, sector_num, type, player) -> void:
	island_entities = []
	island_entities.resize(moveable_npc_deck.size())
	construct_moveable_npcs(moveable_npc_deck)
	island_type = type
	# If the sector number is 1, the probability of spawning items it Medium and Low.
	if sector_num == 1: 
		upgrade_item_spawn_probability = Probability.MEDIUM
		useable_item_spawn_probability = Probability.LOW
	# Otherwise the probability is slightly higher.
	else:
		upgrade_item_spawn_probability = Probability.HIGH
		useable_item_spawn_probability = Probability.MEDIUM
	generate_ground_items_with_vendor(vendor_encounter_deck)

# Constructs all the moveable NPCs on the island with their corresponding encounters and initial positions.
# @Param moveable_npc_deck:Array -- An array of NPCs' encounter information.
func construct_moveable_npcs(moveable_npc_deck: Array) -> void:
	var encounter_idx: int = 0
	 # Loops through all the encounter in the deck.
	for encounter in moveable_npc_deck:
		# Gets the npcs type and removes the line from the encounter.
		var npc_type: String = encounter[0].split(":  ")[1]
		encounter.remove(0)
		# Gets whether they are aggressive and remove the line from the encounter.
		var aggresive_string: String = encounter[0].split(":  ")[1]
		encounter.remove(0)
		var aggresive:bool
		# If the text is "false" then aggressive is set to false also, otherwise its set to true.
		if aggresive_string == "false":
			aggresive = false
		else:
			aggresive = true
		# Adds the moveable npc to an array when constructed with the information.
		island_entities[encounter_idx] = MoveableNPC.new(encounter, Vector2(700,300), npc_type, aggresive, player,difficulty)
		encounter_idx += 1

 #Generates all the items on the map in certain ground spaces by calling the super class version as well as placing the Vendor on the island.
# @Param vendor_encounter_deck:Array -- The vendors encounter information.
func generate_ground_items_with_vendor(vendor_encounter_deck:Array) -> void:
	self.generate_ground_items()
	var valid_pos: bool = false
	var ground_space_key_index: int = 0
	# Loops through until a valid place has been found, i.e. a space which doesnt already contain an npc.
	while !valid_pos:
		if !(ground_spaces[ground_spaces.keys()[ground_space_key_index]] as NPC):
			valid_pos = true
		else:
			ground_space_key_index+=1
	# Sets up the vendor and places them in the valid position found above.
	var npc_type: String = vendor_encounter_deck[0].split(":  ")[1]
	vendor_encounter_deck.remove(0)
#	self.vendor_encounter_deck = vendor_encounter_deck
	vendor = Vendor.new(vendor_encounter_deck, ground_spaces.keys()[ground_space_key_index], "Vendor", player,difficulty)
	ground_spaces[ground_spaces.keys()[ground_space_key_index]] = vendor


# The player interacts with an entity on the island checks to see whether the player is in a certain distance to the NPCs that move around. If so
# the player begins interacting with them.
# @Param player:Player -- An instance of player used to interact with the entities on the island.
func player_interact_with_entity(player)-> void:
	var player_pos = player.get_character().get_position()
	var entity_idx_to_remove: int = -1
	for entity_idx in island_entities.size():
		# If the player is within 80 distance, the players state will change to interacting and the interacting_npc 
		# which will be used is changed to the entity the player is close to.
		if (player.get_character().get_position().distance_to(island_entities[entity_idx].get_position()) < 80):
			interacting_npc = island_entities[entity_idx]
			player.current_state = player.Player_states.INTERACTING
			# If the entity is not alive, remove it from the array.
			if (!island_entities[entity_idx].is_alive):
				entity_idx_to_remove = entity_idx
			# If the entity i sstill alive after the encounter then make them not triggered and not aggessive
			else:
				island_entities[entity_idx].triggered = island_entities[entity_idx].STATE.NOT_TRIGGERED
				island_entities[entity_idx].is_opressive = false
		else:
			# Otherwise the entity is IDLE.
			island_entities[entity_idx].current_state = island_entities[entity_idx].NPC_states.IDLE
	# Removes the entity at a certain index if the entity has died.
	if entity_idx_to_remove != -1:
		island_entities.remove(entity_idx_to_remove)
	# Calls the parent method in normal island.
	.player_interact_with_entity(player)

# Get the vendor on the island.
# @Return vendor: Vendor -- The vendor instance on the island.
func get_vendor() -> Vendor:
	return vendor
	
# Get the type of this island.
# @Return island_type:String -- The type of island this is.
func get_island_type() -> String:
	return island_type

# Get the moving NPCs on the island.
# @Return island_entities:Arrau -- The moveable entities on the island.
func get_island_entities() -> Array:
	return island_entities	
	
#Called to move all the moveable NPCs on the island.
# @Param delta :float -- The time cycles.
# @Param player:Player -- An instance of player
func move_entities(delta, player):
	# Loops through the moveable entities and makes them move.
	for entity in island_entities:
		entity.move_entity(player.get_character())
	
# Checks whether two islands are equal based on their island type, position, adjacent islands
# @Param 0:Node -- A Node thats being compared with.
# @Return bool -- True if two islands are equal.
func equals(o: Node) -> bool:
	if o as Island:
		var island: Island = o as Island
		if (island_type != island.get_island_type()):
			return false
		if (position != island.get_position()):
			return false
		for index in range (adjacent_islands.size()):
			if (adjacent_islands[index] != island.get_adjacent_islands()[index]):
				return false
		return true
	return false