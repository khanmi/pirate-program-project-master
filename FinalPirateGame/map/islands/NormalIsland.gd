extends "res://map/islands/Island.gd"

# A normal island reprents any island which is not the Endgame which is a special type of island. This contains the probabilties of items 
# spawning on the island and multiple NPCs representing an NPC which is interacted with when you enter the island, when your on the island
# and when you leave the island.
class_name NormalIsland

# The NPCs for when on the island, when entering the island and when leaving the island.
var interactable_NPC: NPC #setget ,get_interactable_NPC
var onEnter_NPC: NPC #setget ,get_onEnter_NPC
var onExit_NPC: NPC #setget ,get_onExit_NPC
# Probability of these items spawning.
var upgrade_item_spawn_probability: int setget ,get_upgrade_item_spawn_probability
var useable_item_spawn_probability: int setget ,get_useable_item_spawn_probability
# The probabilities values.
enum Probability{
	HIGH,
	MEDIUM,
	LOW
}

# Used for the probability that an item will spawn, the higher the number the less likely.
const probability_dictionary: Dictionary = {Probability.HIGH: 3, Probability.MEDIUM: 5, Probability.LOW: 8}

# Dictionary containing positions on the island as the keys and the values being @See Entity entities.
var ground_spaces: Dictionary = {Vector2(700,300): null, Vector2(1800,200): null, Vector2(320,1100): null,
								Vector2(1200, 910): null, Vector2(2200, 718): null, Vector2(2315, 310): null,
								Vector2(1650, 840): null, Vector2(1070, 700): null} setget ,get_ground_spaces
# The difficulty of the island.
var difficulty: int
# The current NPC which is interacting with the player.
var interacting_npc: NPC


# Create a new island instance including its name, the NPCs with their correspondign encounters
# The type of island it is and its difficulty.
# @Param name:Sring -- The name of the island.
# @Param onEnter_encounter_deck:Array -- An array containing all the encounter that triggers when ariving at the island
# @Param onIsland_encounter_deck --An array containing all the encounter that triggers when on an island.
# @Param onExit_encounter_deck: Array --An array containing all the encounter that triggers whenleaving an island.
# @Param sector_num:int -- The sector the island is at.
# @Param type:String -- The type of island it is.
# @Param player:Player -- An instance of a @See Player.
func _init(name:String, onEnter_encounter_deck: Array, onIsland_encounter_deck: Array, onExit_encounter_deck: Array, sector_num:int, type:String, player).(name, player) -> void:
	difficulty = sector_num
	# If the onIslandEncounterDeck is not an empty array, it creates an NPC containing the relevant information from the array.
	if (onIsland_encounter_deck != []):
		# Gets the npc type which is the first index in the array and removes it when passing to the NPC class.
		var npc_type: String = onIsland_encounter_deck[0].split(":  ")[1]
		onIsland_encounter_deck.remove(0)
		# Creates an instance of NPC with the encounter_deck and the type of npc it is.
		interactable_NPC = NPC.new(onIsland_encounter_deck, Vector2(100,100), npc_type, player, difficulty)
	# If the onEnter_encounter_deck is not an empty array, it creates an NPC containing the relevant information from the array.
	if (onEnter_encounter_deck != []):
		# Gets the npc type which is the first index in the array and removes it when passing to the NPC class.
		var npc_type: String = onEnter_encounter_deck[0].split(":  ")[1]
		onEnter_encounter_deck.remove(0)
		# Creates an instance of NPC with the encounter_deck and the type of npc it is.
		onEnter_NPC = NPC.new(onEnter_encounter_deck, Vector2(0,0), npc_type, player, difficulty)
	# If the onExit_encounter_deck is not an empty array, it creates an NPC containing the relevant information from the array.
	if (onExit_encounter_deck != []):
		# Gets the npc type which is the first index in the array and removes it when passing to the NPC class.
		var npc_type: String = onExit_encounter_deck[0].split(":  ")[1]
		onExit_encounter_deck.remove(0)
		# Creates an instance of NPC with the encounter_deck and the type of npc it is.
		onExit_NPC = NPC.new(onExit_encounter_deck, Vector2(-1000,-1000), npc_type, player, difficulty)
	interacting_npc = onEnter_NPC
	
	
# Spawns the different items that are in the game in set positions. However the items (if any) that spawn will be random.
# This includes placing the interactable NPC in one of the ground spaces randomoly.
func generate_ground_items() -> void:
	randomize()
	var useable_item_types= ["Sword", "Explosive Barrel", "Gun"]
	var npc_pos = randi() % ground_spaces.keys().size()
	var counter = 0
	# Loops through the ground spaces and checks whether the counter integer is the NPC position
	for ground_space in ground_spaces.keys():
		# If so it places the iteractable_NPC in the ground space and sets the NPCs position.
		if counter == npc_pos:
			ground_spaces[ground_space] = interactable_NPC
			interactable_NPC.set_position(ground_space)
		else:
			# Otherwise, it generates a random number and compares it with the probabilities of this island that a
			# useable item will spawn and if it doesnt exceed that, an upgrade item is checked for the same.
			var rand_num = randi() % 10
			if  rand_num > probability_dictionary[useable_item_spawn_probability]:
				ground_spaces[ground_space] = UseableItem.new(useable_item_types[randi() % useable_item_types.size()], difficulty)
			elif rand_num > probability_dictionary[upgrade_item_spawn_probability]:
				ground_spaces[ground_space] = UpgradeItem.new(difficulty)
		counter += 1
		
		
# Method that checks whether the player is within a proximity to any of the entities that are on the island.
# If so the player will interact with the entity so long as they aren't already interacting i.e. the player needs to leave their proximity and walk back if they are still there.	
# If not the player will also be checked to see whether their curent state is leaving or entering as if so it will set the interacting_npc
# to the corresponding one, to signal the player is interacting with them (they also update their own state to interacting).
# @Param player:Player -- An instance of player which is used to check their state and their position.	
func player_interact_with_entity(player) -> void:
	var player_pos = player.get_character().get_position()
	# Loops through all the static ground spaces on the islands and checks whether the player is within a fixed distance, if so and the entity there is not null, it calls interacted_with_entity_at_ground_space
	for space in ground_spaces.keys():
		if (player_pos.distance_to(space) < 50):
			var entity:Entity = ground_spaces.get(space)
			if (entity != null):
				interacted_with_entity_at_ground_space(space, player)
		# If the player is not within proximity of the space and the space contains an NPC, the NPCs state is set to IDLE.
		elif(ground_spaces[space] is NPC):
			ground_spaces[space].current_state = ground_spaces[space].NPC_states.IDLE
	# If the player is leaving an island and their exists an onExitNPC which isn't already interacting, the NPCs interact method is called to initialise the encounter and its state.
	if player.current_state == player.Player_states.LEAVING_ISLAND:
		if(onExit_NPC!= null && onExit_NPC.current_state != onExit_NPC.NPC_states.INTERACTING):
			onExit_NPC.interact(player)
			interacting_npc = onExit_NPC
		# Otherwise the player can leave the island.
		else:
			player.current_state = player.Player_states.LEFT_ISLAND
	# If the player is entering an island and their exists an onEnterNPC which isn't already interacting, the NPCs interact method is called to initialise the encounter and its state.
	if player.current_state == player.Player_states.ENTERING_ISLAND:
		if (onEnter_NPC!= null && onEnter_NPC.current_state != onEnter_NPC.NPC_states.INTERACTING):
			onEnter_NPC.interact(player)
			interacting_npc = onEnter_NPC
		# Otherwise the player can enter the island.
		else:
			player.current_state = player.Player_states.ON_ISLAND
	# Otherwise if the player is just interacting therefore they are already on the island, if the interacting npc is not null and the interacting NPC isnt already interacting (i.e. interacted 
	# with them already therefore need to leave their proximity to retrigger the interaction), the interacting_npc is set up to interact with the player
	elif player.current_state == player.Player_states.INTERACTING:
		if (interacting_npc!= null && interacting_npc.current_state != interacting_npc.NPC_states.INTERACTING):
			interacting_npc.interact(player)
		# Otherwise the player will remain on the island.
		else:
			player.current_state = player.Player_states.ON_ISLAND

# Used for encountes called when the player choose options when interacting with an NPC. Depending on the players state 
# results in the player progressing to the next stage of the corresponding NPC. If the NPC that they player is interacting
# no longer has any options for the player to choose from, it means the interaction has finished therefore it removes the NPC
# from the game to prevent repeat encounters.
# @Param player:Player -- An instance of the player used to check the player state and position
# @Param option_chosen:String -- The option chosen by the player which is passed to the interacting_npc.
func interact_with_npc(player, option_chosen:String)->void:
	# If the player is currently leaving the island and the OnexitNPC hasn't already been interacted with and the next stage of the encounter returns true meaning the encounter has finished,
	# the player can leave the island and the NPC is set to null.
	if player.current_state == player.Player_states.LEAVING_ISLAND:
		if(onExit_NPC != null && onExit_NPC.next_encounter_stage(player, option_chosen)):
			player.current_state = player.Player_states.LEFT_ISLAND
			onExit_NPC = null	
	# If the player is currently entering the island and the OnEnterNPC hasn't already been interacted with and the next stage of the encounter returns true meaning the encounter has finished,
	# the player can enter the island and the NPC is set to null.
	elif player.current_state == player.Player_states.ENTERING_ISLAND:
		if (onEnter_NPC!=null):
			onEnter_NPC.get_encounter().start_encounter(player)
		if(onEnter_NPC!= null && onEnter_NPC.next_encounter_stage(player, option_chosen)):
			onEnter_NPC = null
			player.current_state = player.Player_states.ON_ISLAND
	# Otherwise if the player is just gnerally interacting with an NPC on the island, and it has finished the encounter, if the current interacting NPC is the interactable NPC i.e the static story 
	# based NPC, it is removed so they cant interact with them again.
	elif player.current_state == player.Player_states.INTERACTING:
		if(interacting_npc!=null && interacting_npc.next_encounter_stage(player, option_chosen)):
			if interacting_npc == interactable_NPC && interacting_npc.get_encounter().get_stage().get_options().size() == 0:
				for space in ground_spaces.keys():
					if space == interactable_NPC.get_position():
						ground_spaces[space] = null
				interactable_NPC = null
			player.current_state = player.Player_states.ON_ISLAND
			interacting_npc = null
		
# Called when a special interaction should end therefore changing the player state back to what they were doing previously.
func end_special_npc_interaction()->void:
	interacting_npc.special_interaction.finish_interaction()
		
		
# Finds the corresponding entity at the space the player is at and sets the ground_space so if its an item it will call its interact method i.e. add the item to their inventory and then the item
# is removed. If its an NPC the players current state is changed to interacting and the NPC at the ground space is stored in the interacting_npc.
# @Param tmp_space:Vector2 -- A space on the island (corresponding to the space in ground_spaces above).
# @Param player:Player -- An instance of Player passed to the @See Entity if its an @See Item.
func interacted_with_entity_at_ground_space(tmp_space: Vector2,player):
	for space in ground_spaces.keys():
		# If the space is equal to the space passed in and the Entity is an Item, then the item is interacted with and removed from the 
		# space.
		if space == tmp_space:
			if ground_spaces[space] is Item:
				if(ground_spaces[space].interact(player)):
					ground_spaces[space] = null
			# If the space is an NPC the NPC is set to interacting and the current interacting_npc is this one.
			elif ground_spaces[space] is NPC:
				interacting_npc = ground_spaces[space]
				player.current_state = player.Player_states.INTERACTING
	
# Gets the ground spaces.
# @Return ground_spaces:Dictionary -- The ground spaces with corresponding entities as values.
func get_ground_spaces() -> Dictionary:
	return ground_spaces

# Get the uprade item spawn probability
# @Return upgrade_item_spawn_probability: int -- The probability the item will spawn.
func get_upgrade_item_spawn_probability() -> int:
	return upgrade_item_spawn_probability

# Get the useable item spawn probability
# @Return useable_item_spawn_probability: int -- The probability the item will spawn.
func get_useable_item_spawn_probability() -> int:
	return useable_item_spawn_probability


# Get the interactable NPC
# @Return interactable_NPC:NPC -- The interactable NPC.
func get_interactable_NPC() -> NPC:
	return interactable_NPC

#Get the current encounter.
# @Return interacting_npc.get_encounter():Encounter -- The current encounter.
func get_current_NPC_encounter() -> Encounter:
	return interacting_npc.get_encounter()
	
#Get the intering NPC
# @Return interacting_npc:NPC -- The intering NPC.
func get_interacting_npc() -> NPC:
	return interacting_npc
	
# Checks whether two islands are equal based on their position and connecting islands.
# @Param o:Node -- A Node used to compare against.
func equals(o: Node) -> bool:
	if o as Island:
		var island: Island = o as Island
		if (position != island.get_position()):
			return false
		for index in range (adjacent_islands.size()):
			if (adjacent_islands[index] != island.get_adjacent_islands()[index]):
				return false
		return true
	return false

