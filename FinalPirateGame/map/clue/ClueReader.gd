extends Node

# This class is ued to read the clues from an text file and then constucts the clues based on the text files information for which then can be obtained 
# from this classs.
class_name ClueReader

# An array of all the clues
var clues:Array = []
# The clue in which the player can get next.
var next_clue = 0


# Reads through the text file from the file path and creates clues based off the information it has.
# @Param filepath:String -- The path to the file.
func read(filepath:String)->void:
	#create file
	var file = File.new()
	#read file
	file.open(filepath, 1)
	#var clues : Array = [] 
	var line : String 
	# Loops through the file until the end is reached, appending clues to the clues array.
	while !file.eof_reached():
		line = file.get_line()
		clues.append(create_clue(line))
	file.close()
	
# Creates a Clue object based on the line read by the file reader. 
# @Param line:String -- A string contains a line of clue information.
func create_clue(line: String) -> Clue:
	# The first section of the .txt file is the island_type for the next clues location(i.e. merchant, navy, etc).
	var island_type : String = line.left(line.find("|"))
	# Removes the island_type part of the line.
	line = line.right(line.find("|") + 1)
	# Creates a int representing the option the player will need to choose at the end of the game.
	var end_game_option: int = line.left(line.find("|")) as int
	# Removes the end game option part of the line.
	line = line.right(line.find("|") + 1)
	# The rest of the line is clue information/text.
	var clue_text = line
	return Clue.new(island_type, end_game_option, clue_text)

# Returns the next clue from the clues array and increments the counter.
# @Return clue:Clue -- The next clue the player can get.
func get_next_clue() -> Clue:
	var clue: Clue = clues[next_clue]
	next_clue += 1
	return clue

# Resets the clue count.
func reset() -> void:
	next_clue = 0