extends VideoPlayer

# Controller for displays a cutscene based on the curren state of the game i.e. if the player dies
# to a battle it shows the battle dealth cutscene.

var aspect_ratio = 7.0/4.0 #Aspect ratio for video
var player:Player #create reference to player
var skipped:bool # Whether the player has skipped the cutscene.
# Called when the node enters the scene tree for the first time.
func _ready():
	#Dynamically Resizing video
	var vsize = get_viewport_rect().size
	get_parent().get_node("ColorRect").set_size(vsize) #Add blackbox to cover anywhere thats not covered by the video
	# Sets the size of the video based on the aspect ratio.
	if vsize.y < vsize.x / aspect_ratio:
		set_size(Vector2(vsize.y * aspect_ratio, vsize.y))
		set_position(Vector2((vsize.x - vsize.y * aspect_ratio) / 2, 0))
	else:
		set_size(Vector2(vsize.x, vsize.y / aspect_ratio))
		set_position(Vector2(0, (vsize.y - vsize.x / aspect_ratio) / 2))
# Called every frame. 'delta' is the elapsed time since the previous frame.

#The management of the cutscenes is done here. Many things will change based on what video is playing:
#for example, the amount of time the cutscene will run for and what scene it will change to after the cutscene
# @Param delta:float -- The delta cycle time.
func _process(delta):
	# Sest the resource to laod and the stream time off it or if its been skipped then it shows a releveant scene 
	# that follows this one.
	if stream.resource_path == "res://Videos/Sailing2.webm" && (get_stream_position() >= 4 || skipped):
		get_tree().change_scene("res://map_graphics/MapView.tscn")
	# Sest the resource to laod and the stream time off it or if its been skipped then it shows a releveant scene 
	# that follows this one.
	elif stream.resource_path == "res://Videos/NoCrew.webm" && (get_stream_position() >= 4 || skipped):
		get_tree().change_scene("res://interface/GameOver.tscn")
	# Sest the resource to laod and the stream time off it or if its been skipped then it shows a releveant scene 
	# that follows this one.
	elif stream.resource_path == "res://Videos/NoGold.webm" && (get_stream_position() >= 4 || skipped):
		get_tree().change_scene("res://interface/GameOver.tscn")
	# Sest the resource to laod and the stream time off it or if its been skipped then it shows a releveant scene 
	# that follows this one.	
	elif stream.resource_path == "res://Videos/NoRepair.webm" && (get_stream_position() >= 4 || skipped):
		get_tree().change_scene("res://interface/GameOver.tscn")
	# Sest the resource to laod and the stream time off it or if its been skipped then it shows a releveant scene 
	# that follows this one.	
	elif stream.resource_path == "res://Videos/NoRum.webm" && (get_stream_position() >= 4 || skipped):
		get_tree().change_scene("res://interface/GameOver.tscn")
	# Sest the resource to laod and the stream time off it or if its been skipped then it shows a releveant scene 
	# that follows this one.	
	elif stream.resource_path == "res://Videos/Sinking.webm" && (get_stream_position() >= 4 || skipped):
		get_tree().change_scene("res://interface/GameOver.tscn")
	# Sest the resource to laod and the stream time off it or if its been skipped then it shows a releveant scene 
	# that follows this one.	
	elif stream.resource_path == "res://Videos/SinkingNavy.webm" && (get_stream_position() >= 4 || skipped):
		get_tree().change_scene("res://interface/GameOver.tscn")
	# Sest the resource to laod and the stream time off it or if its been skipped then it shows a releveant scene 
	# that follows this one.	
	elif stream.resource_path == "res://Videos/DeathFight.webm" && (get_stream_position() >= 4 || skipped):
		get_tree().change_scene("res://interface/GameOver.tscn")
	# Sest the resource to laod and the stream time off it or if its been skipped then it shows a releveant scene 
	# that follows this one.	
	elif stream.resource_path == "res://Videos/Intro.webm" && (get_stream_position() >= 84 || skipped):
		get_node("/root/Main").new_game()
	# Sest the resource to laod and the stream time off it or if its been skipped then it shows a releveant scene 
	# that follows this one.	
	elif stream.resource_path == "res://Videos/Credits.webm" && (get_stream_position() >= 102 || skipped):
		get_tree().change_scene("res://MainMenu.tscn")

	pass

# Input from the player is checked to see if it was a mouse click if so the cutscene is set to be skipped.
func _input(event):
	if event is InputEventMouseButton and event.is_pressed():
		skipped = true




