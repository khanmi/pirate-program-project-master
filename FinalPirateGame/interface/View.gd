extends Node

# The view class is the central view controller that handles loading the correct scene based on the current state of the game and stores all relevant information
# to do with the view that needs to stay persistant like the overlay_support mechanics in which it keeps an instance of that class, as well as a Dictionary of island names
# with corresponding graphics. Therefore, this class should be used to help the individual controllers of each view.
class_name View
# A dictionary with each island as the key and its corresponding graphic as the value.
var islands_to_graphics: Dictionary
# The instance of overlay_support which is used to support the overlay scene.
var overlay_support: OverlaySupport
var death_reason = "" #holds the string value for why the player died
# Constructs the instance of overlay_support and sets up the arrays for inhabited and deserted island views.
func _init()->void:
	overlay_support = OverlaySupport.new()

# Used to update the view (scene tree) by instantiating the correct view based on the state passed in, the states in the game
# and an island (the island the player is currently at).
# @Param state:int -- The current state of the game (an int as a value of the enum also passed in)
# @Param states:Dictionary -- All the states the game can be in
# @Param island:Island -- The island the player is currently at.
# @Param tree:SceneTree -- The current scene tree of the game,
func update(state:int, states:Dictionary, island, tree:SceneTree):
	# The scene path of the view thats going to be loaded.
	var scene_path: String
	# Matches the state passed in with on of the enum values from the states dictionary to load the relevant view.
	match state:
		states.MAIN_MENU:
			scene_path = "res://MainMenu.tscn"
		states.MAP:
			scene_path = "res://map_graphics/MapView.tscn"
		states.CHARACTER_SELECT:
			scene_path = "res://CharacterSelectMenu.tscn"
		states.ENDGAME:
			scene_path = "res://IslandsTopDownView/EndgameIsland.tscn"
		# Calls the display_island_view method to figure out which island to display
		states.ISLAND:
			scene_path = display_island_view(island)
		states.ENCOUNTER:
			scene_path = "res://encounters/EncounterScreen.tscn"
		states.PAUSED:
			scene_path = "res://PauseMenu.tscn"
		states.INVENTORY:
			scene_path = "res://Inventory/InventoryScene.tscn"
		states.SHOP:
			scene_path = "res://interface/shop.tscn"
		#give a different cutscene based on how the player died
		states.GAME_OVER: 
			if death_reason == "NoCrew":
				scene_path = "res://Cutscenes/NoCrew.tscn"
			elif death_reason == "Fight":
				scene_path = "res://Cutscenes/DeathFight.tscn"
			elif death_reason == "NoRum":
				scene_path = "res://Cutscenes/NoRum.tscn"
			elif death_reason == "NoRepairs":
				scene_path = "res://Cutscenes/NoRepairs.tscn"
			elif death_reason == "NoGold":
				scene_path = "res://Cutscenes/NoGold.tscn"
			else:
				scene_path = "res://Cutscenes/SinkingNavy.tscn"
		states.WIN:
			scene_path = "res://interface/WinGame.tscn"
		states.BATTLE:
			scene_path = "res://Battle/battleScene.tscn"
		
	# Uncomment line below for testing interface to be shown
	#scene_path = "res://UnitTest/TestScripts.tscn"
	# Changes the scene tree to the correct view based on the scene_path string.
	tree.change_scene(scene_path)
		
			
# Gets the correct island view path to display based on the island passed in.
# @Param island:Island -- The island that is going to be displayed.
# @Return islands_to_graphics[island]:String -- The path to the correct view.
func display_island_view(island) -> String:
#	return name_island_with_graphics[island]
	# If the island is already in the dictionary with the islands associated with its view then just return it.
	if island is String:
		if (!islands_to_graphics.has(island)):
			# Otherwise, set the view String and rng integer in case the island doesnt have a specific view for it.
			islands_to_graphics[island] = "res://IslandsTopDownView/" + island + ".tscn"
		return islands_to_graphics[island]
	else:
		if (!islands_to_graphics.has(island)):
			# Otherwise, set the view String and rng integer in case the island doesnt have a specific view for it.
			islands_to_graphics[island] = "res://IslandsTopDownView/" + island.get_island_name() + ".tscn"
		return islands_to_graphics[island]

#Change the variable with the death reason of the player
# @Param reason:String -- The reason for the players death.
func set_death_reason(reason):
	death_reason = reason
	#death_reason = "NoCrew"
	