extends Control

# This script controls the encounters screen that corresponds to the encounters you can have with NPCS and acts as a controller into the model an updating the model
# based up on the options the player selects. This includes displaying the relevant descriptions of the curren stage of the encounter along with the options 
# the player can choose from, showing those they cannot in grey. It also is used to display the correct setting and NPC on the screen to the player,

# The options available to the player
var options:Dictionary
# The custom button scene.
var custom_button_scene = load("res://encounters/CustomButton.tscn")
# The background path as a string used to load a suitable background
var background_image_path: String setget ,get_background_image_path
# An instance of the singleton, PlayGame.
var game_state: PlayGame
# Whether the encounter has two lots of background noise.
var second_sound:bool
# Time used for the second sound to play periodically.
var time: float

# Sets up the UI once all nodes have been loaded, including the background image and the NPC, if the island is not a deserted island
# If the island is deserted it also replaces the dialogue box with a piece of paper.
func _ready() -> void:
	game_state = get_node("/root/Main").game
	# Checks to make sure the state of the game is an encounter, if so it gets the relevant setting to the current encounter.
	if game_state.current_state == game_state.State.ENCOUNTER:
		background_image_path = ("res://images/"+game_state.get_player().get_ship().get_current_island().interacting_npc.get_encounter().get_setting())
		# If its night, the encounter shows the night time one, otherwise it shows the default one therefore the change
		# of the suffix.
		if (DayNightCycle.get_time() >= 457):
			background_image_path += "_Night.png"
		else:
			 background_image_path += ".png"
		# Calls the method to display the NPC.
		display_npc()
		# Sets the texture of the background to the setting.
		$Background.texture = load(background_image_path)
		# Updates the overlay.
		update()
	set_up_sound()

# Sets the background sound to a people speaking if they are indoor and the constant sound to a correct corresponding sound 
# i.e. if the setting is a beach setting, the sound will be waves.
func set_up_sound()->void:
	# If the setting is a beach then the constant background sound is waves.
	if (background_image_path.find("Beach") != -1):
		$ConstantBackground.stream = load("res://mapAudio/Rough_Sea_Waves.wav")
	# Otherwise if they are in doors then the background will be music and laughter additionally.
	elif (background_image_path == "res://images/Bar_background.png" || background_image_path == "res://images/Inside_house.png"):
		$ConstantBackground.stream = load("res://mapAudio/Pirate song.wav")
		second_sound = true
		$BackgroundSounds/AdditionalSound.stream = load("res://mapAudio/male-laugh-evil_fJmo7jNd.wav")
	# Otherwise the background sound is birds.
	else:
		$ConstantBackground.stream = load("res://mapAudio/morning-and-birds-ambience_MyU73SVu.wav")
		
		
# Displays the NPC in the encounter if its a non deserted island, otherwise the NPC is just an object therefore it doesnt display
# it and changes the speach bubble to a script of paper.
func display_npc()->void:
	var character_type:String
	# If the island is deserted, dont show the NPC and change the background for the text.
	if (game_state.get_player().get_ship().get_current_island() is DesertedIsland):
			#$NPC.hide()
			$dialogue_1.hide()
			$dialogueDeserted.show()
	# Otherwise get the NPC type
	character_type = game_state.get_player().get_ship().get_current_island().interacting_npc.get_character_type()
	# Loads the character types corresponding image
	$NPC.texture = load("res://images/" + character_type  + ".png")
	# If its a merchant the image is flipped.
	if (character_type == "Merchant"):
		$NPC.flip_h = true
	# If its a Navy soldier, its flipped and scaled down.
	if character_type == "Navy":
		$NPC.flip_h = true

# Proceeds to the next stage of the encounter when the user has clicked an option.	
# @Param button: -- The button that was pressed.
func _on_Button_pressed(button) -> void:
	# The game state is updated based on the button that got pressed passing in its text.
	game_state.option_chosen(button.get_child(0).get_child(0).text)
	# Removes all the buttons from the last set of options.
	clear()
	# If the game state is state is still an encounter i.e. it has finished the encounter, then update the graphics.
	if game_state.current_state == game_state.State.ENCOUNTER:
		update()

# Updates the UI with the new options and description of the current stage and updates the overlay.
func update() -> void:
	$Overlay.update()
	# Gets the encounter and displays the text and displays the options calling the display_options method.
	var encounter:Encounter = game_state.player.get_ship().get_current_island().interacting_npc.get_encounter()
	display_options(encounter)
	$dialogue_text.text = encounter.get_stage().get_desc()
	
# Displays the options within the current encounter based on the encounter passed in and attaches the options text
# to buttons which, when pressed progress the encounter based on the option corresponding to it.
# @Param encounter:Encounter -- Displays the options of the current stage of this encounter.
func display_options(encounter:Encounter)->void:
	options = encounter.get_stage().get_options()
	# If there are options
	if (options.size() != 0):
		# Loops through all the options creating a custom button and setting its text to the correct, corresponding to the encounter text.
		for option in options.keys():
			var button = custom_button_scene.instance()
			button.get_child(0).connect("pressed", self, "_on_Button_pressed", [button])
			button.get_child(0).get_child(0).text = String(option[1])
			# If the player doesnt have the valid resources or item, it sets the texture of the button to grey so the 
			# player is aware they cant choose that option.
			if !encounter.check_player_has_item_or_resources(option, game_state.get_player(), false):
				button.get_child(0).set_normal_texture(load("res://images/greyEncounter.png"))
			# Adds the option to the option space VBoxContainer.
			$Option_Space.add_child(button)
	else:
		# Otherwise it makes a button saying press to end the interaction and attaches it to the scene.
		var button = custom_button_scene.instance()
		button.get_child(0).connect("pressed", self, "_on_Button_pressed", [button])
		button.get_child(0).get_child(0).text = "Press to end interaction!"
		$Option_Space.add_child(button)
		

# Removes all the options buttons from the view.
func clear() -> void:
	for child in $Option_Space.get_children():
		child.queue_free()
	
# Returns the background image being used for the current encounter so it uses the same image when reloading the encounter.
# @Return background_image_path:String -- Returns the background image path.
func get_background_image_path() -> String:
	return background_image_path

# Called every cycle and is used to update the sounds and the overlay over time.
func _process(delta):
	# Increments the time and calls the overlay process method.
	time += 0.01
	$Overlay._process(delta)
	# If the background noise isnt playing then it starts from the beginning.
	if ($ConstantBackground.playing == false):
		$ConstantBackground.play(0.0)
	# If there is an additional sound, then every 10 seconds the sound is played.
	if (second_sound && time > 10.0):
		$BackgroundSounds/AdditionalSound.play(0.0)
		time = 0.0