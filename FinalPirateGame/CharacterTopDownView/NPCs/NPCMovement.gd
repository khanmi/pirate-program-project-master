extends "res://CharacterTopDownView/NPCs/Navigation.gd"

# Constructs the NPCs movement with the NPC corresponding to it and the default position.
# @Param npc: Character -- The character the NPC corresponds to.
# @Param default_pos: Vector -- The default position of the NPC.
func construct(npc: Character, default_pos: Vector2)->void:
	# Sets the character to the NPC and sets the position to the default positon passed in.
	var game_state = get_node("/root/Main")
	self.character = npc
	position = default_pos
	set_physics_process(true)
	
# Sets the target of the entity to the Vector2 passed in.
# @Param target: Vector2 -- The target position of the NPC.
func set_target(target: Vector2)->void:
	set_final_target(target)

