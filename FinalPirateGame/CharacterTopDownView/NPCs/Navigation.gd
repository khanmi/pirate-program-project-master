extends RigidBody2D

# Represents any Moving entity on the island which moves using path finding based on the tile
# map of the current scene the entity is within. Should be used for the visual respresentation
# of NPCS and the player character and updates the backened.

###Attributes
#set speed to 200
export var speed = 175
#how close to get to exact target (jitters if tries to be too precise)
const stopping_distance = 5
# path is array of points
var path = []
#where the character is going
var final_target: Vector2

#var npc: MoveableNPC 
var character: Character


# Called when the scene is loads and starts the processing when entering scene and sets the position of the final target 
# to its current position
func _ready():
	#load the stored character position
	final_target = position
	set_physics_process(true)

###getters/setters
# Sets the final target of the entity.
# @Param new_final_target: Vector2 -- The position the target will be set to.
func set_final_target(new_final_target: Vector2):
	final_target = new_final_target
	
###Bunch of little functions to make code more readable
# Returns whether the path leads anywhere
# @Return (path.size() > 1):bool -- Whether the entity is moving anywhere.
func going_somewhere()->bool:
	#if path size is 1, already on target so not going anywhere
	return (path.size() > 1)

# Gets path to target from the current position to the target position in global terms.
# @Param target:Vector2 -- The position the entity wants to get to.
# @Return :Array -- An array of vectors leading to the final position from the current.
func path_to(target: Vector2)->Array:
	#get Navigation2D node
	var navigation_2D_node = get_tree().get_root().get_node("Node2D/Navigation2D")
	#return path given by nav node
	return navigation_2D_node.get_simple_path(get_global_position(), target, false)

# Returns distance to target from the global position of the entity.
# @Param target: Vector2 -- The target position of the entity.
# @Return :Vector2 -- The distance vector from the position to the target.
func distance_to(target: Vector2)->Vector2:
	#get position vector and subtract from target vector, resultant vector is displacement vector
	return target - get_global_position()

# Returns direction to target from the current position.
# @Param target:Vector2 -- The target of the entity.
# @Return :Vector2 -- The direction vector from the position to the target.
func direction_to(target: Vector2)->Vector2:
	return distance_to(target).normalized()

# Returns true/false depending on position relative to target. If the player is near to the final
# position then there are there.
# @Return (near_target and is_final_target)|| (character.get_position() == character.get_target()):bool -- Whether the target is near the final position or if they are 
# 2 array position off
func there_yet()->bool:
	#get distance from target
	var distance = distance_to(path[1])
	#bools for the two states when close enough
	#if distance is less than or equal to stopping distance, close enough
	var near_target = distance.length() <= stopping_distance
	#if only 2 points left in path, must be going to last point, or at last point
	var is_final_target = path.size() <= 2
	#return whether close enough
	return (near_target and is_final_target)  || (character.get_position() == character.get_target())

# Face the sprite towards a point 
# @Param target:Vector2 -- The target position of the entity.
func face_towards(target: Vector2)->void:
	#current tilemap coords is start of path
	var current_position = path[0]
	#sprite is 0th child of the player's RigidBody2D
	var sprite = get_child(0)
	#get the angle from the sprite's position to the target position, and convert to degrees
	var angle = rad2deg(current_position.angle_to_point(target))
	#set sprites rotation to the angle towards the target
	sprite.rotation_degrees = angle + 90

# Stops the rigid body
func stop()->void:
	set_linear_velocity(Vector2(0, 0))



###NAVIGATION FUNCTION FOR EVERY FRAME
# If the player is going somewhere and they aren't already there then they move a point closer
# to the final target position and sets the direction of the entity accordingly. The relevant 
# animation is also played.
func _physics_process(delta)->void:
	# Get path to target
	path = path_to(final_target)
	# if the path has more than one point
	if going_somewhere():
		get_child(0).play("walking")
		#if not there yet
		if not there_yet():
			#print("in here?")
			#what is the next point to go to (path[0] is current location)
			var next_point = path[1]
			#get direction to go
			var direction = direction_to(next_point)
			#set sprite's rotation to the direction of travel
			face_towards(next_point)
			#velocity is speed with direction
			set_linear_velocity(direction*speed)
			character.set_target(path[path.size() - 1])
		#if there
		else:
			#stop
			get_child(0).play("idle")
			stop()
		#redraw on screen
		update()
	else:
		get_child(0).play("idle")
		stop()
	#save new position to backend
	character.set_position(position)

# @Override from child classes.
func set_target(target: Vector2):
#	set_final_target(target)
	pass