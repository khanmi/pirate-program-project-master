extends Control

# This script is used for the visual representation of the Battle scene. This sets up background and NPC shown based on 
# the current encounter and has buttons to allow the player to choose from a range of battle options including 
# a Gun shot, explosive barrel and several sword attacks.

# Stores the enemy health label
onready var lbl_enemy_HP = get_node("EnemyHealth")
# An instance of the current Battle with an NPC 
var battle:Battle
# The setting of the battle.
var setting:String

# Sets up the current scene including setting up the visuals of the battle i.e. the background, the sounds and 
# all relevant text. It also sets the overlay up and sets the buttons that the player cannot choose to the grey button
# indicating it to the player.
func _ready()->void:
	# Gets the battle from the currenlty interacting npc.
	battle = get_node("/root/Main").game.get_player().get_ship().get_current_island().get_interacting_npc().special_interaction
	# find and set setting from the current encounter with an NPC.
	setting = get_node("/root/Main").game.get_player().get_ship().get_current_island().get_interacting_npc().get_encounter().setting
	# Calls to set up the background image
	set_background()
	# Find enemy type and use to set image
	var enemy = get_node("/root/Main").game.get_player().get_ship().get_current_island().get_interacting_npc().get_character_type()
	$Enemy.texture = load ("res://images/" + enemy + ".png")
	# Updates the overlay to the correct numeric values.
	$Overlay.update()
	# Find player's equipped sword and display its corresponding level
	var equipped_sword: UseableItem = get_node("/root/Main").game.get_player().get_equipped_sword()
	if (equipped_sword != null):
		$SwordInfo/EquippedSwordInfo.text = "Your equipped sword is level: " + String(equipped_sword.get_item_level()) + "."
	# If they don't have equipped sword, the information is hidden.
	else:
		$SwordInfo.hide()
	# Updates the stats and the buttons and sets up the sound
	update_stats()
	set_up_sound()
		
# Updates the corresponding stats for the enemies health, the round information i.e. who and how they won the round
# and the sets the buttons that the player cannot choose to grey to indicate it to the player.
func update_stats()->void:
	# Gets the players items.
	var items : Array = get_node("/root/Main").game.get_player().get_items()
	# Whether the player has a gun and an explosive barrel.
	var has_gun: bool = false
	var has_barrel: bool = false
	#check whether gun and barrel options are valid
	for item in items:
		if item != null:
			# If the name of the item is a gun then the player has a gun
			if item.get_item_name() == "Gun":
				has_gun = true
			# If the name of the item is an explosive barrel, then they have a explosive barrel.
			elif item.get_item_name() == "Explosive Barrel":
				has_barrel = true
	# Grey out invalid options including if the player hasn't got a gun or don't have enough gunpowder
	# they cannot choose that option. Same for if they dont have a gunpowder barrel.
	if !has_gun || get_node("/root/Main").game.get_player().get_resource_value("Gunpowder") < 20:
		$PlayerActions/GunAttack.icon = load("res://images/greyEncounter.png")
	if !has_barrel:
		$PlayerActions/BarrelAttack.icon = load("res://images/greyEncounter.png")
	# Update HP display for the enemy and calls methods to set the round information and the correct options the player can
	# choose out of.
	lbl_enemy_HP.text = "Enemy Health: " + str(battle.enemy_crew)
	display_round_info()
	display_correct_options()
	
# Displays the correct options for the player to click depending on the situation i.e.
# if the player has killed the enemy NPC, it will display the end button and display the information relevant to the 
# battle ending.	
func display_correct_options()->void:
	# If they have no crew left.
	if battle.enemy_crew < 1:
		# Shows the finish battle button to return to the encounter and shows the finished label
		$FinishBattle.show()
		$lblFinished.show()
		# Hides all the players actions.
		$PlayerActions.hide()
		
# Displays the round information based on the round that occured within the battle including the text of 
# who won the fight and what actions they chose to win. If a round hasn't been played then it just hides
# the text area for displaying the battle information.
func display_round_info() -> void:
	# If a round hasn't been played, it returns as it shouldn't show any information.
	if !battle.round_played:
		return
	var round_info_text:String
	$RoundInfoBackground.show()
	# If the player won it displays a relevant message of it and why.
	if battle.who_won == "Player":
		round_info_text = "You won using a " + battle.player_action + " against the enemy's " + battle.enemy_action + "!"
	# If the enemy won it displays a relevant message of it and why.
	elif battle.who_won == "Enemy":
		round_info_text = "You lost using a " + battle.player_action + " against the enemy's " + battle.enemy_action + "!"
	# If it was a draw it shows the relevant message and why.
	elif battle.who_won == "Draw":
		round_info_text = "You drew, both using a " + battle.player_action + "!"
	else:
		$RoundInfoBackground.hide()
	# Sets the text info to the label on the screen.
	$RoundInfo.text = round_info_text

# Progresses the current battle with the player choosing the Heavy attack option and players the 
# correct sound and updates all the UI.
func _on_HeavyAttack_pressed():
	# Tell battle player is doing Heavy Attack
	battle.next_round("Heavy Attack")
	# Update UI
	$Overlay.update()
	update_stats()
	# Play heavy sword sound
	play_event_sound("res://mapAudio/sword-whoosh-in-hall_GyaJhr4_.wav")

# Progresses the current battle with the player choosing the Parry option and players the 
# correct sound and updates all the UI.
func _on_Block_pressed():
	# Tell battle player is doing Parry
	battle.next_round("Parry")
	# Update UI
	$Overlay.update()
	update_stats()
	# Play parry sound
	play_event_sound("res://mapAudio/sword-impact-shield_MyKmZqEO.wav")

# Progresses the current battle with the player choosing the Shoot option and players the 
# correct sound and updates all the UI.
func _on_GunAttack_pressed():
	# Tell battle player is doing Shoot
	battle.next_round("Shoot")
	# Update UI
	$Overlay.update()
	update_stats()
	# Play gunshot sound
	play_event_sound("res://mapAudio/flare-gun-shot_MyNOlvN_.wav")
	
# Progresses the current battle with the player choosing the Explosive barrel option and players the 
# correct sound and updates all the UI.
func _on_BarrelAttack_pressed():
	# Tell battle player is doing Explosive Barrel
	battle.next_round("Explosive Barrel")
	# Update UI
	$Overlay.update()
	update_stats()
	# Play explosion sound 
	play_event_sound("res://mapAudio/jg-032316-sfx-explosion-1.wav")

# Progresses the current battle with the player choosing the Light attack option and players the 
# correct sound and updates all the UI.
func _on_BasicAttack_pressed():
	# Tell battle player is doing Light Attack
	battle.next_round("Light Attack")
	# Update UI
	$Overlay.update()
	update_stats()
	# Play light sword sound
	play_event_sound("res://mapAudio/sword-zing_zyaYcf4O.wav")

# Called every cycle with the delta value. Updates the UI and the background sound.
func _process(delta):
	# Update UI and background sound.
	$Overlay._process(delta)
	play_background_sound()

# Ends the current battle the player is having with the NPC.
func _on_FinishBattle_pressed():
	battle.finish_interaction()

# Loads and plays the sound from the path passed in if the player has played the next round.
# @Param sound_path:String -- The path to the sound file to output.
func play_event_sound(sound_path:String):
	# If the player has completed the next round of battle.
	if battle.round_played:
		# Load sound file
		$EventSound.stream = load(sound_path)
		# Play sound file
		$EventSound.play(0.0)

# Plays the background sound of the scene if its not already playing.
func play_background_sound()->void:
	# If background sound ended
	if ($BackgroundSound.playing == false):
		# Start from beginning
		$BackgroundSound.play(0.0)

# Sets up the background sound based on the setting. If its a beach the sound is waves, if indoor setting
# then it uses the pirate song, otherwise it uses the birds chirping.	
func set_up_sound()->void:
	# If the setting is a beach scene, it loads the waves sound.
	if (setting.find("Beach") != -1):
		$BackgroundSound.stream = load("res://mapAudio/Rough_Sea_Waves.wav")
	# If the setting is indoors it loads the pirate song.
	elif (setting == "Bar_background" || setting == "Inside_house"):
		$BackgroundSound.stream = load("res://mapAudio/Pirate song.wav")
	# Otherwise it loads the birds sound.
	else:
		$BackgroundSound.stream = load("res://mapAudio/morning-and-birds-ambience_MyU73SVu.wav")

# Sets the background of the scene based on the setting of the encounter and whether 
# its a night time setting.
func set_background()->void:
	# Build image path...
	# Begins with setting name
	var image_path:String =  "res://images/" + setting
	# If the time is past 457 seconds, it becomes night therefore if the setting is not indoors 
	# it loads the night time version.
	if (DayNightCycle.get_time() >= 457 && 
		(setting != "Bar_background" && setting != "Inside_house")):
		# Ends with _Night if time is late
		image_path += "_Night.png"
	else:
		# Ends normally if daytime 
		image_path += ".png"
	# Load image 
	$Background.texture = load(image_path)