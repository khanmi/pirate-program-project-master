extends Node

# The PlayGame class acts as the central class for the game, and coordinates with the map, islands and player to conduct the regular
# game player mechanics.
class_name PlayGame
# The view that is called to update itself when the state of the game changes.
var view:View
var centre = Vector2(700,410)
# A single instance of the map that exists.
var map: Map setget ,get_map
# The player (singl-player game)
var player: Player setget ,get_player
# Used to stop the player from going to the next sector without pressing on the first island
var entered_first_island: bool

# Reprents the current state of the game, therefore the view should update itself according to the current state
# i.e. if the State is MAP the map scene should be showing as the game is currently focusing on the map aspect.
enum State {MAIN_MENU, MAP, ISLAND, ENCOUNTER, PAUSED, INVENTORY, WIN, GAME_OVER, CHARACTER_SELECT, SHOP, ENDGAME, BATTLE}
var current_state

# The constructor of the game, sets up the num of days to 0, the booleans for winning and loosing to false,
# creates a new instance of a player and the map and sets the current state of the game to the MAP.
func _init():
	# Resets the game.
	# Sets up the player object
	player = Player.new()
	# Creates the map with the player ship and the centre of the map.
	map = Map.new(centre, player.get_ship(), player)
	entered_first_island = false
	current_state = State.MAP

func update_player_position(pos):
	player.character.position = pos

# Returns the map
# @return map:Map -- the instance of the Map class.
func get_map() -> Map:
	return map

# Returns the player
# @Return player:Player -- the player instance.
func get_player():
	return player

	
# Sets the current_state to the INVENTORY, therefore the view should update accordingly.
func display_inventory():
	current_state = State.INVENTORY
	
#Sets state to SHOP, view will update
func displayShop():
	current_state = State.SHOP

# Checks whether  the winning conditions for the player have been met and sets the state of the game to WIN if so.
# @Returns --  True if the game has been won.
func check_if_won() -> bool:
	if player.has_final_treasure():
		current_state = State.WIN
		return true
	return false


# Changes morale based on how much rum there is, then drinks rum, then checks for mutiny
func crewDrinks() -> void:
	#temporary variables to make this readable
	var rum = player.get_resource_value("Rum")
	var crew = player.get_resource_value("Crew")
	var morale = player.get_resource_value("Morale")
	#change morale by diffrernce between crew and rum
	var rumCrewDifference = rum - crew
	player.change_resource_amount("Morale", rumCrewDifference)
	morale = player.get_resource_value("Morale")
	#maximum morale is 100
	if morale > 100:
		player.set_resource_value("Morale", 100)
		morale = player.get_resource_value("Morale")
	#min morale is 0.
	elif morale < 0:
		player.set_resource_value("Morale", 0)
		morale = player.get_resource_value("Morale")
	#consume rum equal to multiplier of crew
	var rumDrinkMultiplier = 0.1 #This will probably need to be around 0.2 but hey we won't know until playtesting
	var amount:int = int(round(-rumDrinkMultiplier * crew))
	# If the number it generates is lgreater than -1, is sets the amount to change it to minus 1
	if amount > -1:
		amount = -1
	player.change_resource_amount("Rum", amount)
	# Sets the player rum to 0 if they have ran out.
	if (player.get_resource_value("Rum") < 0):
		player.set_resource_value("Rum",0)
	#if morale < 50, lose 15% of crew to deserting
	if morale < 50:
		var crew_lose:int =  int(round(crew * -0.15))
		# If the amount change is greater than -1 it sets it to -1 so it actually has an affect.
		if crew_lose > -1:
			crew_lose = -1
		player.change_resource_amount("Crew", crew_lose)

# Move the player to an island (or visit the island they are currently at if its the same island), move the navy ship and then check the 
# winning and loosing conditions. If not met, change the state of the game to ISLAND.
# @Param island:Island -- is the island the player wants to move to/visit if already there.
func move_player_ship_to_island(island) -> void:
	# Checks whether the players ship is at the island passed in.
	var visiting_current_island = map.is_visiting_island(island)
	# Checks whether the island the player is attemping to move to is valid and if so the player ship will move to that island.
	var valid_move = map.move_player_ship(island,false)
	# If the move was valid, or if the player is visiting the current island they are at, then increase then move the navy ships.
	if (valid_move || visiting_current_island):
		entered_first_island = true
		# The crew drinks every time you move or visit an island.
		crewDrinks()
		# If the player is visting the island, it sets the player characters position to the default one for the island.
		if (visiting_current_island):
			player.get_character().set_position(player.get_character().get_default_position())
			# If the island is an endgame island, the state is endgame otherwise its island.
			if !(island is EndGameIsland):
				current_state = State.ISLAND
			else:
				current_state = State.ENDGAME
			# Changes the player state to entering the island (used for the encounters), and calls the interact with
			# entity method as the island may have an onEnter encounter. The start encounter method is also called.
			player.change_state(player.Player_states.ENTERING_ISLAND)
			player.get_ship().get_current_island().player_interact_with_entity(player)
			start_island_encounter()
				
					
# Called when the player exits an island. This will update the islands current_interaction to the Exit Interaction.
# This will then cause an encounter to occur if that island has an exit encounter.
func leave_island() -> void:
	# If the island isn't an instance of EndGameIsland class, it will update the islands current interaction to the Exit interaction
	if !player.get_ship().get_current_island() as EndGameIsland:
	#	player.get_ship().get_current_island().set_current_interaction(player.get_ship().get_current_island().Interaction.EXIT)
		player.change_state(player.Player_states.LEAVING_ISLAND)
		player.get_ship().get_current_island().player_interact_with_entity(player)
		start_island_encounter()
	
# Updates the current state of the game due to an encounter being initiated.
func start_island_encounter() -> void:
	update_current_state()	
			
			
# Changes the state of the game to INVENTORY, if button has been pressed and the state of the game is not in the: MAIN_MENU, GAME_OVER
# WIN or PAUSED.
func _toggle_inventory():
	if Input.is_action_just_pressed("OpenInventory"):
		if (current_state != State.MAIN_MENU && current_state != State.GAME_OVER 
			&& current_state != State.WIN && current_state != State.PAUSED):
			current_state = State.INVENTORY

# Changes the state of the game to PAUSED, if button has been pressed and the state of the game is not in the: MAIN_MENU, GAME_OVER
# or WIN. 
func _toggle_pause_menu():
	if Input.is_action_just_pressed("OpenPauseMenu"):
		if (current_state != State.MAIN_MENU && current_state != State.GAME_OVER && current_state != State.WIN):
			current_state = State.PAUSED
			DayNightCycle._on_stop_game()
			

# Checks the loosing condition of the game i.e. if the navy has caught up to the player or if the player has run out of a 
# resource thats crucial for them to survive.
# @Return :bool -- True if the player has ran out of a resource or the navy ships has caught up to the player.
func has_lost()-> bool:
	for resource in player.get_resources():
		# If the resource is gunpowder or repairs the game ends if the player has less than 0.
		if ((resource == "Gunpowder" || resource == "Repairs") && player.get_resource_value(resource) < 0):
			current_state = State.GAME_OVER
			return true
		# If the resource is crew that the player has 0 or less, they loose the game also
		elif ((resource == "Crew") && player.get_resource_value(resource) <= 0):
			current_state = State.GAME_OVER
			return true
	# Otherwise the player looses if the navy has caught up to them.
	return map.navy_reached_player()
	
	
# Updates the current state of the game based on the state of the player.	
func update_current_state()->void:
	# If the player has changed state since the last call to this method.
	if player.current_state != player.prev_state:
		# Checks whether the players current state matches a series of states.
		match(player.current_state):
			# If the player is leaving, entering an island or interacting with an NPC the state is changed to the encounter state.
			player.Player_states.LEAVING_ISLAND, player.Player_states.ENTERING_ISLAND, player.Player_states.INTERACTING:
				current_state = State.ENCOUNTER
			# If the player has left the island, the game state is changed to the map.
			player.Player_states.LEFT_ISLAND:
				current_state = State.MAP
			# If the player is on an island, if its an endgame island the state is changed to endgame otherwise its changed to island.
			player.Player_states.ON_ISLAND:
				if player.get_ship().get_current_island() is EndGameIsland:
					current_state = State.ENDGAME
				else:
					current_state = State.ISLAND
			# If the player is shopping, the game state is changed to the shop.
			player.Player_states.SHOPPING:
				current_state = State.SHOP
			# If the player is battling the game state is changed to battle.
			player.Player_states.BATTLING:
				current_state = State.BATTLE
			player.Player_states.SELECTING_CHARACTER:
				current_state = State.CHARACTER_SELECT
		# Updates the previous state to the current state so can be used to test whether state has changed in the future.
		player.prev_state = player.current_state
		check_if_won()
	
				
	
# The player has chosen an encounter option therefore call the interact_with_npc method on the relevant island i.e the island the player is at.
# @Param option_chosen:Stirng -- the option the player has chosen containing a String of text
func option_chosen(option_chosen: String) -> void:
	player.get_ship().get_current_island().interact_with_npc(player,option_chosen)
	update_current_state()
	
	
	
# This method is called every tick which, Moves the players ship and navy ship to their target destination each tick, check whether the
# navy ship has caught the player and whether the inventory or pause menu has been toggled on.
func _process(delta) -> void:
	map.move_navy_ship()
	if has_lost():
		current_state = State.GAME_OVER
	# Checks whether the player has pressed the inventory or pause buttons.
	_toggle_inventory() #open/close inventory
	_toggle_pause_menu()
	# If the state of the game is MAP, the navy and player ships are moved.
	if current_state == State.MAP:
		map.get_player_ship().move_ship(delta)
		map.get_navy_ship().move_ship(delta)
	# Check to see if the current state may result in a change of gameplay loop.
	if current_state == State.ISLAND || current_state == State.ENDGAME || current_state == State.SHOP || current_state == State.BATTLE || current_state == State.CHARACTER_SELECT:
		# If the player is on an island it calls the player_interact_with_entity method with an instance of a player to check whether they are interacting with an entity.
		if player.current_state == player.Player_states.ON_ISLAND:
			player.get_ship().get_current_island().player_interact_with_entity(player)
			# If the island is an inhabited island, it also move the entities on that island.
			if player.get_ship().get_current_island() is InhabitedIsland:
				player.get_ship().get_current_island().move_entities(delta, player)
		# Updates the state of the game and checks to see if the player has won the game.
		update_current_state()
		check_if_won()
	
	
