extends Node2D


# This mapview class represents the visual representaton of the map inlcuding showing all the island with lines connecting them together
# where ever the player is able to go, the player and navy ship scene used to show the player where each are (i.e. sets their positions 
# targets and etc from the backend). Also shows the player a button to go to the next sector if they are at the end of a sector
# and shows them information about the islands if they hover over them

class_name MapView
# The paths to the deserted and inhabited islands stored into arrays
const deserted = ["res://tiles/DesertedIsland1.png", "res://tiles/DesertedIsland2.png"]
const inhabited = ["res://tiles/HabittedIsland1.png", "res://tiles/HabittedIsland2.png", "res://tiles/HabittedIsland3.png"]
# The island graphics scene and the button script use to show the end of sector button.
const island_scene = preload("res://map_graphics/IslandGraphic.tscn")
const button_script = preload("res://encounters/CustomButton.tscn")

# The map instance and player
var map:Map
var player:Player
# Whether an island has been clicked.
var clicked_island:bool
# Dictionary with the island with their corresponding visuals.
var island_with_images: Dictionary
# Whether the end of sector button should be attached.
var should_attach_sector_button:bool
# Counters for the two different island types used to iterate over the island arrays.
var deserted_counter:int = 0
var inhabited_counter:int = 0

var island_array = []
var counter = 0

# When all nodes are loaded in, set up the variables for the map and the player from the model and call the function to
# set up the islands.
func _ready() -> void:
	var game_state = get_node("/root/Main")
	# Sets up the player and the map
	map = game_state.game.map
	player = game_state.game.player
	# Sets up the islands
	set_up_islands_and_player(map,player)
	# Attches the sector button
	attach_sector_button()
	$ParallaxBackground2/Overlay.update()
	
# Creates a show the next sector button if the player is at a end island ready to progress to the next sector. If they are on the first island
# and havnt visited it yet it also shows the player a tutorial message and hides the next sector button until they have done the first island.
func attach_sector_button() -> void:
	should_attach_sector_button = false
	var current_sector: Array = map.get_sector(player.get_ship().get_current_sector())
	# Checks whether the player is on the last island of the current sector they are at.
	if (player.get_ship().get_current_island() == current_sector[current_sector.size() - 1] && player.get_ship().get_current_sector() != map.get_sectors().size()
		&& get_node("/root/Main").game.entered_first_island):
		# Sets up the button with a signal for when its pressed and shows the button.
		$ParallaxBackground2/CustomButton.get_child(0).get_child(0).text = "Click to advance to the next sector"
		$ParallaxBackground2/CustomButton.get_child(0).connect("pressed", self, "_on_Button_pressed")
		$ParallaxBackground2/CustomButton.show()
	# Othewise the tutorial message is shown and the custom button is hidden
	else:
		$ParallaxBackground2/TutorialMessage.show()
		$ParallaxBackground2/CustomButton.hide()
	# If the player has entered the tutorial island this stuff goes.
	if (get_node("/root/Main").game.entered_first_island):
		$ParallaxBackground2/TutorialMessage.hide()
	else:
		$ParallaxBackground2/TutorialMessage.show()

# Loops through each island within the sector and constructs a visual sprite representation of it at the same positon.	It 
# also connects the graphics with signals so when they are clicked they trigger the player to enter the island
# and position the islands in the scene tree. The player and navy ship are also set to the correct position
# @Param temp_map: Map -- A map instance
# @Param temp_player:PLayer -- A player instance.
func set_up_islands_and_player(temp_map:Map, temp_player:Player) -> void:
	#r Loops through the islands in the current sector.
	for island in map.get_sector(temp_player.get_ship().get_current_sector()):
		# Gets the correct image.
		var image = get_island_image_path(island)
		var island_graphic = island_scene.instance()
		#Adds the island graphic with the island as the value into the dictionary
		island_with_images[island_graphic] = island
		# Links every island graphic with a signal that will be emitted if the node is pressed.
		island_graphic.connect("island_clicked", self, "_on_IslandGraphic_island_clicked")
		island_graphic.connect("mouse_entered", self, "_on_IslandGraphic_island_hovered", [island_graphic])
		island_graphic.connect("mouse_exited", self, "_on_IslandGraphic_island_exited", [island_graphic])
		# Sets the texture of the island to the correct image and positions it.
		island_graphic.get_child(0).set_texture(image)
		island_graphic.position = island.get_position()
		# Inserts the island with the counter into the island array
		island_array.insert(counter,island.get_position())#island_array for position islands 
		counter=counter+1
		add_child(island_graphic)
	# Sets the graphics position based on the island in the model.
	$NavyShip.set_position(map.get_navy_ship().get_position())
	$ShipGraphic.set_position(player.get_ship().get_position())
	move_child($NavyShip, get_child_count())
	move_child($ShipGraphic, get_child_count())
	get_child_count()
	if counter != 1:
		_draw()#draw lines function 
		
# Gets the correct PackedScene for the visual representaton of the island passed in.
# @Param island:Island -- The island to get the visual for.
# @Return image:PackedScebe -- The visual represenation scene.
func get_island_image_path(island:Island)->PackedScene:
	var rand_num = 0
	var image:PackedScene
	# Generates a random graphic of either deserted or inhabited islands based on its type.
	if island is DesertedIsland:
		# Sets the number to the deserted counter
		rand_num = deserted_counter
		# Loads the image
		image = load(deserted[rand_num])
		deserted_counter += 1
		# If the counter has reached the size of the array then it loops back around.
		if (deserted_counter == deserted.size()):
			deserted_counter = 0
	else:
		# Sets the number to the inhabited counter
		rand_num = inhabited_counter
		image = load(inhabited[rand_num])
		inhabited_counter += 1
		# If the counter has reached the size of the array then it loops back around.
		if (inhabited_counter == inhabited.size()):
			inhabited_counter = 0
	return image

# Draws the lines between the islands.
func _draw():
	
	_line(0,2,2)#to connect island 0-2
	_line(2,5,3)#to connect island 2-5
	
	_line(0,1,1)#to connect island 0-1
	_line(1,3,2)#to connect island 1-3
	
	_line(2,8,2)#to connect island 2-4-6-8
	_line(5,11,2)#to connect island 5-7-9-11
	_line(10,12,2)#to connect island 10-12
	
	_line(12,13,1)#to connect island 12-13
	
	_line(1,10,3)#to connect island 1-4-7-10
	_line(3,12,3)#to connect island 3-6-9-12
	_line(8,11,3)#to connect island 8-11
	
	_line(11,13,2)#to connect island 11-13

# Method to draw lines giving start and finish position of island 
# @Param start -- The start position
# @Param finish -- The end position
# @Param incrementer -- Increments this to the start.
func _line(start, finish, incrementer):
	if counter != 1:
		# While the start hasnt reached the end meaning the whole line still hasnt been drawn.
		while(start != finish):
			#draws the line with two positions and increments the counter.
			draw_line(island_array[start],island_array[start+incrementer],Color(1,1,1,1),3,false)
			start=start+incrementer
			

# Moves the ship and rotates it towards the island it is moving to each tick. This also plays the relevant sounds for the background
# noise along with hiding the navy ship if they aren't within the same sector.
func _process(delta) -> void:
	# If it should attach the button and they are at their final position then the button is attached.
	if should_attach_sector_button && (get_node("/root/Main").game.get_map().get_player_ship().get_position() == 
			get_node("/root/Main").game.get_map().get_player_ship().get_target()):
		attach_sector_button()
	# If the overlay is not null then it processes the overlay
	if $ParallaxBackground2/Overlay != null:
		$ParallaxBackground2/Overlay._process(delta)
	# If the background sound is not playing then it starts to play.
	if ($BackgroundSea.playing == false):
		$BackgroundSea.play(0.0)
	# If the target position of the backend is not the same as the target then the graphics updated to the positon.
	if (player.get_ship().get_position() != player.get_ship().get_target()):
		$ShipGraphic.position = player.get_ship().get_position()
		# Rotates the players ship accordingly.
		var angle = rad2deg(player.get_ship().get_position().angle_to_point(get_node("/root/Main").game.player.get_ship().target))
		$ShipGraphic.rotation_degrees = angle + 180
		# If the ship sailing isnt playing then it starts playing.
		if ($ShipGraphic/ShipSailingSound.playing == false):
			$ShipGraphic/ShipSailingSound.playing = true
	# If they have reached their target then the sounds stops playing.
	else:
		if ($ShipGraphic/ShipSailingSound.playing == true):
			$ShipGraphic/ShipSailingSound.playing = false
	# If the navy ship isnt at its target then it updates it.
	if (map.get_navy_ship().get_position() != map.get_navy_ship().get_target()):
		$NavyShip.set_position(map.get_navy_ship().get_position())
	# If the navy ship is at a different sector then the ship is hidden otherwise its shown.
	if (map.get_navy_ship().get_current_sector() != map.get_player_ship().get_current_sector()):
		$NavyShip.hide()
	else:
		$NavyShip.show()

# If one of the graphic islands are clicked it calls the function in main to update the game state.
# @Param island:Island -- The island clicked.
func _on_IslandGraphic_island_clicked(island, event) -> void:
	# If the player has reached their target then it updates the backend by moving the players ship to the correct island.
	if (player.get_ship().get_position() == player.get_ship().get_target()):
		get_node("/root/Main").game.move_player_ship_to_island(island_with_images.get(island))
		# Updates the overlay and sets the attach sector button to true.
		$ParallaxBackground2/Overlay.update()
		should_attach_sector_button = true
		
	
	
# Used when an island graphic is hovered over and displays the info about the island passed in
# on the visual of the map.
# @Param island:Island -- The island that is hovered over.
func _on_IslandGraphic_island_hovered(island) -> void:
	# Shows the island information background
	$ParallaxBackground2/IslandInformation.show()
	var island_hovering_on: Island = island_with_images.get(island)
	# Gets the name of the island and sets it so the player can see.
	$ParallaxBackground2/IslandInformation/InformationContainer/IslandName.text = island_hovering_on.get_island_name()
	var island_type_text: String
	# If the island is an instance of destered it shows deseretd as type and ?? if endgame.
	if island_hovering_on is DesertedIsland:
		island_type_text = "Deserted Island"
	elif island_hovering_on is EndGameIsland:
		island_type_text = "???"
	# Othewise if the island is inhabited it shows its island type.
	elif island_hovering_on is InhabitedIsland:
		island_type_text = island_hovering_on.get_island_type() + " Island."
	$ParallaxBackground2/IslandInformation/InformationContainer/IslandType.text = island_type_text

# When the island is no longer being hovered over it hides the display graphic.
func _on_IslandGraphic_island_exited(island) -> void:
	$ParallaxBackground2/IslandInformation.hide()

# When the next sector button is called it updates the backend and displays a cutscene.
func _on_Button_pressed():
	get_node("/root/Main").game.get_map().progress_to_next_sector()
	get_tree().change_scene("res://Cutscenes/Sailing.tscn")