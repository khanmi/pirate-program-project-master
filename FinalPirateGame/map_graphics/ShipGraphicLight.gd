extends KinematicBody2D

# This script represents the light displays over the player ships based on the time of day.

# The canvas modulate and the light used for the effect.
onready var cm = $CM
onready var light = $LT
# Lights are default as off.
var lights_on = false

# Turns the lights on
func turn_lights_on():
	cm.visible = true
	light.visible = true
	lights_on = true

# Turns the lights off and the canvas modulate.
func turn_lights_off():
	cm.visible = false
	light.visible = false
	lights_on = false


# Called every cycle and checks the current time and turns the light on and canvas modulate once it reaches a certain time 
# otherwise they are off.
func _physics_process(delta):
	# Converts to minutes and seconds.
	var seconds = int(DayNightCycle.time) % 60
	var minutes = int(DayNightCycle.time) / 60
	if minutes > 6:
		seconds = seconds + (minutes * 60)
	# If passed 455 seconds and the lights are not on then they are turned on.
	if seconds >= 455:
		if not lights_on:
			turn_lights_on()
	# Othewise if they are on then they are turned off.
	else:
		if lights_on:
			turn_lights_off()

#start the processing when entering scene 
func _ready():
	turn_lights_off()