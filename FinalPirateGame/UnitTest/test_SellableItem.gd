extends "res://addons/gut/test.gd"

var sellable_item_test = preload("res://map/items/SellableItem.gd")
var item_test = preload("res://map/items/Item.gd")
var sellableItem = sellable_item_test.new("item_test", "test_description", 4)
var item = item_test.new("item_test", "test_description")

func test_get_gold_value():
	#testing get_gold_value ->int:
	assert_eq(4, sellableItem.get_gold_value())
 
func test_set_gold_value():
	#testing set_gold_value() ->void
	sellableItem.set_gold_value(10)
	assert_eq(10, sellableItem.gold_value)

func test_inheritance_from_Item():
	#testing inheritance from "res://map/items/Item.gd"
	assert_eq(item.get_item_name(), sellableItem.get_item_name())
	assert_eq(item.get_description(), sellableItem.get_description())
