extends "res://addons/gut/test.gd"

var vendor_test = preload("res://map/islandEntity/Vendor.gd")
var playerTest = preload ("res://player/Player.gd") #done

var player = playerTest.new()

var info = [
"Setting:  Bar_background",
"0|Description:  The Harbourmaster exclaims Greetings lad! It looks like you've seen a ghost.|Option:  1|Desc:  Continue|Need:  Item=null|Give_item:  null|Special_interaction:  false|Save_encounter:  false",
"1|Description:  You reply I don't have time for pretty conversations. I need to get out of this place, fast.|Option:  2|Desc:  Continue|Need:  Item=null|Give_item:  null|Special_interaction:  false|Save_encounter:  false",
"2|Description:  The Harbourmaster replies Aie, I take it that barely sea worth potty over there is yours. If you're runnin' from the navy, you'll need cannons lad.|Option:  4|Desc:  Leave|Need:  Item=null|Option:  3|Desc:  Purchase Upgrade|Need:  Item=null|Give_item:  null|Special_interaction:  false|Save_encounter:  false",
"3|Description:  The Harbourmaster wishes you well on your journey.|Option:  4|Desc:  Continue|Need:  Item=null|Give_item:  null|Special_interaction:  true|Save_encounter:  false",
"4|Description:  Return to your ship.|null|Gold_change:  0|Gunpowder_change:  0|Repairs_change:  0|Crew_change:  0|Rum_change:  0|Morale_change:  0|Give_item:  null|Special_interaction:  false|Save_encounter:  false",
"5|Description:  Never gets here|null"]

#construct vendor
var vendor = vendor_test.new(info, Vector2(100,100), "PIRATE", player, 4)

func test_encounter_stage():
#	testing vendor.next_encounter_stage(player,option_chosen:String) ->bool:
	assert_false(vendor.next_encounter_stage(player, "test"))
	
