extends "res://addons/gut/test.gd"

var shop_test = preload("res://map/islandEntity/Shop.gd")
var playerTest = preload ("res://player/Player.gd") #done

var player = playerTest.new()
var shop = shop_test.new(player, 4)

func test_purchaseable_player_upgrades():
#	testing shop.get_purchaseable_player_upgrade_name(index: int) -> String:
	assert_true(shop.get_purchaseable_player_upgrade_name(0))
	assert_true(shop.get_purchaseable_player_upgrade_name(1))
	assert_true(shop.get_purchaseable_player_upgrade_name(2))

func test_upgrade_item_purchase():
#	testing shop.upgrade_item_purchase(player_upgrade_index: int):
	assert_null(shop.upgrade_item_purchase(0))
	assert_null(shop.upgrade_item_purchase(1))
	assert_null(shop.upgrade_item_purchase(2))

func test_construct_ship_upgrade():
#	testing shop.construct_ship_upgrade() -> void:
	shop.construct_ship_upgrade(4)
	assert_true(shop.get_ship_upgrade().get_item_name())
	assert_true(shop.get_ship_upgrade().get_affect())
	assert_true(shop.get_ship_upgrade().get_gold_value())
	assert_true(shop.get_ship_upgrade().get_affected_resource())
	assert_true(shop.get_ship_upgrade().get_upgrades())

func test_construct_purchaseable_player_upgrades():
#	testing shop.construct_purchaseable_player_upgrades() -> void:
	shop.construct_purchaseable_player_upgrades(2)
	assert_true(shop.get_purchaseable_player_upgrades()[0])
	assert_true(shop.get_purchaseable_player_upgrades()[1])
	assert_true(shop.get_purchaseable_player_upgrades()[2])

func test_sell_item_pressed():
#	testing shop.sellItemPressed(player_items_index: int):
	shop.sell_item_pressed(1)
	assert_null(player.get_items()[1])
	shop.sell_item_pressed(2)
	assert_null(player.get_items()[2])

func test_on_ship_upgrade():
#	testing shop.onShipUpgrade()->void:
	assert_null(shop.on_ship_upgrade())
