extends "res://addons/gut/test.gd"


var clue = Clue.new("Navy", 1, "This is a test of a clue the first part represents the type of island the next clue is at, the second part is realted to the end game puzzle.")



func test_Clue_next_island_type():
	#testing get_next_island_type(): -> String
	assert_eq("Navy", clue.get_next_island_type())

func test_Clue_end_game_option():
	#testing get_end_game_option(): _>int
	assert_eq(1, clue.get_end_game_option())

func test_Clue_text():
	#testing get_description(): ->String  [method inherited from "res://map/items/Item.gd"]
	var text: String = "This is a test of a clue the first part represents the type of island the next clue is at, the second part is realted to the end game puzzle."
	assert_eq(text, clue.get_description())
