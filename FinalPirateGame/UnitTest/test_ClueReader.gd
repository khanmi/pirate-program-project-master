extends "res://addons/gut/test.gd"


var clue_reader = ClueReader.new()


# Test that all the clues are constructed correctly.
func test_ClueReader_read():
	clue_reader.read("res://UnitTest/ClueTest.txt")
	var tmp_clues : Array = []
	tmp_clues.resize(2)
	tmp_clues[0] = Clue.new("Navy", 1, "This is a test of a clue the first part represents the type of island the next clue is at, the second part is realted to the end game puzzle.")
	tmp_clues[1] = Clue.new("Navy", 2, "This is a test of a clue the first part represents the type of island the next clue is at, the second part is realted to the end game puzzle.")
	var clue: Clue = clue_reader.get_next_clue()
	assert_eq(tmp_clues[0].get_description(), clue.get_description())
	assert_eq(tmp_clues[0].get_next_island_type(), clue.get_next_island_type())
	assert_eq(tmp_clues[0].get_end_game_option(), clue.get_end_game_option())

	clue = clue_reader.get_next_clue()
	assert_eq(tmp_clues[1].get_description(), clue.get_description())
	assert_eq(tmp_clues[1].get_next_island_type(), clue.get_next_island_type())
	assert_eq(tmp_clues[1].get_end_game_option(), clue.get_end_game_option())
