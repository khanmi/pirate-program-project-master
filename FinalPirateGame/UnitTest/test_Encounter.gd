extends "res://addons/gut/test.gd"

var encounterTest = load("res://Encounter/Encounter.gd")#done

var info = ["Setting:  Wooden_house",
"0|Description:  You have come face-to-face with a pirate what will you do?|Option:  1|Desc:  Speak to the pirate|Need_item:  PIRATE_LETTER|Option:  2|Desc:  Battle the pirate!|Need_item:  GEM|Give_item:  GEM|Special_interaction:  false",
"1|Description:  The pirate contains some important information about the mystical treaure|Option:  3|Desc:  Find out more?|Need_item:  null|Give_item:  null|Special_interaction:  false",
"2|Description:  The pirate runs away screaming for mercy. 'Please don't attack me just like that ye old Narvera Barsea.'|null|Gold_change:  0|Gunpowder_change:  0|Repairs_change:  0|Crew_change:  0|Rum_change:  0|Morale_change:  0|Give_item:  CLUE|Special_interaction:  false",
"3|Description:  Many have seeked out the mystical treasure and suspiciously have gone missing! I will join your crew and be part of the next 'Pirate Legends'.|null|Gold_change:  50|Gunpowder_change:  0|Repairs_change:  0|Crew_change:  3|Rum_change:  0|Morale_change:  0|Give_item:  null|Special_interaction:  false",
"4|Description:  Never gets here|null|"]

var encounter = encounterTest.new(info)
var player:Player = Player.new()
func test_Encounter_stage():
	assert_eq(encounter.get_stage().get_desc(), "You have come face-to-face with a pirate what will you do?")
	assert_eq(encounter.get_stage().get_options().hash(), {["1","Speak to the pirate"]: "PIRATE_LETTER",["2","Battle the pirate!"]: "GEM"}.hash())
	for key in encounter.get_stage().resource_keys:
		assert_eq(encounter.get_stage().resource_changes[key], 0)

func test_Encounter_invalid_choose_option():
	# Check the stage has not changed from an invalid option
	encounter.choose_option(3, player)
	assert_eq(encounter.get_stage().get_desc(), "You have come face-to-face with a pirate what will you do?")
	assert_eq(encounter.get_stage().get_options().hash(), {["1","Speak to the pirate"]: "PIRATE_LETTER",["2","Battle the pirate!"]: "GEM"}.hash())
	for key in encounter.get_stage().resource_keys:
		assert_eq(encounter.get_stage().resource_changes[key], 0)


func test_give_player_stage_item():
#testin give_palyer_stage_item(player:Player)->void;
	encounter.start_encounter(player)
	assert_eq(100, player.get_resources()["Crew"])
	assert_eq(100, player.get_resources()["Gold"])
	assert_eq(100, player.get_resources()["Gunpowder"])
	assert_eq(100, player.get_resources()["Morale"])
	assert_eq(100, player.get_resources()["Repairs"])
	assert_eq(100, player.get_resources()["Rum"])


func test_check_player_has_required_item():
	#testing check_player_has_required_item(item_needed_name: String,  player:Player) ->bool:
	encounter.start_encounter(player)
	assert_eq(false, encounter.check_player_has_required_item("CLUE_1", player))

func test_check_player_has_required_resources():
#testing check_player_has_required_resources(resource:String, resource_amount:int, player:Player)->bool:
	encounter.start_encounter(player)
	assert_eq(true, encounter.check_player_has_required_resources("Crew", 50, player))
	assert_eq(false, encounter.check_player_has_required_resources("Gold", 120, player))
