extends "res://addons/gut/test.gd"

var normal_island_test = preload("res://map/islands/NormalIsland.gd")
var playerTest = preload ("res://player/Player.gd") #done
var player = playerTest.new()

var onEnter = ["Character_type:  Pirate",
"Setting:  Bar_background",
"0|Description:  You enter an inn that's filled to the brim with drunken sailors singing songs of the seas and bantering with each other about how they will be the one to discover Barsea's treasure.|Option:  1|Desc:  Continue|Need:  Item=null|Give_item:  null|Special_interaction:  false|Save_encounter:  false",
"1|Description:  You sit at the bar and the bartender asks Yer not from round these parts is ya? Man with boots and a hat like yours, I put 10 gold on ya bein out ta snatch ole' Barsea's gold. What'll it be? |Option:  2|Desc:  Rum|Need:  Item=null|Give_item:  null|Special_interaction:  false|Save_encounter:  false",
"2|Description:  After you get drunk, the bartender says If ya ask me, I think this whole treasure is a sham |Option:  3|Desc:  Continue|Need:  Item=null|Give_item:  null|Special_interaction:  false|Save_encounter:  false",
"3|Description:  *BANG*|Option:  4|Desc:  Continue|Need:  Item=null|Give_item:  null|Special_interaction:  false|Save_encounter:  false",
"4|Description:  You reach into your jacket and take out a note signed by Barsea, If the treasure isn't real, how do you explain this?! |Option:  5|Desc:  Continue|Need:  Item=null|Give_item:  null|Special_interaction:  false|Save_encounter:  false",
"5|Description:  The note reads, That's the 10th crew to fail my puzzles, what are they? Imbeciles? This treasure hunt is simple. Why can no one find it?!|Option:  6|Desc:  Continue|Need:  Item=null|Give_item:  null|Special_interaction:  false|Save_encounter:  false",
"6|Description:  A sailor overhears your conversation and announces to the Tavern, THE TREASURE IS REAL! GET THAT PIRATE! |Option:  7|Desc:  Leave|Need:  Item=null|Give_item:  null|Special_interaction:  false|Save_encounter:  false",
"7|Description:  Go see the Harbourmaster.|null|Gold_change:  0|Gunpowder_change:  0|Repairs_change:  0|Crew_change:  0|Rum_change:  0|Morale_change:  0|Give_item:  null|Special_interaction:  false|Save_encounter:  false",
"8|Description:  Never gets here|null"]


var onIsland = ["Character_type:  Pirate",
"Setting:  Wooden_house",
"0|Description:  EXTRA EXTRA, LOCAL PIG FARMER JUMPED BY GANG OF HOODLUMS, HE WAS OINKED WITNESSES SAY!|Option:  1|Desc:  What?|Need:  Item=null|Give_item:  null|Special_interaction:  false|Save_encounter:  false",
"1|Description:  Hey mister, you're not from here are you? Well I'm Gregg the News guy. I want the big scoop |Option:  2|Desc:  Ignore and Leave|Need:  Item=null|Give_item:  null|Special_interaction:  false|Save_encounter:  false",
"2|Description:  Woah woah! Mister, where are you heading off to in such a rush? Stick around a while, Is that a mink coat? Hey mister, hey... |Option:  3|Desc:  Irritatedly ask how much you need to pay to be left alone|Need:  Item=null|Give_item:  null|Special_interaction:  false|Save_encounter:  false",
"3|Description:  Aww mister, that's awfully nice but I'm a journalist! My passion is writing, when I was born, they say I came out of my mothers bosom feather in hand writing poetry of my birth. Oh I'd tell you mister, the looks on their faces when I... |Option:  4|Desc:  Yes yes, get on with it |Need:  Item=null|Give_item:  null|Special_interaction:  false|Save_encounter:  false",
"4|Description:  I just want to know what drives a man to piracy. Nobody was born what they are, except for me ofcourse haha. What is your backstory? |Option:  5|Desc:  Pirate|Need:  Item=null|Option:  6|Desc:  Navy|Need:  Item=null|Option:  7|Desc:  Merchant |Need:  Item=null|Give_item:  null|Special_interaction:  false|Save_encounter:  false",
"5|Description:  Funny, 'No man was born what they are'. As far as I can remember, the sea is the only thing I've ever known. I've seen all the things you writers wish you could write stories of. Stories of adventure, camaraderie, of pleasure and of darkness. |Option:  8|Desc:  Continue|Need:  Item=null|Give_item:  BROKEN_TANKARD|Special_interaction:  false|Save_encounter:  false",
"6|Description:  I was navy. 9th fleet. You're right, we're not always who we start out as. I joined what I thought was rightous and correct but who decides what right? The Navy? Maybe you should write about that in your next publication. |Option:  8|Desc:  Continue|Need:  Item=null|Give_item:  BROKEN_FLINTLOCK|Special_interaction:  false|Save_encounter:  false",
"7|Description:  You wouldn't believe it Nigel, but I am just a humble merchant. I travel and I sell, some men have contempt for a man in my trade. Alas, I have only become a pirate to protect my own interests |Option:  8|Desc:  Continue|Need:  Item=null|Give_item:  BROKEN_TELESCOPE|Special_interaction:  false|Save_encounter:  false",
"8|Description:  Oh mister, tell me more, I need to know more! |Option:  9|Desc:  Continue|Need:  Item=null|Give_item:  null|Special_interaction:  false|Save_encounter:  false",
"9|Description:  I have to go, the navy are on my tail. All you need to know is that I intend to be the man to find Barsea's treasure! |Option:  10|Desc:  Leave|Need:  Item=null|Give_item:  null|Special_interaction:  false|Save_encounter:  false",
"10|Description: Click the bag icon on the top right to check your inventory to inspect the your new item.|null|Gold_change:  0|Gunpowder_change:  0|Repairs_change:  0|Crew_change:  0|Rum_change:  0|Morale_change:  0|Give_item:  null|Special_interaction:  false|Save_encounter:  false",
"11|Description: Never gets here|null"]

var onExit = ["Character_type:  Pirate_captain",
"Setting:  Beach_with_boat",
"0|Description:  A group of ruffians approach you and one of them asks You're really going after Barsea's treasure? Let us join you! |Option:  1|Desc:  Continue|Need:  Item=null|Give_item:  null|Special_interaction:  false|Save_encounter:  false",
"1|Description:  As you're leaving, a sailor from the Tavern spots you and comes after you with his sword in hand.|Option:  2|Desc:  Fight!|Need:  Item=null|Give_item:  null|Special_interaction:  false|Save_encounter:  false",
"2|Description:  You have successfully fended off the Sailor but he has alerted command. Now The Navy are chasing you. Make haste and set sail!|null|Gold_change:  200|Gunpowder_change:  0|Repairs_change:  0|Crew_change:  20|Rum_change:  0|Morale_change:  50|Give_item:  null|Special_interaction:  true|Save_encounter:  false",
"3|Description:  Never gets here|null"]

#Normalised Island constructor
var island = normal_island_test.new("testIsalnd",onEnter,onIsland, onExit, 1, "testString", player)


func test_player_interact_with_enity_onExit_NPC():
#	testing when player.current_state =is equal to player.Player_states.LEAVING_ISLAND player_interact_with_entity(player) -> void:
	player.current_state = player.Player_states.LEAVING_ISLAND
	island.player_interact_with_entity(player)
	assert_eq("A group of ruffians approach you and one of them asks You're really going after Barsea's treasure? Let us join you! ",island.get_interacting_npc().get_encounter().get_stage().get_desc())
	assert_eq(false, island.get_interacting_npc().get_encounter().get_stage().has_special_interaction())

func test_player_interact_with_enity_onEnter_NPC():
#	testing when player.current_state =is equal to player.Player_states.ENTERING_ISLAND player_interact_with_entity(player) -> void:
	player.current_state = player.Player_states.ENTERING_ISLAND
	island.player_interact_with_entity(player)
	assert_eq("You enter an inn that's filled to the brim with drunken sailors singing songs of the seas and bantering with each other about how they will be the one to discover Barsea's treasure.",island.get_interacting_npc().get_encounter().get_stage().get_desc())
	assert_eq(false, island.get_interacting_npc().get_encounter().get_stage().has_special_interaction())

func test_player_interact_with_enity_satates_INTERACTING():
#	testing when player.current_state =is equal to player.Player_states.INTERACTING player_interact_with_entity(player) -> void:
	player.current_state = player.Player_states.INTERACTING
	island.player_interact_with_entity(player)
	assert_eq(false, island.get_interacting_npc().interacted_with_player())

func test_interacted_with_entity_at_ground_space():
#	island. interacted_with_entity_at_ground_space(temp_space: Vector2, player): -> void
	island.interacted_with_entity_at_ground_space(Vector2(700,300), player)
	assert_eq(3,island.player.current_state)

func test_item_upgrade_and_useable_spawn_probability():
	assert_eq(0, island.get_upgrade_item_spawn_probability())
	assert_eq(0, island.get_useable_item_spawn_probability())
