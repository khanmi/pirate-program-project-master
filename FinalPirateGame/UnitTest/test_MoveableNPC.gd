extends "res://addons/gut/test.gd"
var moveableNPC_test = preload ("res://map/islandEntity/MoveableNPC.gd")
var playerTest = preload ("res://player/Player.gd") #done
var player = playerTest.new()
var player_character = PlayerCharacter.new(Vector2(1,1))
var npcEncounter = [
"Setting:  Wooden_house",
"0|Description:  Go away filthy PIRATE!|Option:  1|Desc:  Leave!|Need:  Item=null|Option:  2|Desc:  Try to talk|Need:  Item=BROKEN_FLINTLOCK|Give_item:  null|Special_interaction:  false|Save_encounter:  true",
"1|Description:  You back away slowly watching the soldiers every move*|null|Gold_change:  0|Gunpowder_change:  0|Repairs_change:  0|Crew_change:  0|Rum_change:  0|Morale_change:  0|Give_item:  null|Special_interaction:  false|Save_encounter:  true",
"2|Description:  I will not talk to a rascal pirate!|Option:  3|Desc:  Show your Broken Flintlock|Need:  Item=null|Give_item:  null|Special_interaction:  false|Save_encounter:  false",
"3|Description:  You were a Navy soldier to! If I'm honest I've wanted to leave for a while now to get that gold myself and join a crew but, all pirates I know have not a single truthful bone in their body. But you may be different coming from the Navy! |Option:  4|Desc:  Ask him to join|Need:  Item=null|Give_item:  null|Special_interaction:  false|Save_encounter:  false",
"4|Description:  Yeh of course. A split of that treasure will be more than I could earn in multiple lifetimes! |null|Gold_change:  20|Gunpowder_change:  0|Repairs_change:  0|Crew_change:  5|Rum_change:  0|Morale_change:  0|Give_item:  null|Special_interaction:  false|Save_encounter:  false",
"5|Description:  Never gets here|null"]

#MoveableNPC constructor _init(tmp_encounter: Array, pos: Vector2, temp_type: String, aggresive:bool, player:Player, difficulty:int).(tmp_encounter, pos, temp_type, player, difficulty):
var moveable_NPC = moveableNPC_test.new(npcEncounter,Vector2(100, 100), "Navy", false, player, 2)

func test_default_settings():
#	testing deafult postion and opressivness of NPC
	assert_eq(Vector2(10700,10300), moveable_NPC.get_default_position())
	assert_false(moveable_NPC.is_opressive)

func test_move_enity():
#	testing move_entity(player_character: PlayerCharacter):
#	testing is_triggered(player_position: Vector2):
	moveable_NPC.move_entity(player_character)
	assert_eq(1, moveable_NPC.triggered)
