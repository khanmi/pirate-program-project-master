extends "res://addons/gut/test.gd"

var NPC_test = preload ("res://map/islandEntity/NPC.gd")
var player:Player = Player.new()

var info = ["Setting:  Wooden_house",
"0|Description:  You have come face-to-face with a pirate what will you do?|Option:  1|Desc:  Speak to the pirate|Need_item:  PIRATE_LETTER|Option:  2|Desc:  Battle the pirate!|Need_item:  GEM|Give_item:  GEM|Special_interaction:  false",
"1|Description:  The pirate contains some important information about the mystical treaure|Option:  3|Desc:  Find out more?|Need_item:  null|Give_item:  null|Special_interaction:  false",
"2|Description:  The pirate runs away screaming for mercy. 'Please don't attack me just like that ye old Narvera Barsea|null|Gold_change:  0|Gunpowder_change:  0|Repairs_change:  0|Crew_change:  0|Rum_change:  0|Morale_change:  0|Give_item:  CLUE|Special_interaction:  false",
"3|Description:  Many have seeked out the mystical treasure and suspiciously have gone missing! I will join your crew and be part of the next 'Pirate Legends'.|null|Gold_change:  50|Gunpowder_change:  0|Repairs_change:  0|Crew_change:  3|Rum_change:  0|Morale_change:  0|Give_item:  null|Special_interaction:  false",
"4|Description:  Never gets here|null|"]

var NPC = NPC_test.new(info, Vector2(0,0), "PIRATE", player,3)

func test_NPC_interact_with_player():
	#testing intercat with player function: interacted_with_player() -> bool:
	assert_eq(true, NPC.interact(player))

func test_next_encounter():
	#testing next_encounter_stage(player,option_chosen:String) ->bool:
	assert_eq(false, NPC.next_encounter_stage(player, "0|Description:  You have come face-to-face with a pirate what will you do?|Option:  1|Desc:  Speak to the pirate|Need_item:  PIRATE_LETTER|Option:  2|Desc:  Battle the pirate!|Need_item:  GEM|Give_item:  GEM|Special_interaction:  false"))

func test_interacted_with_player():
	#testing interacted_with_player() ->bool:
	assert_eq(false, NPC.interacted_with_player())
	NPC.encounter.get_stage().options  = {}
	assert_eq(true, NPC.interacted_with_player())
