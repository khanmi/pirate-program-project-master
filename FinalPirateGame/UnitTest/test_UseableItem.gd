extends "res://addons/gut/test.gd"

var useable_item_test = preload("res://map/items/UseableItem.gd")

func test_item_level():
	#testing get_item_level(): ->int
	var useableItem = useable_item_test.new("item_test", 4)
	var level= useableItem.item_level
	assert_eq(level, useableItem.get_item_level())
