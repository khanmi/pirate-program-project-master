extends "res://addons/gut/test.gd"

var upgrade_item_test = preload("res://map/items/UpgradeItem.gd")
var item_test = preload("res://map/items/Item.gd")
var upgradeItem = upgrade_item_test.new(10)



func test_constructor():
	#testing constructor _init(max_level: int).("","",0): -> void
	assert_eq(upgradeItem.upgrades[upgradeItem.item_name].get("cost"), upgradeItem.get_gold_value())

func test_description():
	#set_upgrade_item_description() -> void:
	upgradeItem.set_upgrade_item_description()
	var item = item_test.new(upgradeItem.get_item_name(), upgradeItem.get_description())
	assert_eq(item.get_description(), upgradeItem.get_description())

func test_get_affect():
	#testing get_affect() -> float:
	assert_eq(upgradeItem.upgrades[upgradeItem.item_name]["affect"] + (upgradeItem.upgrade_level / 100 * 2), upgradeItem.get_affect())
