extends "res://addons/gut/test.gd"

var story_item_collection_test = preload("res://map/items/StoryItemCollection.gd")
var storyItem = story_item_collection_test.new()

func test_GEM():
	#testing get_story_items(name: String) -> Item
	assert_eq("Mystical Gem", storyItem.get_story_item("GEM").get_item_name())
	assert_eq("The rumoured mystical gem, thought to have mystical powers!", storyItem.get_story_item("GEM").get_description())

func test_PIRATE_LETTER():
	#testing get_story_items(name: String) -> Item
	assert_eq("Pirate captain letter", storyItem.get_story_item("PIRATE_LETTER").get_item_name())
	assert_eq("A secret pirate letter, containing the pirate code. Sure to gain a pirates trust", storyItem.get_story_item("PIRATE_LETTER").get_description())

func test_MERCHANT_LETTER():
	#testing get_story_items(name: String) -> Item
	assert_eq("Merchant letter", storyItem.get_story_item("MERCHANT_LETTER").get_item_name())
	assert_eq("A letter of graditude, sure to gain any merchant's trust.", storyItem.get_story_item("MERCHANT_LETTER").get_description())

func test_NAVY_LETTER():
	#testing get_story_items(name: String) -> Item
	assert_eq("Navy service letter", storyItem.get_story_item("NAVY_LETTER").get_item_name())
	assert_eq("A letter of service to the Royal Navy.", storyItem.get_story_item("NAVY_LETTER").get_description())

func test_SEA_TACTICS_MANUAL():
	#testing get_story_items(name: String) -> Item
	assert_eq("Sea tactics manual.", storyItem.get_story_item("SEA_TACTICS_MANUAL").get_item_name())
	assert_eq("A written book of legendary sea tactics sure to win a battle.", storyItem.get_story_item("SEA_TACTICS_MANUAL").get_description())

func test_LEFT_CURSE_POTION():
	#testing get_story_items(name: String) -> Item
	assert_eq("Curse removal potion", storyItem.get_story_item("LIFT_CURSE_POTION").get_item_name())
	assert_eq("Potion that removes ancient curses.", storyItem.get_story_item("LIFT_CURSE_POTION").get_description())

func test_ALE():
	#testing get_story_items(name: String) -> Item
	assert_eq("Ale", storyItem.get_story_item("ALE").get_item_name())
	assert_eq("Some good old ale. Sure to lighten up someones day.", storyItem.get_story_item("ALE").get_description())

func test_RED_PEARL():
	#testing get_story_items(name: String) -> Item
	assert_eq("Red Pearl", storyItem.get_story_item("RED_PEARL").get_item_name())
	assert_eq("A stunning Red Pearl. Believed to be lucky among pirates.", storyItem.get_story_item("RED_PEARL").get_description())

func test_SILVER_PEARL():
	#testing get_story_items(name: String) -> Item
	assert_eq("Silver Pearl", storyItem.get_story_item("SILVER_PEARL").get_item_name())
	assert_eq("A stunning silver pearl. Worth a shiny penny.", storyItem.get_story_item("SILVER_PEARL").get_description())

func test_BROKEN_TANKARD():
	#testing get_story_items(name: String) -> Item
	assert_eq("Broken tankard", storyItem.get_story_item("BROKEN_TANKARD").get_item_name())
	assert_eq(10, storyItem.get_story_item("BROKEN_TANKARD").get_gold_value())
#
func test_BROKEN_FLINTLOCK():
	#testing get_story_items(name: String) -> Item
	assert_eq("Broken flintlock", storyItem.get_story_item("BROKEN_FLINTLOCK").get_item_name())
	assert_eq(10, storyItem.get_story_item("BROKEN_FLINTLOCK").get_gold_value())

func test_BROKEN_TELESCOPE():
	#testing get_story_items(name: String) -> Item
	assert_eq("Broken telescope", storyItem.get_story_item("BROKEN_TELESCOPE").get_item_name())
	assert_eq(10, storyItem.get_story_item("BROKEN_TELESCOPE").get_gold_value())

func test_set_up_character_style_items():
#	testing set_up_character_style_items()->void:
	storyItem.set_up_character_style_items()
	assert_eq(0.15,storyItem.get_story_items()["BROKEN_TELESCOPE"].get_affect())
	assert_eq(0.15,storyItem.get_story_items()["BROKEN_FLINTLOCK"].get_affect())
	assert_eq(0.15,storyItem.get_story_items()["BROKEN_TANKARD"].get_affect())
