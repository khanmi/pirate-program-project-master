extends "res://addons/gut/test.gd"

var islandTest = preload("res://map/islands/Island.gd") #done
var player:Player = Player.new()
var island = islandTest.new("name1", player)
var islandTwo = islandTest.new("name2", player)
var islandThree = islandTest.new("name3", player)

func test_Island_adjacent_islands():
	#add_adjacent_island method: it should add and adjacent island to this island 
	#create first island 
	island.add_adjacent_island(islandTwo, true)
	#testing that island.adjacent_island [0] is equal to the given parametre which in this case is islandTwo
	assert_eq(islandTwo, island.adjacent_islands[0]) 
	# Test that island is islandTwo's adjacent islands.
	assert_eq(island, islandTwo.adjacent_islands[0])
	#create second island 
	island.add_adjacent_island(islandThree, false)
	#testing that island.adjacent_island [1] is equal to the given parametre which in this case is islandThree
	assert_eq(islandThree, island.adjacent_islands[1])
	# Test that when connecting two islands, only the first will connect both not the second, otherwise both would have
	# two of the same island adjacent to itself.
	assert_eq(null, islandThree.adjacent_islands[0])
	#testing island.get_adjacent_island() function - it should get all the adjacent island to this islands
	var array = island.get_adjacent_islands()
	assert_eq(islandTwo, array[0])
	assert_eq(islandThree, array[1])

func test_Island_position():
	#testing postion island.set_position(pos: Vector2) and isalnd.get_position()
	island.set_position(Vector2(4,4))
	assert_eq(Vector2(4,4), island.get_position())
